<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?=$site_Info['Title']?> | <?=$Pages[0]->Title?></title>
    <meta name="description" content="<?=$site_Info['MetaDescription']?>, <?=$Pages[0]->MetaDescription?>">
    <meta name="keywords" content="<?=$site_Info['MetaKeywords']?>, <?=$Pages[0]->MetaKeyword?>">
    <meta name="author" content="<?=base_url()?>">
    <?=$this->load->view('inc_header_files');?>
</head>

<body class="header-sticky">
    <?=$this->load->view('inc_header');?>

	<section class="roll-row page-title page-about-alt">
        <div class="main-title parallax">
            <div class="page-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="span12">
                    <h1 class="title pull-center"><?=$Pages[0]->Title?></h1>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>
		<div class="page-nav">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="breadcrumbs">
                            <li class="nav-prev"><a href="<?=base_url()?>">Home</a></li>
                            <li class="nav-split"><a href="#"> > </a></li>
                            <li><a href="#"><?=$Pages[0]->Title?></a></li>
                        </ul>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
		</div>
    </section><!-- /.page-title -->

    <section class="roll-row main-page roll-aboutus">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="aboutus">
                            <?=$Pages[0]->Description?>
                    </div><!-- /.aboutus -->
                </div><!-- /.span12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.roll-aboutus -->

   <?=$this->load->view('inc_footer');?>
   <?=$this->load->view('inc_footer_files');?>
</body>
</html>