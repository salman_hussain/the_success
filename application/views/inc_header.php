    
<style>
#loading-div-background 
{
    display:none;
    position:fixed;
    top:0;
    left:0;
            background:#4D4D4D;
    width:100%;
    height:100%;
            z-index: 1;
            color:#E27513;
            opacity:0.4;
}
#loading-div
{
    width: 300px;
    height: 200px;
    text-align:center;
    position:absolute;
    left: 50%;
    top: 50%;
    margin-left:-150px;
    margin-top: -100px;
}
</style>

<div id="loading-div-background">
    <div id="loading-div" class="ui-corner-all" >
        <img src="<?= PATH ?>web/images/loader.gif" alt="Loading.." style="width: 100px" />
        <h2 style="color:#E27513;">Please wait....</h2>
    </div>
</div>

    <div id="cart_success" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cart</h4>
                </div>
                <div class="modal-body">
                    <p>Item Was Successfully Added To Your Cart</p>
                </div>
                <div class="modal-footer">
                    
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div id="cart_error" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Cart</h4>
                </div>
                <div class="modal-body">
                    <p>Please Try Again Later.</p>
                </div>
                <div class="modal-footer">
                    
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <div class="roll-top">
        <div class="container">
                <div class="row">
                <div class="span12">
                    <ul class="top-left">
                        <li class="email icon-mail"><a href="mailto:<?=$site_Info['EmailTo']?>"><?=$site_Info['EmailTo']?></a></li>
                        <li class="phone icon-phone"><a href="tel:+<?=$site_Info['Phone']?>"><?=$site_Info['Phone']?></a></li>
                    </ul>
                    <ul class="top-right">
                        <li class="cart icon-basket"><a href="<?= base_url() ?>home/view_cart">CART</a></li>
                        <?php
                        if($this->session->userdata('user_Login') != TRUE)
                        {
                        ?>
                        <li class="login icon-user"><a href="<?= base_url()?>accounts?i=login">LOGIN</a></li>
                        <?php
                        }
                        else
                        {
                        ?>
                        <li class="login icon-user"><a href="<?= base_url() ?>accounts/logout">LOGOUT</a></li>
                        <?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <header id="header" class="header">
        <div class="container">
                <div class="row">
                <div class="span2">
                    <div id="logo" class="logo">
                        <a href="<?=base_url()?>" rel="home"><img src="<?=PATH?>upload/<?=$site_Info['Logo']?>" alt="LOGO" /></a>
                    </div><!-- /.logo -->
                </div><!-- /.roll-col2 -->
                <div class="span10">
                    <div class="btn-menu"></div><!-- //mobile menu button -->
                    <nav id="mainnav" class="mainnav">
						<ul class="menu">
						   <?php   
						   $count = 0;
						   foreach($Menu as $menu1){
						   
							 if($menu1->UnderPage=="0" && $menu1->Header=="1" ){
							 $Link = $menu1->SEO;
							 if($Link != "home"){
							  $Link = $Link;
							 }
							 $a=0;
							 
							 foreach($Menu as $menu2){
							  if($menu2->UnderPage==$menu1->IdPages){
							   $a=1;
							   break;
							  }
							 }
						   ?>
						   <li id="<?=$count++?>">
						   <a href="<?=base_url().$Link?>">
							<?=$menu1->Title?>
						   </a>
						   <?php if($a==1){ ?>
							<ul class="sub-menu">
								<?php
								foreach($Menu as $menu2){
								 if($menu2->UnderPage==$menu1->IdPages){
								  $Link = $menu2->SEO;
								  $b=0;
								  foreach($Menu as $menu3){
								   if($menu3->UnderPage==$menu2->IdPages){
									$b=1;
									break;
								   }
								  }
								 ?>
								  <li>
								  <a href="<?=base_url().$Link?>"><?=$menu2->Title?></a>
								  <?php if($b==1){?>
									<ul class="sub-menu">
									 <?php 
									 foreach($Menu as $menu3){
									 $Link = $menu3->SEO;
									 ?>
									  <li><a href="<?=base_url().$Link?>"><?=$menu3->Title?></a></li>
									 <?php }?>
									 </ul>
								  <?php }?>
								  </li>
								 <?php
								 }
								}?>
							   </ul>
						   <?php }?>
						   </li>
						   <?php }}?>        
						</ul>                      
					  <!-- /.menu -->
                    </nav><!-- /nav -->
                </div><!-- /.span10 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </header>