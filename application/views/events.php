<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?=$site_Info['Title']?> | <?=$Pages[0]->Title?></title>
    <meta name="description" content="<?=$site_Info['MetaDescription']?>, <?=$Pages[0]->MetaDescription?>">
    <meta name="keywords" content="<?=$site_Info['MetaKeywords']?>, <?=$Pages[0]->MetaKeyword?>">
    <meta name="author" content="<?=base_url()?>">
    <?=$this->load->view('inc_header_files');?>
</head>

<body class="header-sticky">
    <?=$this->load->view('inc_header');?>

    <section class="roll-row page-title page-about-alt">
        <div class="main-title parallax">
            <div class="page-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="span12">
                    <h1 class="title pull-center"><?=$Pages[0]->Title?></h1>
					<?=$Pages[0]->Description?>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>
		<div class="page-nav">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="breadcrumbs">
                            <li class="nav-prev"><a href="<?=base_url()?>">Home</a></li>
                            <li class="nav-split"><a href="#"> > </a></li>
                            <li><a href="#"><?=$Pages[0]->Title?></a></li>
                        </ul>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
		</div>
    </section><!-- /.page-title -->

    <section class="roll-row main-page">
        <div class="container">
            <div class="row">
                <div class="clearfix"></div>
                <div class="blog-container">
                  
				   <?php 
				   foreach($Events as $events){
				   ?>
				   <div class="span4 happy" style="height:475px;">
                        <article class="post">
						    <?php if($events->Image){?>
                            <a href="events/<?=$events->SEO?>"><img src="<?=PATH?>upload/<?=$events->Image?>" alt="image" style="height:275px;width:500px"></a>
                            <?php }else{?>
							<a href="events/<?=$events->SEO?>"><img src="<?=PATH?>upload/No_available_image.gif" alt="image" style="height:275px;width:500px"></a>
							<?php }?>
							<h4 class="title-post" style="height:100px;"><a href="events/<?=$events->SEO?>"><?=strip_tags(word_limiter($events->Title,5))?></a></h4>
                            <div class="meta-post">
                                <span class="date"> <?=date('d-M-Y',strtotime($events->Created));?></span>
                            </div>
							<div style="height:80px;">
							<?=strip_tags(word_limiter($events->Description, 20))?><br><br>
							</div>
                            <a href="events/<?=$events->SEO?>" class="read-more">READ MORE</a>
                        </article><!-- /.post -->
                    </div>
					<?php }?>
					
                </div><!-- /.blog-container --> 
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.main-page -->

   <?=$this->load->view('inc_footer');?>
   <?=$this->load->view('inc_footer_files');?>
</body>
</html>