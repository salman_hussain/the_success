<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
            <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
            <title><?= $site_Info['Title'] ?> | Forgot Password</title>
            <meta name="description" content="<?= $site_Info['MetaDescription'] ?>">
                <meta name="keywords" content="<?= $site_Info['MetaKeywords'] ?>">
                    <meta name="author" content="<?= base_url() ?>">
                        <?= $this->load->view('inc_header_files'); ?>
                        </head>

                        <body class="header-sticky">
                            <?= $this->load->view('inc_header'); ?>

                            <section class="roll-row page-title page-about-alt">
                                <div class="page-nav">
                                    <div class="container">
                                        <div class="row">
                                            <div class="span12">
                                                <ul class="breadcrumbs">
                                                    <li class="nav-prev"><a href="<?= base_url() ?>">Home</a></li>
                                                    <li class="nav-split"><a href="#"> > </a></li>
                                                    <li><a href="#">Accounts</a></li>
                                                </ul>
                                            </div><!-- /.span12 -->
                                        </div><!-- /.row -->
                                    </div><!-- /.container -->
                                </div>
                            </section><!-- /.page-title -->

                            <section class="roll-row contact-page main-page">
                                <div class="container">
                                    <div class="row">	
                                        <div class="span6">
                                            <div class="comment-respond">
                                                <div class="span12"> 
                                                    <h4 class="title">Forgot Password</h4>
                                                </div>  												<br>												<div class="span6"> 												<?php												if ($this->session->userdata('Error')) {													echo $this->session->userdata('Error');													$this->session->unset_userdata('Error');												}												?>												<?php												if ($this->session->userdata('Success')) {													echo $this->session->userdata('Success');													$this->session->unset_userdata('Success');												}												?>												</div>
                                                <form class="comment-form" id="login_form" method="post" action="<?= base_url() ?>accounts/reset_password" >
                                                    <div class="span12"> 
                                                        <div class="input-wrap name">
                                                            <input type="email" size="14" value="" placeholder="E-mail" name="email" required="">
                                                        </div>
                                                    </div>
                                                    <div class="span12"> 
                                                        <div class="submit-wrap">
                                                            <button type="submit" id="submitcomment" name="submitcomment" class="btn-submit">
                                                                SUBMIT
                                                            </button>
                                                        </div>
                                                    </div>	
                                                </form>
                                            </div><!-- /.comment-respond -->
                                        </div>
                                    </div><!-- /.row -->
                                </div><!-- /.container -->
                            </section>

                            <?= $this->load->view('inc_footer'); ?>

                            <?= $this->load->view('inc_footer_files'); ?>
                        </body>
                        </html>