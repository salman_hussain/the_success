<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?=$site_Info['Title']?> | <?=$Shops[0]->Title?></title>
    <meta name="description" content="<?=$site_Info['MetaDescription']?>, <?=$Shops[0]->MetaDescription?>">
    <meta name="keywords" content="<?=$site_Info['MetaKeywords']?>, <?=$Shops[0]->MetaKeyword?>">
    <meta name="author" content="<?=base_url()?>">
	<!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?=PATH?>web/stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?=PATH?>web/stylesheets/style.css">

    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?=PATH?>web/stylesheets/animate.css">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Sanchez:400italic,400' rel='stylesheet' type='text/css'>

    <!-- Favicon and touch icons  -->
    <link href="<?=PATH?>web/icon/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
    <link href="<?=PATH?>web/icon/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
    <link href="<?=PATH?>web/icon/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
    <link href="<?=PATH?>web/icon/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="<?=PATH?>web/icon/favicon.png" rel="shortcut icon">

    <!--[if lt IE 9]>
        <script src="<?=PATH?>web/javascript/html5shiv.js"></script>
        <script src="<?=PATH?>web/javascript/respond.min.js"></script>
    <![endif]-->
	<link rel="stylesheet" type="text/css" href="<?= PATH ?>fancyapps/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<link rel="stylesheet" type="text/css" href="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<link rel="stylesheet" type="text/css" href="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}

		.fancybox-custom .fancybox-skin {
			max-width: 700px;
			margin: 0 auto;
		}
	</style>
</head>

<body class="header-sticky">
    <?=$this->load->view('inc_header');?>
    <section class="roll-row page-title page-about-alt">
        <div class="main-title parallax">
            <div class="page-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="span12">
                    <h1 class="title pull-center"><?=$Shops[0]->Title?></h1>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>
		<div class="page-nav">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="breadcrumbs">
                            <li class="nav-prev"><a href="<?=base_url()?>">Home</a></li>
                            <li class="nav-split"><a href="#"> > </a></li>
                            <li><a href="<?=base_url()?>shops">Shops</a></li>
                            <li class="nav-split"><a href="#"> > </a></li>
                            <li><a href="#"><?=$Shops[0]->Title?></a></li>
                        </ul>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
		</div>
    </section><!-- /.page-title -->

    <section class="roll-row product-details" style="padding:46px 0 29px 0;">
        <div class="container">
            <div class="row">
                <div class="span12">
                   <!-- /.proc-detail-left -->
                    <div class="proc-detail-center">
                        <div class="item-image">
                            <div class="image">
                                <a class="zoom fancybox-buttons" data-fancybox-group="button" href="<?=PATH?>upload/<?=$Shops[0]->Image?>"></a>
                                <img src="<?=PATH?>upload/<?=$Shops[0]->Image?>" alt="<?=$Shops[0]->Title?>" style="width:600px; height:600px;">
                            </div>
                            <a href="#" class="heart-product"></a>
                            Alternate Images
							<div class="image-small">
							<?php foreach($ShopsImages as $ShopsImages){?>
                                <div class="picture" data-image="<?=PATH?>upload/<?=$ShopsImages->Image?>"><img src="<?=PATH?>upload/<?=$ShopsImages->Image?>" alt="<?=$Shops[0]->Title?>" style="width:75px; height:75px"></div>
							<?php }?>
							</div>
                        </div>
                    </div><!-- /.proc-detail-center -->
                    <div class="proc-detail-right">
                            <h2 class="name"><?=$Shops[0]->Title?></h2>
                            <span class="price">US $<?= $Shops[0]->OtherText ?>/piece</span>
                        <?=$Shops[0]->Description?>
                        
                        <div class="btn-addcart">
                            <button onclick="add_cart('<?= $Shops[0]->IdPages ?>', '<?= $Shops[0]->OtherText ?>')" class="btn-order">ADD TO CART</button>
                        </div>

                        
                    </div><!-- /.proc-detail-right -->
                    
                </div><!-- /.span12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.product-details -->

    <?=$this->load->view('inc_footer');?>
    <?= $this->load->view('inc_footer_files'); ?>
	<!-- Add mousewheel plugin (this is optional) -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/lib/jquery.mousewheel-3.0.6.pack.js"></script>

                            <!-- Add fancyBox main JS and CSS files -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/source/jquery.fancybox.js?v=2.1.5"></script>

                            <!-- Add Button helper (this is optional) -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

                            <!-- Add Thumbnail helper (this is optional) -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

                            <!-- Add Media helper (this is optional) -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

                            <script type="text/javascript">
								$(document).ready(function() {
									/*
									 *  Simple image gallery. Uses default settings
									 */

									$('.fancybox').fancybox();

									/*
									 *  Different effects
									 */

									// Change title type, overlay closing speed
									$(".fancybox-effects-a").fancybox({
										helpers: {
											title : {
												type : 'outside'
											},
											overlay : {
												speedOut : 0
											}
										}
									});

									// Disable opening and closing animations, change title type
									$(".fancybox-effects-b").fancybox({
										openEffect  : 'none',
										closeEffect	: 'none',

										helpers : {
											title : {
												type : 'over'
											}
										}
									});

									// Set custom style, close if clicked, change title type and overlay color
									$(".fancybox-effects-c").fancybox({
										wrapCSS    : 'fancybox-custom',
										closeClick : true,

										openEffect : 'none',

										helpers : {
											title : {
												type : 'inside'
											},
											overlay : {
												css : {
													'background' : 'rgba(238,238,238,0.85)'
												}
											}
										}
									});

									// Remove padding, set opening and closing animations, close if clicked and disable overlay
									$(".fancybox-effects-d").fancybox({
										padding: 0,

										openEffect : 'elastic',
										openSpeed  : 150,

										closeEffect : 'elastic',
										closeSpeed  : 150,

										closeClick : true,

										helpers : {
											overlay : null
										}
									});

									/*
									 *  Button helper. Disable animations, hide close button, change title type and content
									 */

									$('.fancybox-buttons').fancybox({
										openEffect  : 'none',
										closeEffect : 'none',

										prevEffect : 'none',
										nextEffect : 'none',

										closeBtn  : false,

										helpers : {
											title : {
												type : 'inside'
											},
											buttons	: {}
										},

										afterLoad : function() {
											this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
										}
									});


									/*
									 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
									 */

									$('.fancybox-thumbs').fancybox({
										prevEffect : 'none',
										nextEffect : 'none',

										closeBtn  : false,
										arrows    : false,
										nextClick : true,

										helpers : {
											thumbs : {
												width  : 50,
												height : 50
											}
										}
									});

									/*
									 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
									*/
									$('.fancybox-media')
										.attr('rel', 'media-gallery')
										.fancybox({
											openEffect : 'none',
											closeEffect : 'none',
											prevEffect : 'none',
											nextEffect : 'none',

											arrows : false,
											helpers : {
												media : {},
												buttons : {}
											}
										});

									/*
									 *  Open manually
									 */

									$("#fancybox-manual-a").click(function() {
										$.fancybox.open('1_b.jpg');
									});

									$("#fancybox-manual-b").click(function() {
										$.fancybox.open({
											href : 'iframe.html',
											type : 'iframe',
											padding : 5
										});
									});

									$("#fancybox-manual-c").click(function() {
										$.fancybox.open([
											{
												href : '1_b.jpg',
												title : 'My title'
											}, {
												href : '2_b.jpg',
												title : '2nd title'
											}, {
												href : '3_b.jpg'
											}
										], {
											helpers : {
												thumbs : {
													width: 75,
													height: 50
												}
											}
										});
									});


								});
							</script>
   
</html>