<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title><?= $site_Info['Title'] ?> | Checkout</title>
        <meta name="description" content="<?= $site_Info['MetaDescription'] ?>">
        <meta name="keywords" content="<?= $site_Info['MetaKeywords'] ?>">
        <meta name="author" content="<?= base_url() ?>">
        <?= $this->load->view('inc_header_files'); ?>
    </head>

    <body class="header-sticky">
        <?= $this->load->view('inc_header'); ?>

        <section class="roll-row page-title page-about-alt">
            <div class="page-nav">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <ul class="breadcrumbs">
                                <li class="nav-prev"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="nav-split"><a href="#"> > </a></li>
                                <li><a href="#">Checkout</a></li>
                            </ul>
                        </div><!-- /.span12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div>
        </section><!-- /.page-title -->

        <section class="roll-row contact-page main-page">
            <div class="container">
                <div class="row">				<div class="span6">
                        <div class="comment-respond">
                            <h4 class="title">Checkout - Shipping Address</h4>
                            
                            <div class="alerts alert-error register_error" style="display: none;"></div>
                            <div class="alerts alert-notice register_success" style="display: none;"></div>
                                
							<form action="<?=base_url()?>home/submit"class="comment-form" method="post" name="_xclick">							
								<div class="input-wrap name">                                    
									<input type="text" size="14" value="" placeholder="First Name*" name="FirstName" required>       
									<div class="alerts alert-error FirstName" style="display: none;"></div>	
								</div>							
								<div class="input-wrap email">			
									<input type="text" size="14" value="" placeholder="Last Name*" name="LastName" required>	
									<div class="alerts alert-error LastName" style="display: none;"></div>						
								</div>													
								<div class="input-wrap name">                             
									<input type="text" size="14" value="" placeholder="Company (optional)" name="Company" >                              
									<div class="alerts alert-error Company" style="display: none;"></div>	
								</div>													
								<div class="input-wrap email">								
									<input type="number" size="14" value="" placeholder="Phone*" name="Phone" required>
									<div class="alerts alert-error Phone" style="display: none;"></div>		
								</div>													
									<div class="textarea-wrap">                                
									<textarea id="comments" name="Address" placeholder="Address*" class="" required></textarea>      
									<p style="text-align: left;color: red;display: none;" id="msg_error"></p>     
								</div>													
									<div class="input-wrap name">                                 
									<input type="text" size="14" value="" placeholder="City*" name="City" required>    
								<div class="alerts alert-error City" style="display: none;"></div>		
								</div>						
								<div class="input-wrap email">		
									<input type="text" size="14" value="" placeholder="Country*" name="Country" required>	
									<div class="alerts alert-error Country" style="display: none;"></div>		
								</div>																		
								<div class="submit-wrap">
									<button id="submitcomment" name="submit" class="btn-submit checkout_submit">
									Submit
									</button>
								</div>
							</form>
                        </div><!-- /.comment-respond -->
                    </div><!-- /.span -->
				
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section>

        <?= $this->load->view('inc_footer'); ?>

        <?= $this->load->view('inc_footer_files'); ?>
    </body>
</html>