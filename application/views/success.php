<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title><?= $site_Info['Title'] ?> | Success</title>
        <meta name="description" content="<?= $site_Info['MetaDescription'] ?>">
        <meta name="keywords" content="<?= $site_Info['MetaKeywords'] ?>">
        <meta name="author" content="<?= base_url() ?>">
        <?= $this->load->view('inc_header_files'); ?>
    </head>

    <body class="header-sticky">
        <?= $this->load->view('inc_header'); ?>

        <section class="roll-row page-title page-about-alt">
            <div class="page-nav">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <ul class="breadcrumbs">
                                <li class="nav-prev"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="nav-split"><a href="#"> > </a></li>
                                <li><a href="#">Success</a></li>
                            </ul>
                        </div><!-- /.span12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div>
        </section><!-- /.page-title -->

        <section class="roll-row person-order-items" style="padding: 69px 0 74px 0;">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <h1 class="title pull-center">Your order has been successfully processed.</h1>
						<center><?=$msg?></center>
						<br>
						<a href="<?=base_url()?>shops" class="span12 btn btn-info btn-lg"> Go Back Our Shops</a>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.person-order-items -->

        <?= $this->load->view('inc_footer'); ?>

        <?= $this->load->view('inc_footer_files'); ?>
    </body>
</html>