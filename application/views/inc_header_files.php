	<!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?=PATH?>web/stylesheets/bootstrap.css" >

    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?=PATH?>web/stylesheets/style.css">

    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?=PATH?>web/stylesheets/animate.css">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:500,600,700,100,800,900,400,200,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Sanchez:400italic,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!-- Favicon and touch icons  -->
    <link href="<?=PATH?>web/icon/apple-touch-icon-144-precomposed.png" rel="apple-touch-icon-precomposed" sizes="144x144">
    <link href="<?=PATH?>web/icon/apple-touch-icon-114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114">
    <link href="<?=PATH?>web/icon/apple-touch-icon-72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72">
    <link href="<?=PATH?>web/icon/apple-touch-icon-57-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="<?=PATH?>web/icon/favicon.png" rel="shortcut icon">

    <!--[if lt IE 9]>
        <script src="<?=PATH?>web/javascript/html5shiv.js"></script>
        <script src="<?=PATH?>web/javascript/respond.min.js"></script>
    <![endif]-->