<div class="span12">
                        <div class="per-order-items">
                            <table>
                                <tr class="order-head">
                                    <th class="order-image">PRODUCT</th>
                                    <th>&nbsp;</th>
                                    <th>PRICE</th>
                                    <th>QUANTITY</th>
                                    <th>&nbsp;</th>
                                    <th>TOTAL</th>
                                </tr>
                                <?php
                                $cart_check = $this->cart->contents();
                                if (count($cart_check) == 0) {
                                    echo '<tr >
                                            <td class="order-image" colspan="6">
                                                No Product In Cart
                                            </td>
                                        </tr>';
                                }
                                else 
                                {
                                    $total = 0;
                                    foreach ($cart_check as $item) {
                                        
                                        $total += $item['qty'] * $item['price'];
                                ?>
                                    <tr>
                                        <td class="order-image first-items">
                                            <img src="<?= PATH ?>upload/<?= $this->menu->product_image($item['id']) ?>" alt="images" style="height: 75%;width: 65%;">
                                        </td>
                                        <td class="product-name"><?= $this->menu->product_name($item['id']) ?></td>
                                        <td class="order-money">$ <?= $item['price'] ?></td>
                                        <td class="order-count">
                                            <button onclick="subtract_cart_item('<?= $item['rowid'] ?>')" class="order-minus icon-minus">&nbsp;</button>
                                            <span class="number-order"><?= $item['qty'] ?></span>
                                            <button onclick="add_cart_item('<?= $item['rowid'] ?>')" class="order-plus icon-plus">&nbsp;</button>
                                        </td>
                                        <td class="order-empty">&nbsp;</td>
                                        <td class="order-mtotal">$ <?= $item['qty'] * $item['price'] ?><button onclick="remove_cart_item('<?= $item['rowid'] ?>')" class="btn-order-remove">Remove</button></td>
                                    </tr>
                                <?php
                                    }
                                ?>
                                <tr>
                                    <td colspan="6">
                                        <div class="sum-order">
                                            <ul>
                                                <li class="sum-left total">Total</li>
                                                <li class="sum-right total">$ <?=$total?></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                                }
                                ?>
                            </table>
                        </div><!-- /.per-order-items -->
                        <a href="<?= base_url() ?>shops" class="shop-continue">CONTINUE SHOPPING</a>
                        <?php
                        if (count($cart_check) != 0) {
                        ?>
						<?php if($this->session->userdata('user_Login') == true) {?>
							<a href="checkout" class="read-more shop-order">CHECKOUT</a>
						<?php }else{?>
							<a href="<?=base_url()?>accounts?i=login" class="read-more shop-order">CHECKOUT</a>
                        <?php
							}
						}
                        ?>
                    </div>