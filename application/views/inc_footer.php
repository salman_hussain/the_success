    <section class="roll-row purchase">
        <div class="container">
            <div class="row">
                <div class="span12">
                    <h3>Interested in our services? Don’t hesitate to contact us!</h3>
                    <a href="<?=base_url()?>contact-us" class="read-more">LEARN MORE ABOUT US</a>
                </div><!-- /.span12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.purchase -->

    <footer class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="widget widget-recent-post">
                            <h5 class="title">Menu</h5>
                            <ul>
                                <li>
                                    <a href="<?=base_url()?>">Home</a>
                                </li>
                                <li>
                                    <a href="<?=base_url()?>about-us">About Us</a>
                                </li>
								<li>
                                    <a href="<?=base_url()?>contact-us">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div><!-- /.span3 -->
					<div class="span3">
                        <div class="widget widget-about">
                            <h5 class="title">About Us</h5>
                            <p><?=$site_Info['FooterText']?></p>
                        </div>
                    </div><!-- /.span3 -->
                    <div class="span6">
                        <div class="widget widget-newsletter">
                            <h5 class="title">Follow Up</h5>
                            <div id="fb-root"></div>
							<script>(function(d, s, id) {
							  var js, fjs = d.getElementsByTagName(s)[0];
							  if (d.getElementById(id)) return;
							  js = d.createElement(s); js.id = id;
							  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=684475521630979";
							  fjs.parentNode.insertBefore(js, fjs);
							}(document, 'script', 'facebook-jssdk'));</script>
							<div class="fb-page" data-href="https://www.facebook.com/MMSService/" data-tabs="timeline" data-height="170" data-small-header="true" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/MMSService/"><a href="https://www.facebook.com/MMSService/">MMS Services</a></blockquote></div></div>
                            
                        </div><!-- /.widget-newsletter -->
                    </div><!-- /.span3 -->
                </div><!-- /.roll-rows -->
            </div><!-- /.roll-container -->
        </div><!-- /.footer-top -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <div class="copyright"><?=$site_Info['Copyright']?>.</div><!-- /.copyright -->
                        <div class="link">
                            <a href="<?=base_url()?>">&nbsp;</a>

                        </div><!-- /.link -->
                        <div class="social">
							<?php if($site_Info['Twitter']) ?>
                            <a href="<?=$site_Info['Twitter']?>" target="_blank"><i class="icon-twitter-circled"></i></a>
                            <?php if($site_Info['Facebook']) ?>
						    <a href="<?=$site_Info['Facebook']?>" target="_blank"><i class="icon-facebook-circled"></i></a>
                            <?php if($site_Info['GooglePlus']) ?>
							<a href="<?=$site_Info['GooglePlus']?>" target="_blank"><i class="icon-gplus-circled"></i></a>
                            <?php if($site_Info['LinkedIn']) ?>
							<a href="<?=$site_Info['LinkedIn']?>" target="_blank"><i class="icon-linkedin-circled"></i></a>
                        </div><!-- /.social -->
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.footer-bottom -->
    </footer>

   <!-- Go Top -->
   <a class="go-top">
      <i class="icon-up-open"></i>
   </a>