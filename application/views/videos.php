<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
            <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
                <title><?=$site_Info['Title']?> | <?=$Pages[0]->Title?></title>
				<meta name="description" content="<?=$site_Info['MetaDescription']?>, <?=$Pages[0]->MetaDescription?>">
				<meta name="keywords" content="<?=$site_Info['MetaKeywords']?>, <?=$Pages[0]->MetaKeyword?>">
				<meta name="author" content="<?=base_url()?>">
				<?=$this->load->view('inc_header_files');?>
                       <link rel="stylesheet" type="text/css" href="<?= PATH ?>fancyapps/source/jquery.fancybox.css?v=2.1.5" media="screen" />
                        <link rel="stylesheet" type="text/css" href="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
                        <link rel="stylesheet" type="text/css" href="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
                        <style type="text/css">
							.fancybox-custom .fancybox-skin {
								box-shadow: 0 0 50px #222;
							}

							.fancybox-custom .fancybox-skin {
								max-width: 700px;
								margin: 0 auto;
							}
						</style>
						<?=$this->load->view('inc_header_files'); ?>
                        </head>

                        <body class="header-sticky">
                            <?= $this->load->view('inc_header'); ?>

                            <section class="roll-row page-title page-about-alt">
                                <div class="main-title parallax">
                                    <div class="page-overlay"></div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="span12">
                                                <h1 class="title pull-center"><?= $Pages[0]->Title ?></h1>
                                                <?= $Pages[0]->Description ?>
                                            </div><!-- /.span12 -->
                                        </div><!-- /.row -->
                                    </div><!-- /.container -->
                                </div>
                                <div class="page-nav">
                                    <div class="container">
                                        <div class="row">
                                            <div class="span12">
                                                <ul class="breadcrumbs">
                                                    <li class="nav-prev"><a href="<?= base_url() ?>">Home</a></li>
                                                    <li class="nav-split"><a href="#"> > </a></li>
                                                    <li><a href="#"><?= $Pages[0]->Title ?></a></li>
                                                </ul>
                                            </div><!-- /.span12 -->
                                        </div><!-- /.row -->
                                    </div><!-- /.container -->
                                </div>
                            </section><!-- /.page-title -->

							<section class="roll-row main-page">
								<div class="container">
									<div class="row">
										<div class="clearfix"></div>
										<div class="blog-container">
										  
										   <?php 
										function get_youtube($url) {

                                            $youtube = "http://www.youtube.com/oembed?url=" . $url . "&format=json";
                                            $curl = curl_init($youtube);
                                            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                                            $return = curl_exec($curl);
                                            curl_close($curl);
                                            return json_decode($return, true);
                                        }
										   foreach ($Videos as $videos) {
										    $URL = $videos->Description;
											$Details = get_youtube($URL);
										   ?>
										   <div class="span4 happy" style="height:475px;">
												<article class="post">
													<?php if($Details['thumbnail_url']){?>
													<a href="<?= $URL ?>" class="fancybox-media"><img src="<?= $Details['thumbnail_url'] ?>" alt="image" style="height:275px;width:500px"></a>
													<?php }else{?>
													<a href="<?= $URL ?>" class="fancybox-media"><img src="<?=PATH?>upload/No_available_image.gif" alt="image" style="height:275px;width:500px"></a>
													<?php }?>
													<h4 class="title-post" style="height:50px;"><a href="<?= $URL ?>"  class="fancybox-media"><?=$videos->Title?></a></h4>
													<div class="meta-post">
														<span class="date"> <?=date('d-M-Y',strtotime($videos->Created));?></span>
													</div>
													<div style="height:50px;">
													<?= $Details['author_name'] ?>
													</div>
													<a href="<?= $URL ?>" class="fancybox-media read-more">Play Now</a>
												</article><!-- /.post -->
											</div>
											<?php }?>
											
										</div><!-- /.blog-container --> 
									</div><!-- /.row -->
								</div><!-- /.container -->
							</section><!-- /.main-page -->

                            <?= $this->load->view('inc_footer'); ?>
                            <?= $this->load->view('inc_footer_files'); ?>

                            <!-- Add mousewheel plugin (this is optional) -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/lib/jquery.mousewheel-3.0.6.pack.js"></script>

                            <!-- Add fancyBox main JS and CSS files -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/source/jquery.fancybox.js?v=2.1.5"></script>

                            <!-- Add Button helper (this is optional) -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

                            <!-- Add Thumbnail helper (this is optional) -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

                            <!-- Add Media helper (this is optional) -->
                            <script type="text/javascript" src="<?= PATH ?>fancyapps/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

                            <script type="text/javascript">
								$(document).ready(function() {
									/*
									 *  Simple image gallery. Uses default settings
									 */

									$('.fancybox').fancybox();

									/*
									 *  Different effects
									 */

									// Change title type, overlay closing speed
									$(".fancybox-effects-a").fancybox({
										helpers: {
											title : {
												type : 'outside'
											},
											overlay : {
												speedOut : 0
											}
										}
									});

									// Disable opening and closing animations, change title type
									$(".fancybox-effects-b").fancybox({
										openEffect  : 'none',
										closeEffect	: 'none',

										helpers : {
											title : {
												type : 'over'
											}
										}
									});

									// Set custom style, close if clicked, change title type and overlay color
									$(".fancybox-effects-c").fancybox({
										wrapCSS    : 'fancybox-custom',
										closeClick : true,

										openEffect : 'none',

										helpers : {
											title : {
												type : 'inside'
											},
											overlay : {
												css : {
													'background' : 'rgba(238,238,238,0.85)'
												}
											}
										}
									});

									// Remove padding, set opening and closing animations, close if clicked and disable overlay
									$(".fancybox-effects-d").fancybox({
										padding: 0,

										openEffect : 'elastic',
										openSpeed  : 150,

										closeEffect : 'elastic',
										closeSpeed  : 150,

										closeClick : true,

										helpers : {
											overlay : null
										}
									});

									/*
									 *  Button helper. Disable animations, hide close button, change title type and content
									 */

									$('.fancybox-buttons').fancybox({
										openEffect  : 'none',
										closeEffect : 'none',

										prevEffect : 'none',
										nextEffect : 'none',

										closeBtn  : false,

										helpers : {
											title : {
												type : 'inside'
											},
											buttons	: {}
										},

										afterLoad : function() {
											this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
										}
									});


									/*
									 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
									 */

									$('.fancybox-thumbs').fancybox({
										prevEffect : 'none',
										nextEffect : 'none',

										closeBtn  : false,
										arrows    : false,
										nextClick : true,

										helpers : {
											thumbs : {
												width  : 50,
												height : 50
											}
										}
									});

									/*
									 *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
									*/
									$('.fancybox-media')
										.attr('rel', 'media-gallery')
										.fancybox({
											openEffect : 'none',
											closeEffect : 'none',
											prevEffect : 'none',
											nextEffect : 'none',

											arrows : false,
											helpers : {
												media : {},
												buttons : {}
											}
										});

									/*
									 *  Open manually
									 */

									$("#fancybox-manual-a").click(function() {
										$.fancybox.open('1_b.jpg');
									});

									$("#fancybox-manual-b").click(function() {
										$.fancybox.open({
											href : 'iframe.html',
											type : 'iframe',
											padding : 5
										});
									});

									$("#fancybox-manual-c").click(function() {
										$.fancybox.open([
											{
												href : '1_b.jpg',
												title : 'My title'
											}, {
												href : '2_b.jpg',
												title : '2nd title'
											}, {
												href : '3_b.jpg'
											}
										], {
											helpers : {
												thumbs : {
													width: 75,
													height: 50
												}
											}
										});
									});


								});
							</script>

                        </body>
                        </html>