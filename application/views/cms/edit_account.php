<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			Edit Account
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit Account</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				<div class="result-p">
				</div>
                <form role="form" class="form" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                   <?php
						$Get       = $this->uri->segment(2, 0);
						$GetUrl    = $this->uri->segment(3, 0);
						$IdAccount = $this->uri->segment(4, 0);
						
						
					?> 
				   <div class="row">
						<div class="col-xs-12">
						  <label for="Title">Full Name*</label>
						  <input type="hidden" class="form-control" id="IdAccount" name="IdAccount" placeholder="text here" value="<?=$Account[0]->IdAccount?>">					
						  <input type="text" class="form-control" id="FullName" name="FullName" placeholder="type here" required value="<?=$Account[0]->FullName?>">
						</div>
					</div>
                    		
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Email Address*</label>
						  <input type="email" class="form-control" id="EmailAddress" readonly name="EmailAddress" placeholder="type here" required value="<?=$Account[0]->EmailAddress?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Password*</label>
						  <input type="password" class="form-control" id="Password" name="Password" placeholder="type here" required value="<?php $key=''; echo rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($Account[0]->Password), MCRYPT_MODE_CBC, md5(md5($key))), "\0"); ?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Confirm Password*</label>
						  <input type="password" class="form-control" id="ConfirmPassword" name="ConfirmPassword" placeholder="type here" required value="">
						<a id="aTag" href="javascript:toggleAndChangeText();">
						  Show
						</a>
						<span id="divToToggle" style="display:none">
						<?php $key=''; echo rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($Account[0]->Password), MCRYPT_MODE_CBC, md5(md5($key))), "\0"); ?>
						</span>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Profile Image</label>
						  <input type="file" class="form-control" id="ProfileImage" name="ProfileImage[]" placeholder="type here">
						</div>
					</div>
					
					<?php if($Account[0]->ProfileImage){?>
					<br>
					<div class="row">
								<div class="col-md-2 center">
									<img src="<?=PATH?>upload/<?=$Account[0]->ProfileImage?>" width="100%" height="150"><br>
									<a href="#" class="btn btn-block btn-danger delete_image" data-id="<?=$Account[0]->IdAccount?>">Delete</a>
								</div>
					</div>
					<?php }?>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Phone Number</label>
						  <input type="text" class="form-control" id="PhoneNumber" name="PhoneNumber" placeholder="type here" value="<?=$Account[0]->PhoneNumber?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Website</label>
						  <input type="text" class="form-control" id="Website" name="Website" placeholder="type here" value="<?=$Account[0]->Website?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Twitter</label>
						  <input type="text" class="form-control" id="Twitter" name="Twitter" placeholder="type here" value="<?=$Account[0]->Twitter?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">LinkedIn</label>
						  <input type="text" class="form-control" id="LinkedIn" name="LinkedIn" placeholder="type here" value="<?=$Account[0]->LinkedIn?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Googleplus</label>
						  <input type="text" class="form-control" id="Googleplus" name="Googleplus" placeholder="type here" value="<?=$Account[0]->Googleplus?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">YouTube</label>
						  <input type="text" class="form-control" id="YouTube" name="YouTube" placeholder="type here" value="<?=$Account[0]->YouTube?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Instagram</label>
						  <input type="text" class="form-control" id="Instagram" name="Instagram" placeholder="type here" value="<?=$Account[0]->Instagram?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Pinterest</label>
						  <input type="text" class="form-control" id="Pinterest" name="Pinterest" placeholder="type here" value="<?=$Account[0]->Pinterest?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Account Type</label>
						    <select class="form-control select2" style="width: 100%;" name="IdAccountType" id="IdAccountType">
							  <option value="">Select</option>
							  <?php foreach($AccountType as $Accounttype){?>
									<option value="<?=$Accounttype->IdAccountType?>" <?php if($Account[0]->IdAccountType == $Accounttype->IdAccountType){echo "selected";}?>><?=$Accounttype->Type?></option>
							  <?php }?>
							</select>
						</div>
					</div>					
                 
				 
				  
				  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
              </div><!-- /.box -->              
            </div><!--/.col (left) -->
            <!-- right column -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
	  <script>
	  function toggleAndChangeText() {
			 $('#divToToggle').toggle();
			 if ($('#divToToggle').css('display') == 'none') {
				  $('#aTag').html('Show');
			 }
			 else {
				  $('#aTag').html('Hide');
			 }
	  }		
			window.onload = function () {
				document.getElementById("Password").onchange = validatePassword;
				document.getElementById("ConfirmPassword").onchange = validatePassword;
			}
			function validatePassword(){
			var pass2=document.getElementById("ConfirmPassword").value;
			var pass1=document.getElementById("Password").value;
				if(pass1!=pass2){
					document.getElementById("ConfirmPassword").setCustomValidity("Passwords Don't Match");
				}else{
					document.getElementById("ConfirmPassword").setCustomValidity('');	 
				//empty string means no validation error
				}
			}
			
			
		function CheckEmail(){
			var Email = $("#EmailAddress").val();
			
			if(Email.length > 2){
				$('#Loading').show();
				$.post("checkemail", {
					EmailAddress: $('#EmailAddress').val(),
				}, function(response){
					if(response == 1){
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'The Email is Already registered in our system...'+
										'</div>');
					 $("#EmailAddress").val("");					
				    $("#loading-div-background").hide();
					}else{
						$('.result-p').html('');
					}
				});
				return false;
			}
		}
	  $(document).ready(function($){
		  
			 $(".select2").select2();

			$('.form').submit(function(evt) {
				$("#loading-div-background").show();
				$(".result-p").html('Please Wait...');
                evt.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                type: 'POST',
                url:"<?=base_url()?>cms/account/do_edit",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Record Has Been Successfully Updated...'+
										'</div>');
				     $("#loading-div-background").hide();
				$( ".load_Page" ).load('<?=base_url()?>cms/account/edit/<?=$IdAccount?>?ajaxcall=1'); 
                },
                error: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Please Try Again..'+
										'</div>');
				    $("#loading-div-background").hide();						
                }
                });
            });
			
			$(".delete_image").click(function(){
			var confirm_delete = confirm("Are you sure you want to delete this?");			  
				    if (confirm_delete) {
						  $.ajax({
							url:'<?=base_url()?>cms/account/delete_image/' + this.getAttribute('data-id'),
							type: "GET",
							dataType:'json',
							success: function(data) {
							$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
							$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
													'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
													'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
													'Record Has Been Successfully Deleted...'+
													'</div>');
												$("#loading-div-background").hide();
							$( ".load_Page" ).load('<?=base_url()?>cms/account/edit/<?=$IdAccount?>?ajaxcall=1'); 					
							},
							error: function(data) {
								$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
								$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
														'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
														'Please Try Again..'+
													'</div>');
												$("#loading-div-background").hide();						
							}
						});
						 return false;
					}
			});
		});
	  </script>