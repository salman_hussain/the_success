<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			View Order Details
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url()?>cms"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View Order Details</li>
          </ol>
        </section>
        <!-- Main content -->
       <section class="invoice">
          <!-- title row -->
          <div class="row">
            <div class="col-xs-12">
              <h2 class="page-header">
                &nbsp;
                <small class="pull-right">Date: <?=$orders[0]->Created?></small>
              </h2>
            </div><!-- /.col -->
          </div>
          <!-- info row -->
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              <address>
                <strong>User Information</strong><br>
                <?=$orders[0]->FullName?><br>
                Phone: <?=$orders[0]->PhoneNumber?><br>
                Email: <?=$orders[0]->EmailAddress?>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <address>
                <strong>Shipping Address</strong><br>
                <?=$orders[0]->FirstName?>, <?=$orders[0]->LastName?><br>
                Address:   <?=$orders[0]->Address?><br>
                Phone:   <?=$orders[0]->Phone?><br>
                Company: <?=$orders[0]->Company?>
              </address>
            </div><!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <b>Invoice #<?=$orders[0]->IdOrders?></b><br>
              <b>Order ID:</b> <?=$orders[0]->OrderNo?><br>
              <b>Account:</b>  US $<?=$orders[0]->Total?>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- Table row -->
          <div class="row">
            <div class="col-xs-12 table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th width="2%">#</th>
                    <th width="65%">Product</th>
                    <th width="10%">Qty</th>
                    <th width="15%">Price</th>
					<th width="15%">Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                 <?php foreach($details as $details){?>
				 <tr>
                    <td>1</td>
					<td><?=$details->ItemName?></td>
                    <td><?=$details->Qty?></td>
                    <td>US $<?=$details->Price?></td>
					<td>US $<?=$details->Price*$details->Qty?></td>
                  </tr>
				 <?php }?>
                </tbody>
              </table>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
              <p class="lead">Payment Methods:</p>
               <img src="<?=PATH_ADMIN?>dist/img/credit/paypal2.png" alt="Paypal">
            </div><!-- /.col -->
            <div class="col-xs-6">
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <th style="width:50%">Subtotal:</th>
                    <td>US $<?=$orders[0]->Total?></td>
                  </tr>
                  <tr>
                    <th>Tax (0%)</th>
                    <td>US $0.00</td>
                  </tr>
                  <tr>
                    <th>Shipping:</th>
                    <td>US $0.00</td>
                  </tr>
                  <tr>
                    <th>Total:</th>
                    <td>US $<?=$orders[0]->Total?></td>
                  </tr>
                </table>
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->

          <!-- this row will not appear when printing -->
          <div class="row no-print">
			<div class="col-xs-12">
              <a href="<?=$GetIdOrders?>?i=Print" target="_blank" class="btn btn-default pull-right"><i class="fa fa-print"></i> Print</a>
            </div>
          </div>
        </section><!-- /.content -->
      </div>
	  <script>
	  $(document).ready(function($){
			$('.form').submit(function(evt) {
				$("#loading-div-background").show();
				$(".result-p").html('Please Wait...');
                evt.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                type: 'POST',
                url:"<?=base_url()?>cms/orders/do_edit_orders",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Record Has Been Successfully Added...'+
										'</div>');
				    $("#loading-div-background").hide();
                },
                error: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Please Try Again..'+
										'</div>');
				$("#loading-div-background").hide();						
                }
                });
            });
		});
	  </script>