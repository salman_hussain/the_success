	<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?=PATH?>upload/<?=$profile_Info['Pro_ProfileImage']?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?=ucwords($this->session->userdata('FullName'))?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
		<?php foreach ($menu as $row){?>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-folder"></i>
                <span><?=$row->Title?></span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>cms/<?=$row->SEO?>/add_new/<?=str_replace('=','',base64_encode($row->IdBuilder))?>" class="get_page"><i class="fa fa-circle-o"></i> Add New</a></li>
                <li><a href="<?=base_url()?>cms/<?=$row->SEO?>/view_all/<?=str_replace('=','',base64_encode($row->IdBuilder))?>" class="get_page"><i class="fa fa-circle-o"></i> View All</a></li>
              </ul>
            </li>
		<?php }?>			
			<li class="treeview">
              <a href="#">
                <i class="fa fa-shopping-cart"></i>
                <span>View All Orders</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>cms/orders/view_all_orders" class="get_page"><i class="fa fa-circle-o"></i> View All</a></li>
              </ul>
            </li>
			<li class="treeview">
              <a href="<?=base_url()?>cms/settings" class="get_page">
                <i class="fa fa-gears"></i> <span>Settings</span>
              </a>
            </li>
			
			<li class="treeview">
              <a href="<?=base_url()?>cms/home/logout">
                <i class="fa fa-sign-out"></i> <span>Sign Out</span>
              </a>
            </li>
			<li class="treeview">
              <a href="<?=base_url()?>" target="_blank">
                <i class="fa fa-television"></i> <span>Live Site</span>
              </a>
            </li>
		   <?php if($this->session->userdata('Account_Type') == 5){?>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Accounts</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>cms/account/add_new" class="get_page"><i class="fa fa-circle-o"></i> Add New</a></li>
                <li><a href="<?=base_url()?>cms/account/view_all" class="get_page"><i class="fa fa-circle-o"></i> View All</a></li>
              </ul>
            </li>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Account Type</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>cms/account_type/add_new_account_type" class="get_page"><i class="fa fa-circle-o"></i> Add New</a></li>
                <li><a href="<?=base_url()?>cms/account_type/view_all_account_type" class="get_page"><i class="fa fa-circle-o"></i> View All</a></li>
              </ul>
            </li>
			<li class="treeview">
              <a href="#">
                <i class="fa fa-building"></i>
                <span>Builder</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?=base_url()?>cms/builder/add_new_builder" class="get_page"><i class="fa fa-circle-o"></i> Add New</a></li>
                <li><a href="<?=base_url()?>cms/builder/view_all_builder" class="get_page"><i class="fa fa-circle-o"></i> View All</a></li>
              </ul>
            </li>
			<?php }?>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
		<div id="loading-div-background">
            <div id="loading-div" class="ui-corner-all" >
                <img style="height:80px;margin:10px;" src="<?=PATH_ADMIN?>loader.gif" alt="Loading.."/>
                <h2 style="color:#00A0EC;">Please wait....</h2>
            </div>
        </div>
	  <div class="load_Page">