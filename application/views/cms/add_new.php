<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			Add New <?=$PageSettings[0]->Title?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url()?>cms"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add New <?=$PageSettings[0]->Title?></li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				<div class="result-p">
				</div>
                <form role="form" class="form" name="form" id="form" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                    <?php
						$Get       = $this->uri->segment(2, 0);
						$GetUrl    = $this->uri->segment(3, 0);
						$IdBuilder = $this->uri->segment(4, 0);
					?> 
					<input type="hidden" class="form-control" id="IdBuilder" name="IdBuilder" placeholder="text here" value="<?=base64_decode($IdBuilder)?>">					
					<?php
					if($Get == "pages"){?>
						<div class="form-group">
						  <label for="Title">UnderPage</label>
						    <select class="form-control select2" style="width: 100%;" name="UnderPage" id="UnderPage">
							  <option value="0">Select</option>
							  <?php foreach($UnderPage as $underPage){?>
									<option value="<?=$underPage->IdPages?>"><?=$underPage->Title?></option>
							  <?php }?>
							</select>
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_1 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_1?></label>
						  <input type="text" class="form-control" id="Title" name="Title" placeholder="text here" required>
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_2 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_2?></label>
						  <input type="text" class="form-control" id="MetaKeyword" name="MetaKeyword" placeholder="text here">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_3 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_3?></label>
						  <input type="text" class="form-control" id="MetaDescription" name="MetaDescription" placeholder="text here">
						</div>
					<?php }?>
					
					
					<?php if($PageSettings[0]->Status_Label_4 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_4?></label>
						  <input type="text" class="form-control" <?php if($PageSettings[0]->Status_Label_14 == 1){?> id="FromAndTo" <?php }else{ ?> id="datepicker" <?php } ?> name="DatePicker" placeholder="text here">
						</div>
					<?php }?>
					
					
					<?php if($PageSettings[0]->Status_Label_5 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_5?></label>
						  <input type="text" class="form-control" id="OtherText" name="OtherText" placeholder="text here">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_6 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_6?></label>
						  <input type="file" class="form-control" id="UploadImage" name="UploadImage[]" <?php if($PageSettings[0]->Status_Label_12 == 1){?> multiple="" <?php }?> placeholder="text here">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_7 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_7?></label>
						  <input type="file" class="form-control" id="UploadFile" name="UploadFile[]" <?php if($PageSettings[0]->Status_Label_13 == 1){?> multiple="" <?php }?> placeholder="text here">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_8 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_8?></label>
						  <textarea id="Description" name="Description" class="form-control"></textarea>
						  <?php if($PageSettings[0]->Status_Label_11 == 1){?>
						  <script type="text/javascript">
							 CKEDITOR.replace( 'Description',
							{
							filebrowserBrowseUrl : '<?=PATH_ADMIN?>plugins/ckfinder/ckfinder.html',

							filebrowserImageBrowseUrl : '<?=PATH_ADMIN?>plugins/ckfinder/ckfinder.html?Type=Images',

							filebrowserFlashBrowseUrl : '<?=PATH_ADMIN?>plugins/ckfinder/ckfinder.html?Type=Flash',

							filebrowserUploadUrl : '<?=PATH_ADMIN?>plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

							filebrowserImageUploadUrl : '<?=PATH_ADMIN?>plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

							filebrowserFlashUploadUrl :'<?=PATH_ADMIN?>plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

							});
						 </script>
						  <?php }?>
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_9 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_9?></label>
						  <input type="text" class="form-control" id="ExternalURL" name="ExternalURL" placeholder="text here">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_10 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_10?></label>
						  <input type="text" class="form-control" id="OrderBy" name="OrderBy" placeholder="text here">
						</div>
					<?php }?>
					<?php
					if($Get == "pages"){?>
					<div class="form-group">
					  <label for="Title">Show on</label>
					  <div class="checkbox">
                        <label>
                          <input type="checkbox" value="1" name="Header">
                          Header
                        </label>
                      </div>

                      <div class="checkbox">
                        <label>
                          <input type="checkbox" value="1" name="Footer">
                          Footer
                        </label>
                      </div>					
					</div>
					<?php }?>
				
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->              
            </div><!--/.col (left) -->
            <!-- right column -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
	<script>
	  $(document).ready(function($){
		 <?php if($PageSettings[0]->Status_Label_12 == 1){?>
		  var file = document.getElementById('UploadImage');
				file.onchange = function(e){
					var ext = this.value.match(/\.([^\.]+)$/)[1];
					switch(ext)
					{
						case 'jpg':
						case 'JPG':
						case 'jpeg':
						case 'JPEG':
						case 'bmp':
						case 'BMP':
						case 'png':
						case 'GNG':
							break;
						default:
							alert('Upload Image Not allowed');
							this.value='';
					}
				};
		  <?php }?>		
		  <?php if($PageSettings[0]->Status_Label_13 == 1){?>
				var file = document.getElementById('UploadFile');

				file.onchange = function(e){
					var ext = this.value.match(/\.([^\.]+)$/)[1];
					switch(ext)
					{
						case 'docx':
						case 'doc':
						case 'pdf':
						case 'ppt':
						case 'pptx':
						case 'xls':
						case 'xlsx':
							break;
						default:
							alert('Upload File Not allowed');
							this.value='';
					}
				};
		  <?php }?>	
			$('.form').submit(function(evt) {
				$("#loading-div-background").show();
				$(".result-p").html('Please Wait...');
                evt.preventDefault();
				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}
                var formData = new FormData(this);
                $.ajax({
                type: 'POST',
                url:"<?=base_url()?>cms/pages/do_add_new",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
				success: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Record Has Been Successfully Added...'+
										'</div>');
				$("#loading-div-background").hide();
				  document.forms["form"].reset()
                },
                error: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Please Try Again..'+
										'</div>');
				$("#loading-div-background").hide();						
                }
                });
            });
			$('#datepicker').datepicker();
			$('#FromAndTo').daterangepicker();
		});
	  </script>