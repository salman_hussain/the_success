<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			Add New Account
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add New Account</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				<div class="result-p">
				</div>
                <form role="form" class="form" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                   
				   <div class="row">
						<div class="col-xs-12">
						  <label for="Title">Full Name*</label>
						  <input type="text" class="form-control" id="FullName" name="FullName" placeholder="type here" required>
						</div>
					</div>
                    		
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Email Address*</label>
						  <input type="email" class="form-control" id="EmailAddress" onBlur="return CheckEmail();" name="EmailAddress" placeholder="type here" required>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Password*</label>
						  <input type="password" class="form-control" id="Password" name="Password" placeholder="type here" required>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Confirm Password*</label>
						  <input type="password" class="form-control" id="ConfirmPassword" name="ConfirmPassword" placeholder="type here" required>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Profile Image</label>
						  <input type="file" class="form-control" id="ProfileImage" name="ProfileImage[]" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Phone Number</label>
						  <input type="text" class="form-control" id="PhoneNumber" name="PhoneNumber" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Website</label>
						  <input type="text" class="form-control" id="Website" name="Website" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Twitter</label>
						  <input type="text" class="form-control" id="Twitter" name="Twitter" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">LinkedIn</label>
						  <input type="text" class="form-control" id="LinkedIn" name="LinkedIn" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Googleplus</label>
						  <input type="text" class="form-control" id="Googleplus" name="Googleplus" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">YouTube</label>
						  <input type="text" class="form-control" id="YouTube" name="YouTube" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Instagram</label>
						  <input type="text" class="form-control" id="Instagram" name="Instagram" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Pinterest</label>
						  <input type="text" class="form-control" id="Pinterest" name="Pinterest" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
						  <label for="Title">Account Type</label>
						    <select class="form-control select2" style="width: 100%;" name="IdAccountType" id="IdAccountType" required>
							  <option value="">Select</option>
							  <?php foreach($AccountType as $Accounttype){?>
									<option value="<?=$Accounttype->IdAccountType?>"><?=$Accounttype->Type?></option>
							  <?php }?>
							</select>
						</div>
					</div>					
                 
				 
				  
				  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->              
            </div><!--/.col (left) -->
            <!-- right column -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
	  <script>
	  
			window.onload = function () {
				document.getElementById("Password").onchange = validatePassword;
				document.getElementById("ConfirmPassword").onchange = validatePassword;
			}
			function validatePassword(){
			var pass2=document.getElementById("ConfirmPassword").value;
			var pass1=document.getElementById("Password").value;
				if(pass1!=pass2){
					document.getElementById("ConfirmPassword").setCustomValidity("Passwords Don't Match");
				}else{
					document.getElementById("ConfirmPassword").setCustomValidity('');	 
				//empty string means no validation error
				}
			}
			
			
		function CheckEmail(){
			var Email = $("#EmailAddress").val();
			
			if(Email.length > 2){
				$('#Loading').show();
				$.post("checkemail", {
					EmailAddress: $('#EmailAddress').val(),
				}, function(response){
					if(response == 1){
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'The Email is Already registered in our system...'+
										'</div>');
					 $("#EmailAddress").val("");					
				    $("#loading-div-background").hide();
					}else{
						$('.result-p').html('');
					}
				});
				return false;
			}
		}
	  $(document).ready(function($){
		  
			 $(".select2").select2();

			$('.form').submit(function(evt) {
				$("#loading-div-background").show();
				$(".result-p").html('Please Wait...');
                evt.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                type: 'POST',
                url:"<?=base_url()?>cms/account/do_add_new_account",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Record Has Been Successfully Added...'+
										'</div>');
				     $("#loading-div-background").hide();
				$( ".load_Page" ).load('<?=base_url()?>cms/account/add_new?ajaxcall=1'); 
                },
                error: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Please Try Again..'+
										'</div>');
				    $("#loading-div-background").hide();						
                }
                });
            });
		});
	  </script>