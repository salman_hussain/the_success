<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			Edit <?=$PageSettings[0]->Title?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url()?>cms"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit <?=$PageSettings[0]->Title?></li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				<div class="result-p">
				</div>
                <form role="form" class="form" name="form" id="form" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                    <?php
						$Get       = $this->uri->segment(2, 0);
						$GetUrl    = $this->uri->segment(3, 0);
						$IdBuilder = $this->uri->segment(4, 0);
						
						$explode    = explode('_', $IdBuilder);
						$IdBuilder  = $explode[0];
						$IdPages    = $explode[1];
					?> 
					<input type="hidden" class="form-control" id="IdPages" name="IdPages" placeholder="text here" value="<?=base64_decode($IdPages)?>">					
					<input type="hidden" class="form-control" id="IdBuilder" name="IdBuilder" placeholder="text here" value="<?=base64_decode($IdBuilder)?>">					
					<?php
					if($Get == "pages"){?>
						<div class="form-group">
						  <label for="Title">UnderPage</label>
						    <select class="form-control select2" style="width: 100%;" name="UnderPage" id="UnderPage">
							  <option value="0">Select</option>
							  <?php foreach($UnderPage as $underPage){?>
									<option value="<?=$underPage->IdPages?>" <?php if($EditPages[0]->UnderPage == $underPage->IdPages){echo "selected";}?> ><?=$underPage->Title?></option>
							  <?php }?>
							</select>
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_1 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_1?></label>
						  <input type="text" class="form-control" id="Title" name="Title" placeholder="text here" required value="<?=$EditPages[0]->Title?>">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_2 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_2?></label>
						  <input type="text" class="form-control" id="MetaKeyword" name="MetaKeyword" placeholder="text here" value="<?=$EditPages[0]->MetaKeyword?>">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_3 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_3?></label>
						  <input type="text" class="form-control" id="MetaDescription" name="MetaDescription" placeholder="text here" value="<?=$EditPages[0]->MetaDescription?>">
						</div>
					<?php }?>
					
					
					<?php if($PageSettings[0]->Status_Label_4 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_4?></label>
						  <input type="text" class="form-control" <?php if($PageSettings[0]->Status_Label_14 == 1){?> id="FromAndTo" <?php }else{ ?> id="datepicker" <?php } ?> name="DatePicker" placeholder="text here" value="<?=$EditPages[0]->DatePicker?>">
						</div>
					<?php }?>
					
					
					<?php if($PageSettings[0]->Status_Label_5 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_5?></label>
						  <input type="text" class="form-control" id="OtherText" name="OtherText" placeholder="text here" value="<?=$EditPages[0]->OtherText?>">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_6 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_6?></label>
						  <input type="file" class="form-control" id="UploadImage" name="UploadImage[]" <?php if($PageSettings[0]->Status_Label_12 == 1){?> multiple="" <?php }?> placeholder="text here">
							  <br>
							  <div class="row">
							  <?php foreach($Images as $images){
								  if($images->IdPages == $EditPages[0]->IdPages){?>
								<div class="col-md-2 center">
									<img src="<?=PATH?>upload/<?=$images->Image?>" width="100%" height="150"><br>
									<a href="#" class="btn btn-block btn-danger delete_image" data-id="<?=$images->IdImages?>">Delete</a>
								</div>
							  <?php }
							  }?>
							  </div>
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_7 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_7?></label>
						  <input type="file" class="form-control" id="UploadFile" name="UploadFile[]" <?php if($PageSettings[0]->Status_Label_13 == 1){?> multiple="" <?php }?> placeholder="text here">
						    <br>
						    <div class="row">
							<?php foreach($File as $file){
									if($file->IdPages == $EditPages[0]->IdPages){
							?>
							<div class="col-md-2 center">
							<a href="<?=PATH?>upload/<?=$file->File?>"><?=$file->File?></a>
							<a href="#" class="btn btn-block btn-danger delete_file" data-id="<?=$file->IdFiles?>">Delete</a>
							</div>
							<?php }
							}?>
							</div>
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_8 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_8?></label>
						  <textarea id="Description" name="Description" class="form-control"><?=$EditPages[0]->Description?></textarea>
						  <?php if($PageSettings[0]->Status_Label_11 == 1){?>
						  <script type="text/javascript">
							 CKEDITOR.replace( 'Description',
							{
							filebrowserBrowseUrl : '<?=PATH_ADMIN?>plugins/ckfinder/ckfinder.html',

							filebrowserImageBrowseUrl : '<?=PATH_ADMIN?>plugins/ckfinder/ckfinder.html?Type=Images',

							filebrowserFlashBrowseUrl : '<?=PATH_ADMIN?>plugins/ckfinder/ckfinder.html?Type=Flash',

							filebrowserUploadUrl : '<?=PATH_ADMIN?>plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

							filebrowserImageUploadUrl : '<?=PATH_ADMIN?>plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

							filebrowserFlashUploadUrl :'<?=PATH_ADMIN?>plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'

							});
						 </script>
						  <?php }?>
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_9 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_9?></label>
						  <input type="text" class="form-control" id="ExternalURL" name="ExternalURL" placeholder="text here" value="<?=$EditPages[0]->ExternalURL?>">
						</div>
					<?php }?>
					
					<?php if($PageSettings[0]->Status_Label_10 == 1){?>
						<div class="form-group">
						  <label for="Title"><?=$PageSettings[0]->Label_10?></label>
						  <input type="text" class="form-control" id="OrderBy" name="OrderBy" placeholder="text here" value="<?=$EditPages[0]->OrderBy?>">
						</div>
					<?php }?>
					<?php
					if($Get == "pages"){?>
					<div class="form-group">
					  <label for="Title">Show on</label>
					  <div class="checkbox">
                        <label>
                          <input type="checkbox" value="1" name="Header" <?php if($EditPages[0]->Header == "1"){echo "checked";}?>>
                          Header
                        </label>
                      </div>

                      <div class="checkbox">
                        <label>
                          <input type="checkbox" value="1" name="Footer" <?php if($EditPages[0]->Footer == "1"){echo "checked";}?>>
                          Footer
                        </label>
                      </div>					
					</div>
					<?php }?>
				
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
              </div><!-- /.box -->              
            </div><!--/.col (left) -->
            <!-- right column -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
	<script>
	  
	  $(document).ready(function($){
		  <?php if($PageSettings[0]->Status_Label_12 == 1){?>
		  var file = document.getElementById('UploadImage');
				file.onchange = function(e){
					var ext = this.value.match(/\.([^\.]+)$/)[1];
					switch(ext)
					{
						case 'jpg':
						case 'JPG':
						case 'jpeg':
						case 'JPEG':
						case 'bmp':
						case 'BMP':
						case 'png':
						case 'GNG':
							break;
						default:
							alert('Upload Image Not allowed');
							this.value='';
					}
				};
		  <?php }?>		
		  <?php if($PageSettings[0]->Status_Label_13 == 1){?>
				var file = document.getElementById('UploadFile');

				file.onchange = function(e){
					var ext = this.value.match(/\.([^\.]+)$/)[1];
					switch(ext)
					{
						case 'docx':
						case 'doc':
						case 'pdf':
						case 'ppt':
						case 'pptx':
						case 'xls':
						case 'xlsx':
							break;
						default:
							alert('Upload File Not allowed');
							this.value='';
					}
				};
		  <?php }?>		
			$('.form').submit(function(evt) {
				$("#loading-div-background").show();
				$(".result-p").html('Please Wait...');
                evt.preventDefault();
				for (instance in CKEDITOR.instances) {
					CKEDITOR.instances[instance].updateElement();
				}
                var formData = new FormData(this);
                $.ajax({
				type: 'POST',
                url:"<?=base_url()?>cms/<?=$Get?>/do_edit_pages",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
				success: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Record Has Been Successfully Updated...'+
										'</div>');
				$("#loading-div-background").hide();
				$( ".load_Page" ).load('<?=base_url()?>cms/<?=$Get?>/edit_pages/<?=$IdBuilder."_".$IdPages?>?ajaxcall=1'); 
                },
                error: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Please Try Again..'+
										'</div>');
				$("#loading-div-background").hide();						
                }
                });
            });
			$(".delete_image").click(function(){
			var confirm_delete = confirm("Are you sure you want to delete this?");			  
				    if (confirm_delete) {
						  $.ajax({
							url:'<?=base_url()?>cms/pages/delete_image/' + this.getAttribute('data-id'),
							type: "GET",
							dataType:'json',
							success: function(data) {
							$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
							$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
													'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
													'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
													'Record Has Been Successfully Deleted...'+
													'</div>');
												$("#loading-div-background").hide();
							$( ".load_Page" ).load('<?=base_url()?>cms/pages/edit_pages/<?=$IdBuilder."_".$IdPages?>?ajaxcall=1'); 					
							},
							error: function(data) {
								$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
								$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
														'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
														'Please Try Again..'+
													'</div>');
												$("#loading-div-background").hide();						
							}
						});
						 return false;
					}
		});
			$(".delete_file").click(function(){
			var confirm_delete = confirm("Are you sure you want to delete this?");			  
				    if (confirm_delete) {
						  $.ajax({
							url:'<?=base_url()?>cms/pages/delete_file/' + this.getAttribute('data-id'),
							type: "GET",
							dataType:'json',
							success: function(data) {
							$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
							$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
													'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
													'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
													'Record Has Been Successfully Deleted...'+
													'</div>');
												$("#loading-div-background").hide();
							$( ".load_Page" ).load('<?=base_url()?>cms/pages/edit_pages/<?=$IdBuilder."_".$IdPages?>?ajaxcall=1'); 					
							},
							error: function(data) {
								$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
								$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
														'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
														'Please Try Again..'+
													'</div>');
												$("#loading-div-background").hide();						
							}
						});
						 return false;
					}
		});
			$('#datepicker').datepicker();
			$('#FromAndTo').daterangepicker();
		});
	  </script>