 </div>
	  <footer class="main-footer">
        <?=$site_Info['Copyright']?>
      </footer>
    </div><!-- ./wrapper -->
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=PATH_ADMIN?>bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
    <script src="<?=PATH_ADMIN?>plugins/select2/select2.full.min.js"></script>
    <!-- FastClick -->
    <script src="<?=PATH_ADMIN?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=PATH_ADMIN?>dist/js/app.min.js"></script>
    <!-- Sparkline -->
    <script src="<?=PATH_ADMIN?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?=PATH_ADMIN?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?=PATH_ADMIN?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	    <!-- DataTables -->
    <script src="<?=PATH_ADMIN?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=PATH_ADMIN?>plugins/datatables/dataTables.bootstrap.min.js"></script>
	<script src="<?=PATH_ADMIN?>plugins/daterangepicker/daterangepicker.js"></script>

    <!-- SlimScroll 1.3.0 -->
    <script src="<?=PATH_ADMIN?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- ChartJS 1.0.1 -->
	<script src="<?=PATH_ADMIN?>plugins/chartjs/Chart.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=PATH_ADMIN?>dist/js/demo.js"></script>

	  <script>
	  $(".get_page").click(function(){
		$("#loading-div-background").show();
		$(".load_Page").empty();
		history.pushState(null, document.title, this.getAttribute('href'));
        page = (this.getAttribute('href'));
		$( ".load_Page" ).load(page+'?ajaxcall=1'); 
		$("#loading-div-background").hide();
		return false;
      });
	  </script>
  </body>
</html>