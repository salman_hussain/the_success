<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			Settings
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url()?>builder"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Settings</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <!-- form start -->
				<div class="result-p">
				</div>
                <form role="form" class="form" method="post" enctype="multipart/form-data">
                  	<div class="box-header with-border">
					  <h3 class="box-title">Edit Information</h3>
					</div><!-- /.box-header -->

				  <div class="box-body">
				
				   <div class="row">
						<div class="col-xs-12">
						  <label for="Title">Application Title</label>
						  <input type="text" class="form-control" id="Title" name="Title" value="<?=$Settings[0]->Title?>" placeholder="Enter Title" required>
						  <input type="hidden" class="form-control" id="IdSettings" name="IdSettings" value="<?=$Settings[0]->IdSettings?>" placeholder="Enter Title" required>
						</div>
					</div>
                    
					<div class="row">
						<div class="col-xs-12">
							<label for="MetaKeyword">Meta Keyword</label>
							<input type="text" class="form-control" id="MetaKeyword" name="MetaKeyword" value="<?=$Settings[0]->MetaKeyword?>" placeholder="type here">
						</div>
					</div>
				
					<div class="row">
						<div class="col-xs-12">
							<label for="MetaDescription">Meta Description</label>
							<input type="text" class="form-control" id="MetaDescription" name="MetaDescription" value="<?=$Settings[0]->MetaDescription?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							  <label for="Logo">Logo</label>
							 <input type="file" class="form-control" id="Logo" name="Logo[]">
							 <br>
							  <div class="row">
							  <?php
								  if($Settings[0]->Logo){?>
								<div class="col-md-2 center">
									<img src="<?=PATH?>upload/<?=$Settings[0]->Logo?>" width="100%" height="150"><br>
									<a href="#" class="btn btn-block btn-danger delete_image" data-id="<?=$Settings[0]->IdSettings?>">Delete</a>
								</div>
							  <?php
							  }?>
							  </div>
						</div>
					</div>
					
				</div>	
				
				<div class="box-header with-border">
					  <h3 class="box-title">Email Setup</h3>
				</div><!-- /.box-header -->
					
				<div class="box-body">
					<div class="row">
						<div class="col-xs-12">
							<label for="EmailTo">Email To</label>
							<input type="text" class="form-control" id="EmailTo" name="EmailTo" value="<?=$Settings[0]->EmailTo?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="EmailFrom">EmailFrom</label>
							<input type="text" class="form-control" id="EmailFrom" name="EmailFrom" value="<?=$Settings[0]->EmailFrom?>" placeholder="type here">
						</div>
					</div>
				</div>	
				
				<div class="box-header with-border">
					  <h3 class="box-title">Contact Information</h3>
				</div><!-- /.box-header -->
				
				<div class="box-body">	
					<div class="row">
						<div class="col-xs-12">
							<label for="Mobile">Mobile</label>
							<input type="text" class="form-control" id="Mobile" name="Mobile" value="<?=$Settings[0]->Mobile?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="Phone">Phone</label>
							<input type="text" class="form-control" id="Phone" name="Phone" value="<?=$Settings[0]->Phone?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="Address">Address</label>
							<input type="text" class="form-control" id="Address" name="Address" value="<?=$Settings[0]->Address?>" placeholder="type here">
						</div>
					</div>
				</div>
				
				<div class="box-header with-border">
					  <h3 class="box-title">Other Information</h3>
				</div><!-- /.box-header -->				
				
				<div class="box-body">	
					
					<div class="row">
						<div class="col-xs-12">
							<label for="HeaderText">Header Text</label>
							<input type="text" class="form-control" id="HeaderText" name="HeaderText" value="<?=$Settings[0]->HeaderText?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="FooterText">Footer Text</label>
							<input type="text" class="form-control" id="FooterText" name="FooterText" value="<?=$Settings[0]->FooterText?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="Copyright">Copyright</label>
							<input type="text" class="form-control" id="Copyright" name="Copyright" value="<?=$Settings[0]->Copyright?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="Copyright">Text 1</label>
							<input type="text" class="form-control" id="Text_1" name="Text_1" value="<?=$Settings[0]->Text_1?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="Copyright">Text 2</label>
							<input type="text" class="form-control" id="Text_2" name="Text_2" value="<?=$Settings[0]->Text_2?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="Copyright">Text 3</label>
							<input type="text" class="form-control" id="Text_3" name="Text_3" value="<?=$Settings[0]->Text_3?>" placeholder="type here">
						</div>
					</div>
					
				</div>
				
				<div class="box-header with-border">
					  <h3 class="box-title">Social Media Information</h3>
				</div><!-- /.box-header -->				
				
				<div class="box-body">	
					
					<div class="row">
						<div class="col-xs-12">
							<label for="Facebook">Facebook</label>
							<input type="text" class="form-control" id="Facebook" name="Facebook" value="<?=$Settings[0]->Facebook?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="Twitter">Twitter</label>
							<input type="text" class="form-control" id="Twitter" name="Twitter" value="<?=$Settings[0]->Twitter?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="GooglePlus">GooglePlus</label>
							<input type="text" class="form-control" id="GooglePlus" name="GooglePlus" value="<?=$Settings[0]->GooglePlus?>" placeholder="type here">
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<label for="LinkedIn">LinkedIn</label>
							<input type="text" class="form-control" id="LinkedIn" name="LinkedIn" value="<?=$Settings[0]->LinkedIn?>" placeholder="type here">
						</div>
					</div>
					
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->              
            </div><!--/.col (left) -->
            <!-- right column -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
	  <script>
	  $(document).ready(function($){
			$('.form').submit(function(evt) {
				$("#loading-div-background").show();
				$(".result-p").html('Please Wait...');
                evt.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                type: 'POST',
                url:"<?=base_url()?>cms/settings/do_edit_settings",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Record Has Been Successfully Updated...'+
										'</div>');
				$( ".load_Page" ).load('<?=base_url()?>cms/settings?ajaxcall=1'); 
				$("#loading-div-background").hide();
				},
                error: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Please Try Again..'+
										'</div>');
				$("#loading-div-background").hide();						
                }
                });
            });
			
			$(".delete_image").click(function(){
			var confirm_delete = confirm("Are you sure you want to delete this?");			  
				    if (confirm_delete) {
						  $.ajax({
							url:'<?=base_url()?>cms/settings/delete_image/' + this.getAttribute('data-id'),
							type: "GET",
							dataType:'json',
							success: function(data) {
							$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
							$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
													'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
													'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
													'Record Has Been Successfully Deleted...'+
													'</div>');
												$("#loading-div-background").hide();
							$( ".load_Page" ).load('<?=base_url()?>cms/settings?ajaxcall=1'); 					
							},
							error: function(data) {
								$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
								$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
														'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
														'Please Try Again..'+
													'</div>');
												$("#loading-div-background").hide();						
							}
						});
						 return false;
					}
			});
		});
	  </script>