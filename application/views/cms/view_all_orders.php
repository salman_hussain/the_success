<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			View All Orders
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?=base_url()?>cms"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">View All Orders</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">View All</h3>
                </div><!-- /.box-header -->
				<div class="result-p">
				</div>
                <div class="box-body">
                  <table id="Json" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>#</th>
						<th>FullName</th>
						<th>OrderNo</th>
                        <th>EmailAddress</th>
						<th>Phone</th>
						<th>Total</th>
						<th>Created</th>
						<th>Status</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    
                    <tfoot>
                      <tr>
                        <th>#</th>
						<th>FullName</th>
						<th>OrderNo</th>
                        <th>EmailAddress</th>
						<th>Phone</th>
						<th>Total</th>
						<th>Created</th>
						<th>Status</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div>
	  	  <script>
	   var table;
	    $("body").delegate(".get_page", "click", function(){ 
		$("#loading-div-background").show();
		$(".load_Page").empty();
		history.pushState(null, document.title, this.getAttribute('href'));
        page = (this.getAttribute('href'));
		$( ".load_Page" ).load(page+'?ajaxcall=1'); 
		$("#loading-div-background").hide();
		return false;
      }); 
	   /* Table load */
		$(document).ready(function(){
			table = $('#Json').DataTable({
			"ordering": true,
			"info":     true,
			"paging":   true,
			"ajax": "<?=base_url()?>cms/orders/get_Json",
			"aLengthMenu": [[15, 25, 50, 100, 200], [15, 25, 50, 100, 200]],
			"iDisplayLength": 100,
			"order": [[ 0, "asc" ]],
			"aoColumnDefs": [ {
                "aTargets": [ 8 ],
                        "mData": "download_link",
                        "mRender": function (data, type, full) {
							var action =  '<div class="btn-group">'+
										  '<a href="<?=base_url()?>cms/orders/edit_orders/'+ full[8] +'" class="btn btn-default get_page"><i class="fa fa-list-alt"></i></a>'+
										  '<a href="#" data-id="'+ full[8] +'" class="btn btn-danger delete"><i class="glyphicon glyphicon-remove-circle"></i></a>';
								action += '</div>';
								if(full[3] != ''){
									 return action;	
								}else{
									 return action = '';	
								}			
                        }
                }
                ]
			});
		  });
		 $("body").delegate(".delete", "click", function(){
			var confirm_delete = confirm("Are you sure you want to delete this?");			  
				    if (confirm_delete) {
						  $.ajax({
							url:'<?=base_url()?>cms/orders/delete/' + this.getAttribute('data-id'),
							type: "GET",
							dataType:'json',
							success: function(data) {
							$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
							$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
													'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
													'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
													'Record Has Been Successfully Deleted...'+
													'</div>');
												table.ajax.reload();
												$("#loading-div-background").hide();
							},
							error: function(data) {
								$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
								$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
														'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
														'Please Try Again..'+
													'</div>');
												$("#loading-div-background").hide();						
							}
						});
						 return false;
					}
		});
		$("body").delegate(".status", "click", function(){
						  $.ajax({
							url:'<?=base_url()?>cms/orders/status/' + this.getAttribute('data-id'),
							type: "GET",
							dataType:'json',
							success: function(data) {
							$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
							$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
													'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
													'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
													'Status Has Been Successfully Updated...'+
													'</div>');
												table.ajax.reload();
												$("#loading-div-background").hide();
							},
							error: function(data) {
								$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
								$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
														'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
														'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
														'Please Try Again..'+
													'</div>');
												$("#loading-div-background").hide();						
							}
						});
						 return false;
		});
	  </script>