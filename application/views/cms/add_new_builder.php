<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
			Add New Slider
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
				<div class="result-p">
				</div>
                <form role="form" class="form" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                    <div class="row">
						<div class="col-xs-12">
						  <label for="Title">Section (Title, Name)</label>
						  <input type="text" class="form-control" id="Title" name="Title" placeholder="Enter Title" required>
						</div>
					</div>
                    
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Title</label>
							<input type="text" class="form-control" id="Label_1" name="Label_1" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_1" id="Status_Label_1">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Meta Keyword</label>
							<input type="text" class="form-control" id="Label_2" name="Label_2" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_2" id="Status_Label_2">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Meta Description </label>
							<input type="text" class="form-control" id="Label_3" name="Label_3" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_3" id="Status_Label_3">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Date Picker</label>
							<input type="text" class="form-control" id="Label_4" name="Label_4" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_4" id="Status_Label_4">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Other Text</label>
							<input type="text" class="form-control" id="Label_5" name="Label_5" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_5" id="Status_Label_5">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Upload Image (jpg, png)</label>
							<input type="text" class="form-control" id="Label_6" name="Label_6" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_6" id="Status_Label_6">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Upload File (docx, pdf)</label>
							<input type="text" class="form-control" id="Label_7" name="Label_7" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_7" id="Status_Label_7">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Description</label>
							<input type="text" class="form-control" id="Label_8" name="Label_8" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_8" id="Status_Label_8">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">External URL, PDF URL</label>
							<input type="text" class="form-control" id="Label_9" name="Label_9" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_9" id="Status_Label_9">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-9">
							<label for="Description">Order By</label>
							<input type="text" class="form-control" id="Label_10" name="Label_10" placeholder="Enter Label Text">
						</div>
						<div class="col-xs-3">
							<label>Status</label>
							  <select class="form-control" name="Status_Label_10" id="Status_Label_10">
								<option value="1">Enable</option>
								<option value="0" selected>Disable</option>
							  </select>
						</div>
					</div>
					
					<div class="row">
						<div class="col-xs-2">
							<label for="Status_Label_11">Enable Text Editor</label>
							  <select class="form-control" name="Status_Label_11" id="Status_Label_11">
								<option value="1">Yes</option>
								<option value="0" selected>No</option>
							  </select>
						</div>
						
						<div class="col-xs-2">
							<label for="Status_Label_12">Enable Multiple Upload Images</label>
							  <select class="form-control" name="Status_Label_12" id="Status_Label_12">
								<option value="1">Yes</option>
								<option value="0" selected>No</option>
							  </select>
						</div>
						
						<div class="col-xs-2">
							<label for="Status_Label_13">Enable Multiple Upload Files</label>
							  <select class="form-control" name="Status_Label_13" id="Status_Label_13">
								<option value="1">Yes</option>
								<option value="0" selected>No</option>
							  </select>
						</div>
						
						<div class="col-xs-2">
							<label for="Status_Label_14">Enable From & To Date Picker</label>
							  <select class="form-control" name="Status_Label_14" id="Status_Label_14">
								<option value="1">Yes</option>
								<option value="0" selected>No</option>
							  </select>
						</div>
						
						<div class="col-xs-2" style="display:none">
							<label for="Status_Label_15">Enable Categories</label>
							  <select class="form-control" name="Status_Label_15" id="Status_Label_15">
								<option value="1">Yes</option>
								<option value="0" selected>No</option>
							  </select>
						</div>
					</div>
					
					
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->              
            </div><!--/.col (left) -->
            <!-- right column -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div>
	  <script>
	  $(document).ready(function($){
			$('.form').submit(function(evt) {
				$("#loading-div-background").show();
				$(".result-p").html('Please Wait...');
                evt.preventDefault();
                var formData = new FormData(this);
                $.ajax({
                type: 'POST',
                url:"<?=base_url()?>cms/builder/do_add_new_builder",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-success alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Record Has Been Successfully Added...'+
										'</div>');
				$("#loading-div-background").hide();
				$( ".load_Page" ).load('<?=base_url()?>cms/builder/view_all_builder?ajaxcall=1'); 
                },
                error: function(data) {
					$('html, body').animate({ scrollTop: $('.box-title').offset().top }, 'slow');
					$('.result-p').html('<div class="alert alert-danger alert-dismissable">' + 
											'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
											'<h4>	<i class="icon fa fa-check"></i> Alert!</h4>' +
											'Please Try Again..'+
										'</div>');
				$("#loading-div-background").hide();						
                }
                });
            });
		});
	  </script>