<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$site_Info['Title']?> <?php  if(!isset($_GET['i'])) {?>| <?=$P_title?><?php } ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=PATH_ADMIN?>bootstrap/css/bootstrap.min.css">
	<!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- DataTables -->
    <link rel="stylesheet" href="<?=PATH_ADMIN?>plugins/datatables/dataTables.bootstrap.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=PATH_ADMIN?>plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
	<link rel="stylesheet" href="<?=PATH_ADMIN?>plugins/select2/select2.min.css">
    <link rel="stylesheet" href="<?=PATH_ADMIN?>dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?=PATH_ADMIN?>plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?=PATH_ADMIN?>plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=PATH_ADMIN?>dist/css/skins/_all-skins.min.css">
    <!-- jQuery 2.1.4 -->
    <script src="<?=PATH_ADMIN?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="<?=PATH_ADMIN?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
	<script src="<?=PATH_ADMIN?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<script src="<?=PATH_ADMIN?>plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="<?=PATH_ADMIN?>plugins/ckeditor/ckeditor.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini" <?php  if (isset($_GET['i'])&& $_GET['i']=="Print") {?>onload="window.print();" <?php }?>>
    <div class="wrapper">

<header class="main-header">

        <!-- Logo -->
        <a href="<?=base_url()?>cms" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><?=$site_Info['Title']?></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><?=$site_Info['Title']?></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=PATH?>upload/<?=$profile_Info['Pro_ProfileImage']?>" class="user-image" alt="<?=ucwords($this->session->userdata('FullName'))?>">
                  <span class="hidden-xs"><?=ucwords($this->session->userdata('FullName'))?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?=PATH?>upload/<?=$profile_Info['Pro_ProfileImage']?>" class="img-circle" alt="User Image">
                    <p>
                     <?=ucwords($this->session->userdata('FullName'))?>
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?=base_url()?>cms/account/edit/<?=str_replace('=','',base64_encode($this->session->userdata('AccountId')))?>" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?=base_url()?>cms/home/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="<?=base_url()?>cms/home/logout" title="Sign Out"><i class="fa fa-sign-out"></i></a>
              </li>
            </ul>
          </div>

        </nav>
      </header>