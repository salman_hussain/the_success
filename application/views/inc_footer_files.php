	<!-- Javascript -->
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.min.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/bootstrap.min.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.easing.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery-waypoints.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.bxslider.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.flexslider-min.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/owl.carousel.min.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/imagesloaded.min.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.isotope.min.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.sticky.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jflickrfeed.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.transit.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/parallax.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.appear.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.tweet.min.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.animateNumber.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/smoothscroll.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/jquery.mb.YTPlayer.js"></script>
   <script type="text/javascript" src="<?=PATH?>web/javascript/main.js"></script>
   <script type="text/javascript">
        function add_cart(id, price) {
            var id = id;
            var p_price = price;
            var qty = 1;
            $('#loading-div-background').show();
            $.ajax({
               type: "POST",
               url: "<?= base_url() ?>home/add_cart/",
               data: {'id': id, 'price': p_price, 'qty': qty},
               success: function (result) {
                    $('#loading-div-background').hide();
                    if (result > 0) {
                       //alert('Product Added Successfully.');
                       //$('.cart_count').html(result);
                       //$('.success_message').html('<i class="fa fa-check-circle"></i> Product Added Successfully');
                       //$('.success_message').fadeIn('slow');
                       /*setTimeout(function () {
                           $('#basic-modal-content').modal();
                           $('.success_message').fadeOut('slow');
                       }, 2000); // <-- time in milliseconds*/
                       
                       $('#cart_success').modal('show');


                   }
                   else {

                       $('#cart_error').modal('show');
                   }
               }
           });
        }
		
        function remove_cart_item(id) {
           var pId = id;
           //$('#loading-div-background').show();
           $.ajax({
               type: "POST",
               url: "<?= base_url() ?>home/remove_cart_item",
               data: {
                   'pId': pId
               },
               success: function (result) {
                   $('#loading-div-background').hide();
                   if (result == 1) {
                       // window.location.reload();
					   $("#load_cart").load('<?=base_url()?>home/load_cart');
                       
                   }
                   else {
                       
                   }
               }
           });
        }
        
        function subtract_cart_item(id) {
        
            var pId = id;
           // $('#loading-div-background').show();
            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>home/subtract_cart_item",
                data: {
                    'pId': pId
                },
                success: function (result) {
                    $('#loading-div-background').hide();
                    if (result == 1) {
                        //window.location.reload();
						$("#load_cart").load('<?=base_url()?>home/load_cart');
                    }
                    else {
                        
                    }
                }
            });
        }
        
        function add_cart_item(id) {
        
            var pId = id;
            //$('#loading-div-background').show();
            $.ajax({
                type: "POST",
                url: "<?= base_url() ?>home/add_cart_item",
                data: {
                    'pId': pId
                },
                success: function (result) {
                    $('#loading-div-background').hide();
                    if (result == 1) {
                        //window.location.reload();
						$("#load_cart").load('<?=base_url()?>home/load_cart');
                    }
                    else {
                        
                    }
                }
            });
        }
        
        function isValidEmail(email) 
        {
            var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
            return pattern.test(email);
        }
        
        $(".register_submit").click(function() {
            
            $('#loading-div-background').show();
            
            var r_name = $("input[name=r_name]").val();
            var r_email = $("input[name=r_email]").val();
            var r_password = $("input[name=r_password]").val();
            var r_phone = $("input[name=r_phone]").val();
            var r_city = $("input[name=r_city]").val();
            var r_country = $("input[name=r_country]").val();
            var count = 0;
            
            if(r_name == "")
            {
                $(".r_name").show();
                $(".r_name").html("* Required Field");
                count = 1;
            }
            else
            {
                $(".r_name").hide();
            }
            
            if(r_email == "")
            {
                $(".r_email").show();
                $(".r_email").html("* Required Field");
                count = 1;
            }
            else if(isValidEmail(r_email) == false)
            {
                $(".r_email").show();
                $(".r_email").html("* Invalid Email");
                count = 1;
            }
            else
            {
                $(".r_email").hide();
            }
            
            if(r_password == "")
            {
                $(".r_password").show();
                $(".r_password").html("* Required Field");
                count = 1;
            }
            else
            {
                $(".r_password").hide();
            }
            
            if(r_phone == "")
            {
                $(".r_phone").show();
                $(".r_phone").html("* Required Field");
                count = 1;
            }
            else
            {
                $(".r_phone").hide();
            }
            
            if(r_city == "")
            {
                $(".r_city").show();
                $(".r_city").html("* Required Field");
                count = 1;
            }
            else
            {
                $(".r_city").hide();
            }
            
            if(r_country == "")
            {
                $(".r_country").show();
                $(".r_country").html("* Required Field");
                count = 1;
            }
            else
            {
                $(".r_country").hide();
            }
            
            if(count == 0)
            {
                $.ajax({
                   type: "POST",
                   url: "<?= base_url() ?>accounts/register_account",
                   data: {
                       'r_name': r_name,
                       'r_email': r_email,
                       'r_password': r_password,
                       'r_phone': r_phone,
                       'r_city': r_city,
                       'r_country': r_country
                   },
                   success: function (result) {
                       $('#loading-div-background').hide();
                        if (result == 1) {
							$('.register_error').hide();
                            $('.register_success').html('<p class="text-small">Account Registered Successfully, Please Check Email.</p>');
                            $('.register_success').fadeIn('slow');
                            $("#register_form").hide("slow", function () {
                            });
                        }
                        else if (result == 2)
                        {
                            $('.register_error').html('<p class="text-small">Email Already Registered.</p>');
                            $('.register_error').show();
                        }
                        else {
                            $('.register_error').html('<p class="text-small">Please Try Again.</p>');
                            $('.register_error').show();
                        }
                   }
                });
            }
            else
            {
                $('#loading-div-background').hide();
            }
            return false;
        });
        
        $('.contact_us_form').click(function () {
                    
                    $('#loading-div-background').show();
                    
                    $("#name_error").hide();
                    $("#email_error").hide();
                    $("#msg_error").hide();
                    
                    $('.error_message').hide();
                    $('.success_message').hide();
                    
                    var count = 0;
                    var c_name = $("[name='name']").val();
                    var c_email = $("[name='email']").val();
                    var c_message = $("#comments").val();
                    if (c_name == "")
                    {
                        $("#name_error").html('Error: Required field.');
                        $("#name_error").show();
                        $("[name='name']").focus();
                        count = 1;
                        return false;
                    }
                    if (c_email == "")
                    {
                        $("#email_error").html('Error: Required field.');
                        $("#email_error").show();
                        $("[name='email']").focus();
                        count = 1;
                        return false;
                    }
                    else
                    {
                        function validateEmail(email)
                        {
                            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                            if (!emailReg.test(email))
                            {
                                count = 1;
                                return false;
                            }
                            else
                            {
                                count = 0;
                                return true;
                            }
                        }
                        if (!validateEmail(c_email))
                        {
                            $("#email_error").html('Error: Invalid email.');
                            $("#email_error").show();
                            $("[name='email']").focus();
                            return false;
                        }
                    }
                    if (c_message == "")
                    {
                        $("#msg_error").html('Error: Required field.');
                        $("#msg_error").show();
                        $("[name='msg']").focus();
                        count = 1;
                        return false;
                    }
                    if (count == 0)
                    {
                        
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>contact_us/contact_msg/",
                            data: {'name': c_name, 'email': c_email,'msg': c_message},
                            success: function (result)
                            {
                                $('#loading-div-background').hide();
                                if (result == 1)
                                {
                                    $("[name='name']").val('');
                                    $("[name='email']").val('');
                                    $("[name='msg']").val('');
                                    $('.success_message').show();
                                    $('#loading-div-background').hide();
                                }
                                else
                                {
                                    $("[name='name']").val('');
                                    $("[name='email']").val('');
                                    $("[name='msg']").val('');
                                    $('.error_message').show();
                                    $('#loading-div-background').hide();
                                }
                            }
                        });
                    }
                    else
                    {
                        $('#loading-div-background').hide();
                    }
                    
                    return false;
                });
   </script>