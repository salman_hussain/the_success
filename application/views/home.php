<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?=$site_Info['Title']?> | <?=$Pages[0]->Title?></title>
    <meta name="description" content="<?=$site_Info['MetaDescription']?>, <?=$Pages[0]->MetaDescription?>">
    <meta name="keywords" content="<?=$site_Info['MetaKeywords']?>, <?=$Pages[0]->MetaKeyword?>">
    <meta name="author" content="<?=base_url()?>">
    <?=$this->load->view('inc_header_files');?>
</head>

<body class="header-sticky">
    <?=$this->load->view('inc_header');?>
    <section class="head-slider slide-mask parallax">
            <div class="flexslider">
            <ul class="slides">
               <?php foreach($Slider as $slider){?>
			   <li style="background:url(<?=PATH?>upload/<?=$slider->Image?>) no-repeat; background-position:50% 0; background-size:cover;">
                    <div class="container">
                            <div class="row">
                                <div class="offset1 span11">
									<div class="content">
									    <?php if($slider->Title){?>
										<p class="title item-1 roll-bounceInDown"><?=$slider->Title?></p>
										<?php }?>
										<?php if($slider->Description){?>
										<p class="title item-2 roll-bounceInDown"><?=$slider->Description?></p>
										<?php }?>
										<a href="<?=$slider->ExternalURL?>"  class="read-more roll-bounceInLeft">READ MORE</a>
									</div><!-- /.content -->
								</div>
							</div>
                    </div>
                </li>
			   <?php }?>
            </ul>
        </div>
    </section>

    <section class="roll-row roll-feature-project" style="border-bottom: 1px solid #e5e5e5;  padding: 0px; margin: 0px;"> 
        <div class="container">
            <div class="row">
                <div class="project-title">
            			<h2 class="name">OUR Products</h2>
                	</div>
            	<div class="shop-items">
					<?php foreach ($Shops as $shops) { ?>
					
						<div class="items">
							<div class="image ">
								<img src="<?= PATH ?>upload/<?= $shops->Image ?>" alt="<?= $shops->Title ?>" style="height:275px;width:500px">
									<div class="icons icon-heart-empty"></div>
									<div class="item-detail">
										<a onclick="add_cart('<?= $shops->IdPages ?>', '<?= $shops->OtherText ?>')" class="add-cart icon-basket">Add to Cart</a>
										<div class="split"></div>
										<a href="shops/<?= $shops->SEO ?>" class="details icon-search">Details</a>
									</div>
							</div><!-- /.image -->
							<h5 class="title"><a href="shops/<?= $shops->SEO ?>"><?= $shops->Title ?></a></h5>
							<span class="number">US $<?= $shops->OtherText ?>/piece</span>
						</div><!-- /.items -->
					
					<?php } ?>	
				</div><!-- /.project-items -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.roll-feature-project -->
<!-- /.roll-getin -->
    <section class="roll-row main-page services-home roll-animation"  data-portfolio-effect="fadeInDown" data-animation-delay="0" data-animation-offset="75%" style="padding: 52px 0 28px 0;">
        <div class="container">
            <div class="row">
				<div class="span12">
                	<div class="project-title">
            			<h2 class="name"><center>Latest Blog</center></h2>
                	</div><!-- /.project-title -->
					<br>
                </div>	
                <div class="span12">
                   <?php 
				   $count = 0;
				   foreach($BlogsList as $blogs){
				   $count++;
				   ?>
				   <div class="items">
						<?php if($blogs->Image){?>
						<a href="blog/<?=$blogs->SEO?>"><img src="<?=PATH?>upload/<?=$blogs->Image?>" alt="image" style="height:275px;width:500px"></a>
						<?php }else{?>
						<a href="blog/<?=$blogs->SEO?>"><img src="<?=PATH?>upload/No_available_image.gif" alt="image" style="height:275px;width:500px"></a>
						<?php }?>
                        <h4 class="title-post" style="height:50px;"><a href="blog/<?=$blogs->SEO?>"><?=strip_tags(word_limiter($blogs->Title,4))?></a></h4>
						<div class="meta-post">
							<span class="date"> <?=date('d-M-Y',strtotime($blogs->Created));?></span>
						</div>
						<div style="height:80px;">
						<?=strip_tags(word_limiter($blogs->Description, 20))?><br><br>
						</div>
						<a href="blog/<?=$blogs->SEO?>" class="read-more">READ MORE</a>
                    </div><!-- /.items -->
                    <?php if($count == 3){break; }}?>
                </div><!-- /.span12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.services-home -->
   <?=$this->load->view('inc_footer');?>
   <?=$this->load->view('inc_footer_files');?>
</body>
</html>