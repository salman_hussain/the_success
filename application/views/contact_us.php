<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
		<title><?=$site_Info['Title']?> | <?=$Pages[0]->Title?></title>
		<meta name="description" content="<?=$site_Info['MetaDescription']?>, <?=$Pages[0]->MetaDescription?>">
		<meta name="keywords" content="<?=$site_Info['MetaKeywords']?>, <?=$Pages[0]->MetaKeyword?>">
		<meta name="author" content="<?=base_url()?>">
		<?=$this->load->view('inc_header_files');?>
    </head>

    <body class="header-sticky">
        <?= $this->load->view('inc_header'); ?>

        <section class="roll-row contact-map" style="padding:0;">
            <div id="map" style="width: 100%; height: 533px; opacity:0.9;"></div>
        </section>

        <section class="roll-row page-title page-about-alt">
            <div class="page-nav">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <ul class="breadcrumbs">
                                <li class="nav-prev"><a href="<?= base_url() ?>">Home</a></li>
                                <li class="nav-split"><a href="#"> > </a></li>
                                <li><a href="#"><?= $Pages[0]->Title ?></a></li>
                            </ul>
                        </div><!-- /.span12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div>
        </section><!-- /.page-title -->

        <section class="roll-row contact-page main-page">
            <div class="container">
                <div class="row">
                    <div class="span8">
                        <div class="comment-respond">
                            <h4 class="title">Post your message</h4>
                            
                               
                            <div class="result"></div>

                            <form class="comment-form contact-form" id="commentform" method="post" action="contact-us/contact_msg">
                                <div class="input-wrap name">
                                    <input type="text" size="14" value="" placeholder="Name" name="name" required>
                                        <p id="name_error" style="text-align: left;color: red;display: none;"></p>
                                </div>
                                <div class="input-wrap email">
                                    <input type="text" size="14" value="" placeholder="E-mail" name="email" required>
                                    <p id="email_error" style="text-align: left;color: red;display: none;"></p> 
                                </div>
                                
                                <div class="textarea-wrap">
                                    <textarea class=""  placeholder="Message" name="msg" id="comments" required></textarea>
                                    <p id="msg_error" style="text-align: left;color: red;display: none;"></p> 
                                </div>
                                
                                <div class="submit-wrap">
                                    <button type="submit" id="submitcomment" name="submitcomment" class="contact_us_form btn-submit">Send</button>
                                </div>
                            </form>
                        </div><!-- /.comment-respond -->
                    </div><!-- /.span -->

                    <div class="span3">
                        <div class="contact-info">
                            <?= $Pages[0]->Description ?>
                            <div class="social">
                                <a href="#" class="twitter">
                                    <i class="icon-twitter"></i>
                                </a>
                                <a href="#" class="facebook">
                                    <i class="icon-facebook"></i>
                                </a>
                            </div>
                        </div>
                    </div><!-- /.span2 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section>
        <?= $this->load->view('inc_footer'); ?>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.min.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.easing.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery-waypoints.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.bxslider.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.flexslider-min.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/owl.carousel.min.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/imagesloaded.min.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.sticky.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jflickrfeed.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.transit.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/parallax.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.appear.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.tweet.min.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/jquery.animateNumber.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/smoothscroll.js"></script>
		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/gmap3.min.js"></script>
		<script type="text/javascript" src="<?=PATH?>web/javascript/main.js"></script>
    </body>
</html>