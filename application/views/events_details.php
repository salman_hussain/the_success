<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <title><?=$site_Info['Title']?> | <?=$Events[0]->Title?></title>
    <meta name="description" content="<?=$site_Info['MetaDescription']?>, <?=$Events[0]->Title?>">
    <meta name="keywords" content="<?=$site_Info['MetaKeywords']?>, <?=$Events[0]->Title?>">
    <meta name="author" content="<?=base_url()?>">
    <?=$this->load->view('inc_header_files');?>
</head>

<body class="header-sticky">
    <?=$this->load->view('inc_header');?>
     <section class="roll-row page-title page-about-alt">
        <div class="main-title parallax">
            <div class="page-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="span12">
                    <h1 class="title pull-center"><?=$Events[0]->Title?></h1>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div>
		<div class="page-nav">
            <div class="container">
                <div class="row">
                    <div class="span12">
                        <ul class="breadcrumbs">
                            <li class="nav-prev"><a href="<?=base_url()?>">Home</a></li>
                            <li class="nav-split"><a href="#"> > </a></li>
                            <li><a href="<?=base_url()?>events">Events</a></li>
                            <li class="nav-split"><a href="#"> > </a></li>
                            <li><a href="#"><?=$Events[0]->Title?></a></li>
                        </ul>
                    </div><!-- /.span12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
		</div>
    </section><!-- /.page-title -->
    <section class="roll-row main-page">
	<br>
        <div class="container">
            <div class="row">
                <div class="span9">
                    <div class="blog-post">
                        <!-- /.blog-title -->
                        <article class="post" style="margin-top: -25px;">
                             <h4 class="title-post"><?=$Events[0]->Title?></h4>
							 <div class="meta-post">
									<span class="date"><?=date('d-M-Y',strtotime($Events[0]->DatePicker));?></span>
                            </div>
							<?php if($Events[0]->Image){?>
							<div>
                                <img src="<?=PATH?>upload/<?=$Events[0]->Image?>" alt="image">
                            </div>
                            <br><br>
							<?php }?>
							<?=$Events[0]->Description?>

                            <div class="spacing"></div>
						</article><!-- /.post -->
                        
                    </div><!-- /.blog-post -->
                </div><!-- /.span9 -->
                <div class="span3">
						<div class="widget widget-archives">
							<h4 class="title-post">RECENT POSTS</h4>
							<ul class="items-archives">
								<?php 
								$count = 0;
								foreach($EventsList as $events){
									if($Events[0]->IdPages != $events->IdPages){
								$count++;
								?>
								<li>
									<h4 class="title-post"><a href="<?=$events->SEO?>"><?=$events->Title?></a></h4>
									<br>
									<span class="date"> <?=date('d-M-Y',strtotime($events->DatePicker));?></span>
									<br>
									<?=strip_tags(word_limiter($events->Description, 30))?>
									<br>
									<br>
									<div class="spacing"></div>
									<a href="<?=$events->SEO?>">read more</a><br>
									<div class="spacing"></div>
									<br>
									<br>
								</li>
								
									<?php if($count == 5){break; }}}?>
							</ul>
						</div><!-- /.widget-archives -->
                </div><!-- /.span3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section>
   <?=$this->load->view('inc_footer');?>
   <?=$this->load->view('inc_footer_files');?>
</body>
</html>