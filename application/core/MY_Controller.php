<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* * *********************************************************************************************** 
 * ******************************Start of file home.php********************************************* 
 * ********************Base Controller for the project********************************************** 
 * *************************Location: ./application/core/MY_Controller.php************************
 * *********************************************************************************************** */

class MY_Controller extends CI_Controller {

    
    function __construct() {
        // then execute the parent constructor anyway
        parent::__construct();
        $this->load->helper('url');
        
        $this->load->library('session');
 
       /* if ($this->session->userdata('id') == '') 
        {
            if ($this->uri->segment(1) != 'login')
            {
                redirect(base_url().'login');
            }
        
        }
       */
    }
    
   
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */