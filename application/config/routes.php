<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override']       = 'pages';

$route['about-us'] 							= "about_us";
$route['contact-us'] 						= "contact_us";
$route['contact-us/contact_msg'] 			= "contact_us/contact_msg";
$route['blog/(:any)'] 						= "blog/details";
$route['events/(:any)'] 					= "events/details";
$route['glitz-life'] 						= "glitz_life";
$route['glitz-life/(:any)'] 				= "glitz_life/details";
$route['shops/(:any)'] 						= "shops/details";


// the last resort (dynamic) backend

$route['cms/home/'] 						= "cms/home/";
$route['cms/home/do_login'] 				= "cms/home/do_login";

$route['cms/account_type/status/(:any)'] 	= "cms/account_type/status/$1$2";
$route['cms/account_type/delete/(:any)'] 	= "cms/account_type/delete/$1";

$route['cms/builder/status/(:any)'] 	    = "cms/builder/status/$1$2";
$route['cms/builder/delete/(:any)'] 	    = "cms/builder/delete/$1";

$route['cms/account/status/(:any)'] 	    = "cms/account/status/$1$2";
$route['cms/account/delete/(:any)'] 	    = "cms/account/delete/$1";
$route['cms/orders/delete/(:any)'] 	    	= "cms/orders/delete/$1";


$route['cms/(:any)/view_all/(:any)'] 		= "cms/pages/view_all/$2";
$route['cms/(:any)/add_new/(:any)']  		= "cms/pages/add_new/$2";
$route['cms/(:any)/do_add_page/(:any)']  	= "cms/pages/do_add_page/$2";
$route['cms/(:any)/delete/(:any)']  		= "cms/pages/delete/$2";
$route['cms/(:any)/edit_pages/(:any)']     	= "cms/pages/edit_pages/$2";
$route['cms/(:any)/do_edit_pages']  		= "cms/pages/do_edit_pages";
$route['cms/(:any)/status/(:any)']  		= "cms/pages/status/$2";



/* End of file routes.php */
/* Location: ./application/config/routes.php */