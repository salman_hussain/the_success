<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class accounts extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('query');
        $this->load->library('Custom');
        $this->load->library('pagination');
        $this->load->library('cart');
    }

    public function index() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        $this->load->view('accounts', $data);
    }
    
    public function forgot_password() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        $this->load->view('forgot_pass', $data);
    }
    
    public function reset_password() {
        $email = $this->input->post('email');

        $check_email = $this->query->check_email($this->input->post('email'));

        if (count($check_email) == 1) {

            $passwordlength = 7;
            $new_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $passwordlength);

            $key = '';
            $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $new_pass, MCRYPT_MODE_CBC, md5(md5($key))));


            $reset = array('Password' => $PasswordEncrypted);

            $table = 'accounts';

            $query = $this->query->do_edit($table, $reset, $email);

            $logo = $this->custom->site_Info();
            $txt2 = "";
            $to2 = $email;
            $subject2 = "Reset Password";
            $txt2 .= "<div style='text-align: center;'>"
                    . "<h1 style='font-weight: bold'>Password Reset</h1></div><div style='font-size: 18px;'>"
                    . "Hello ,<br/>"
                    . "Your Account Password has been reset.<br/><br/>"
                    . "New Password: ".$new_pass."<br/><br/>"
                    . "Best Regards,<br/>"
                    . "The Success Team <br/>";

            $from2 = $logo['EmailFrom'];
            $headers = "From: thesuccess <$from2>" . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html\r\n";

            mail($to2, $subject2, $txt2, $headers);

            $msg = '<div class="alert alert-success"><p class="text-small">Password Reset Successfully..</p></div>';

            $this->session->set_userdata('Success', $msg);
            
            
        } else {
            $msg = '<div class="alert alert-danger"><p class="text-small">Email Not Registered..</p></div>';

            $this->session->set_userdata('Error', $msg);
        }

        

        redirect(base_url() . 'accounts/forgot_password');
    }
    
    public function user_login() {


        $key = '';
        $Password = $this->input->post('l_password');
        $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $Password, MCRYPT_MODE_CBC, md5(md5($key))));

        $email = $this->input->post('l_email');
        $type = "6";

        $query = $this->query->user_login($email, $PasswordEncrypted, $type);


        if ($query->num_rows() == 1) {

				foreach ($query->result_array() as $recs => $res) {
					$this->session->set_userdata(
							array(
								'user_id'    => $res['IdAccount'],
								'user_name'  => $res['FullName'],
								'user_Type'  => $res['IdAccountType'],
								'user_Login' => true
							)
					);
				}
			if($res['Status'] == "1"){
				if($this->cart->contents()){
					redirect(base_url() . 'home/view_cart');
				}else{
					redirect(base_url() . 'home');
				}
			}else{
				$msg = '<div class="alert alert-danger"><p class="text-small">Please check your email and click “Verify Email Address”</p></div>';

				$this->session->set_userdata('Error', $msg);

				redirect(base_url() . 'accounts?i=login');
			}	
			
        } else {

            $msg = '<div class="alert alert-danger"><p class="text-small">Invalid Email or Password.</p></div>';

            $this->session->set_userdata('Error', $msg);

            redirect(base_url() . 'accounts?i=login');
        }
    }

    public function logout() {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_Type');
        $this->session->unset_userdata('user_Login');
        $this->session->unset_userdata('user_name');
        $this->session->sess_destroy();

        redirect(base_url() . 'home');
    }

    public function register_account() {

        $key = '';
        $Password = $this->input->post('r_password');
        $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $Password, MCRYPT_MODE_CBC, md5(md5($key))));

        $data = array(
            'FullName' => $this->input->post('r_name'),
            'EmailAddress' => $this->input->post('r_email'),
            'Password' => $PasswordEncrypted,
            'IdAccountType' => '6',
            'PhoneNumber' => $this->input->post('r_phone'),
            'city' => $this->input->post('r_city'),
            'country' => $this->input->post('r_country'),
            'Status' => 0,
            'Created' => date("Y-m-d")
        );

        $check_email = $this->query->check_email($this->input->post('r_email'));

        if (count($check_email) == 0) {
            $table = 'accounts';
            $query = $this->query->insert_query($table, $data);

            if ($query['query'] == 1) {

                if ($query['query'] == 1) {

                    $logo = $this->custom->site_Info();
                    $txt2 = "";
                    $to2 = $this->input->post('r_email');
                    $subject2 = "New User registration";
                    $txt2 .= "<div style='text-align: center;'>"
                            . "<h1 style='font-weight: bold'>New Account Created</h1></div><div style='font-size: 18px;'>"
                            . "Dear " . ucwords($this->input->post('name')) . ",<br/>"
                            . "Your New Account has been created.<br/><br/>"
                            . "<a href='" . base_url() . "accounts/verify_email/" . str_replace("=", "", base64_encode($query["CreatedID"])) . "'>Verify Your Email.</a><br/><br/>"
                            . "Best Regards,<br/>"
                            . "The Success Team <br/>";

                    $from2 = $logo['EmailFrom'];
                    $headers = "From: thesuccess <$from2>" . "\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/html\r\n";

                    mail($to2, $subject2, $txt2, $headers);

                    echo "1";
                } else {
                    echo "0";
                }
            } else {
                echo "0";
            }
        } else {
            echo "2";
        }
    }

    public function verify_email($id) {
        $user_id = base64_decode($id);

        $data = array(
            'Status' => 1
        );

        $result = $this->query->update_query("accounts", $data, "IdAccount", $user_id);

        $msg = '<div class="alert alert-success"><p class="text-small">Account Verified Successfully. Please Login</p></div>';

        $this->session->set_userdata('Success', $msg);

        redirect(base_url() . 'accounts?i=login');
    }

}
