<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('cms/query');
        $this->load->library('Custom');
    }

    public function index() {
			
        if ($this->session->userdata('Is_Login') == true) {
			
			$data['menu']        = $this->custom->menuBar();
			
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Dashboard";
            $this->load->view('cms/dashboard', $data);
        } else {
            redirect('cms/');
        }
    }
	
	
	
	 public function inc_dashboard() {
		 
        if ($this->session->userdata('Is_Login') == true) {
			
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Dashboard";
			$this->load->view('cms/inc_dashboard', $data);
		} else {
            redirect('cms/');
        }
    }
	
	public function add_new_slider() {
			
        if ($this->session->userdata('Is_Login') == true) {
			
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Add New Slider";
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/add_new_slider', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/add_new_slider');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_add_new_slider() {
					$data = array(
                        'Title' 				=> $this->input->post('Title'),
                        'Description' 			=> $this->input->post('Description'),
                        'Price' 				=> $this->input->post('Price'),
                        'ButtonText' 			=> $this->input->post('ButtonText'),
                        'ButtonLink' 			=> $this->input->post('ButtonLink'),
                        'UploadImage' 			=> $this->input->post('UploadImage'),
                        'Status' 				=> 1,
                        'Created' 				=> date("Y-m-d H:i:s"),
                        'CreatedBy' 			=> $this->session->userdata('AccountId')
                    );
					
					$table = 'slider';
					$query = $this->query->insert_query($table, $data);
					$CreatedID = $query['CreatedID'];
					if ($query['query'] == 1) {
					}else{
					}

	}
	public function view_all_slider() {
			
        if ($this->session->userdata('Is_Login') == true) {
			
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "View All Slider";
			
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_slider', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_slider');
			$this->load->view('cms/footer');		 
			}
		} else {
            redirect('cms/');
        }
    }
	
}
