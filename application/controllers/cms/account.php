<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('cms/query');
        $this->load->library('custom');
    }

    public function index() {
		
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();	
			
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Dashboard";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_account', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_account');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	 public function view_all() {
			
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();	
				
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Dashboard";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_account', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_account');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	
	public function add_new() {
			

			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();	
			
			$row = $this->query->query("SELECT * FROM `account_type` WHERE `Status` = 1");
			$data['AccountType'] = $row;
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Add New Account";
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/add_new_account', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/add_new_account');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function checkemail(){
			$table = 'accounts';
			$key   = 'IdAccount';
			$row = $this->query->query("SELECT * FROM `accounts` WHERE `EmailAddress` = '".$this->input->post('EmailAddress')."'");
			$data['Account'] = $row;
			if(count($data['Account']) > 0){
				echo 1;
			}else{
				echo 0;
			}
	}
	
	public function do_add_new_account() {
		
							/*------Image--------*/	
					$name_array = array();
					$count = count($_FILES['ProfileImage']['size']);
					foreach($_FILES as $key=>$value)
					for($s=0; $s < $count; $s++) {
					$_FILES['ProfileImage']['name']			= $value['name'][$s];
					$_FILES['ProfileImage']['type']    		= $value['type'][$s];
					$_FILES['ProfileImage']['tmp_name'] 	= $value['tmp_name'][$s];
					$_FILES['ProfileImage']['error']       	= $value['error'][$s];
					$_FILES['ProfileImage']['size']    		= $value['size'][$s];  
					$config['upload_path'] 					= 'assets/upload/';
					$config['allowed_types'] 				= 'gif|jpg|png';
					
					$this->load->library('upload', $config);
					
					if($this->upload->do_upload('ProfileImage')){
					$data = $this->upload->data();
						$ProfileImage = $data['file_name'];
						}else{
						$ProfileImage = '';
						}
					}				
					/*------Image--------*/
		
			$key = '';
			$PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key),$this->input->post('Password'), MCRYPT_MODE_CBC, md5(md5($key))));
			$data = array(
                        'FullName' 				=> $this->input->post('FullName'),
                        'EmailAddress' 			=> $this->input->post('EmailAddress'),
                        'Password' 				=> $PasswordEncrypted,
                        'ProfileImage' 			=> $ProfileImage,
                        'PhoneNumber' 			=> $this->input->post('PhoneNumber'),
                        'Website' 				=> $this->input->post('Website'),
                        'Twitter' 				=> $this->input->post('Twitter'),
                        'Facebook' 				=> $this->input->post('Facebook'),
                        'LinkedIn' 				=> $this->input->post('LinkedIn'),
                        'Googleplus' 			=> $this->input->post('Googleplus'),
                        'YouTube' 				=> $this->input->post('YouTube'),
                        'Instagram' 			=> $this->input->post('Instagram'),
                        'Pinterest' 			=> $this->input->post('Pinterest'),
                        'IdAccountType' 		=> $this->input->post('IdAccountType'),
                        'Status' 				=> 1,
                        'Created' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
                    );
					
					$table = 'accounts';
					$query = $this->query->insert_query($table, $data);
					$CreatedID = $query['CreatedID'];
					if ($query['query'] == 1) {
						echo "1";
					}else{
						echo "0";
					}

	}

	public function edit($ID) {

			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();	
				
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Edit Account";
			$IdAccount			  = $this->get_view();
			
			$row = $this->query->query("SELECT * FROM `account_type` WHERE `Status` = 1");
			$data['AccountType'] = $row;
			
			$row = $this->query->query("SELECT * FROM `accounts` WHERE `IdAccount` = ".base64_decode($ID));
			$data['Account'] = $row;
			
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/edit_account', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/edit_account');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_edit() {
                    
					$IdAccount 				= $this->input->post('IdAccount');
						
								/*------Image--------*/	
					$name_array = array();
					$count = count($_FILES['ProfileImage']['size']);
					foreach($_FILES as $key=>$value)
					for($s=0; $s < $count; $s++) {
					$_FILES['ProfileImage']['name']			= $value['name'][$s];
					$_FILES['ProfileImage']['type']    		= $value['type'][$s];
					$_FILES['ProfileImage']['tmp_name'] 	= $value['tmp_name'][$s];
					$_FILES['ProfileImage']['error']       	= $value['error'][$s];
					$_FILES['ProfileImage']['size']    		= $value['size'][$s];  
					$config['upload_path'] 					= 'assets/upload/';
					$config['allowed_types'] 				= 'gif|jpg|png';
					
					$this->load->library('upload', $config);
					
					if($this->upload->do_upload('ProfileImage')){
					$data = $this->upload->data();
						$ProfileImage = $data['file_name'];
							$data = array(
								'ProfileImage' 			=> $ProfileImage
							);
							$table = 'accounts';
							$key   = 'IdAccount';
							$query = $this->query->update_query($table, $data, $key, $IdAccount);
						}
					}				
					/*------Image--------*/
		
			$key = '';
			$PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key),$this->input->post('Password'), MCRYPT_MODE_CBC, md5(md5($key))));
			$data = array(
                        'FullName' 				=> $this->input->post('FullName'),
                        'EmailAddress' 			=> $this->input->post('EmailAddress'),
                        'Password' 				=> $PasswordEncrypted,
                        'PhoneNumber' 			=> $this->input->post('PhoneNumber'),
                        'Website' 				=> $this->input->post('Website'),
                        'Twitter' 				=> $this->input->post('Twitter'),
                        'Facebook' 				=> $this->input->post('Facebook'),
                        'LinkedIn' 				=> $this->input->post('LinkedIn'),
                        'Googleplus' 			=> $this->input->post('Googleplus'),
                        'YouTube' 				=> $this->input->post('YouTube'),
                        'Instagram' 			=> $this->input->post('Instagram'),
                        'Pinterest' 			=> $this->input->post('Pinterest'),
                        'IdAccountType' 		=> $this->input->post('IdAccountType'),
                        'Status' 				=> 1,
                        'Updated' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
                    );
					
					$table = 'accounts';
					$key   = 'IdAccount';
					$query = $this->query->update_query($table, $data, $key, $IdAccount);
					if ($query) {
						echo "1";
					}else{
						echo "0";
					}

	}
	
	public function get_Json() {
        $row = $this->query->query("SELECT * FROM `accounts` WHERE `IdAccountType` NOT LIKE '1' AND `IdAccountType` NOT LIKE '5' ORDER BY IdAccount DESC");
        $data['get_Json'] = $row;
        $count  = 1;
		if(count($data['get_Json']) > 0){
			foreach ($data['get_Json'] as $sj) {
				if($sj->IdAccount != 1 && $sj->IdAccountType != 5){
			    $Action  = "";			
				$Action .= str_replace('=','',base64_encode($sj->IdAccount));
				$rows[0] = $count++;
				$rows[1] = $sj->FullName;
				$rows[2] = $sj->EmailAddress;
				$rows[3] = $sj->Created;
				$rows[4] = $Action;
				$rows[5] = $sj->Status;
				$response['aaData'][] = $rows;
				}
			}
			echo json_encode($response, true);
		}else{
				$rows[0] = "";
				$rows[1] = "";
				$rows[2] = "";
				$rows[3] = "";
				$rows[4] = "";
				$rows[5] = "";
				$response['aaData'][] = $rows;
			echo json_encode($response, true);	
		}
    }
	
	public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
	
	public function delete($id) {
					$table = 'accounts';
                    $key   = 'IdAccount';
					$row = $this->query->query("SELECT * FROM `accounts` WHERE `IdAccount` = ".base64_decode($id));
					$data['Account'] = $row;
					if(count($data['Account']) > 0){
						$query = $this->query->delete($table, $key, str_replace('=','',$id));
						if ($query) {
							echo 1;
						}else{
							echo 0;	
						}
					}else{
						echo 0;
					}
    }
	public function status($id) {
		$explode 	   = explode('_', $id);
		$IdAccount 	   = $explode[0];
		$Status    	   = $explode[1];
                if ($Status == 1) {
                    $ac = 0;
                } else {
                    $ac = 1;
                }
                $data = array(
                    'Status' => $ac
                );
                $table  = 'accounts';
                $key 	= 'IdAccount';
                $query  = $this->query->status($table, $data, $key, $IdAccount, $ac);
                if ($query) {
                    echo 1;
                }else{
					echo 0;
				}
	}
	
	public function delete_image($IdAccount) {
		$data = array(
			'ProfileImage' 	=> ''
		);
		$table = 'accounts';
		$key   = 'IdAccount';
		$query = $this->query->update_query($table, $data, $key, $IdAccount);
		if ($query){
		echo 1;
		}else{
		echo 0;
		}
	}
}
