<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Builder extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('cms/query');
        $this->load->library('custom');
    }

    public function index() {

			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "View All Builder";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_builder', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_builder');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	 public function view_all_builder() {
			
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "View All Builder";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_builder', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_builder');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	
	public function add_new_builder() {

			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Add New Builder";
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/add_new_builder', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/add_new_builder');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_add_new_builder() {
					$data = array(
                        'Title' 				=> $this->input->post('Title'),
                        'Label_1' 				=> $this->input->post('Label_1'),
                        'Status_Label_1' 		=> $this->input->post('Status_Label_1'),
                        'Label_2' 				=> $this->input->post('Label_2'),
                        'Status_Label_2' 		=> $this->input->post('Status_Label_2'),
                        'Label_3' 				=> $this->input->post('Label_3'),
                        'Status_Label_3' 		=> $this->input->post('Status_Label_3'),
                        'Label_4' 				=> $this->input->post('Label_4'),
                        'Status_Label_4' 		=> $this->input->post('Status_Label_4'),
                        'Label_5' 				=> $this->input->post('Label_5'),
                        'Status_Label_5' 		=> $this->input->post('Status_Label_5'),
                        'Label_6' 				=> $this->input->post('Label_6'),
                        'Status_Label_6' 		=> $this->input->post('Status_Label_6'),   
						'Label_7' 				=> $this->input->post('Label_7'),
                        'Status_Label_7' 		=> $this->input->post('Status_Label_7'),
                        'Label_8' 				=> $this->input->post('Label_8'),
                        'Status_Label_8' 		=> $this->input->post('Status_Label_8'),
                        'Label_9' 				=> $this->input->post('Label_9'),
                        'Status_Label_9' 		=> $this->input->post('Status_Label_9'),						
                        'Label_10' 				=> $this->input->post('Label_10'),
                        'Status_Label_10' 		=> $this->input->post('Status_Label_10'),
                        'Status_Label_11' 		=> $this->input->post('Status_Label_11'),
                        'Status_Label_12' 		=> $this->input->post('Status_Label_12'),
                        'Status_Label_13' 		=> $this->input->post('Status_Label_13'),
                        'Status_Label_14' 		=> $this->input->post('Status_Label_14'),
                        'Status_Label_15' 		=> $this->input->post('Status_Label_15'),
                        'Status' 				=> 1,
                        'Created' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
						'SEO'      				=> $this->custom->seo_Url($this->input->post('Title'))
                    );
					
					$table = 'builder';
					$query = $this->query->insert_query($table, $data);
					$CreatedID = $query['CreatedID'];
					if ($query['query'] == 1) {
						echo "1";
					}else{
						echo "0";
					}

	}

	public function edit_builder() {

			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Edit Builder";
			$IdBuilder			  = $this->get_view();
			$row = $this->query->query("SELECT * FROM `builder` WHERE `IdBuilder` = ".base64_decode($IdBuilder));
			$data['Builder'] = $row;
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/edit_builder', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/edit_builder');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_edit_builder() {
                        $IdBuilder 				= $this->input->post('IdBuilder');
					$data = array(
                        'Title' 				=> $this->input->post('Title'),
                        'Label_1' 				=> $this->input->post('Label_1'),
                        'Status_Label_1' 		=> $this->input->post('Status_Label_1'),
                        'Label_2' 				=> $this->input->post('Label_2'),
                        'Status_Label_2' 		=> $this->input->post('Status_Label_2'),
                        'Label_3' 				=> $this->input->post('Label_3'),
                        'Status_Label_3' 		=> $this->input->post('Status_Label_3'),
                        'Label_4' 				=> $this->input->post('Label_4'),
                        'Status_Label_4' 		=> $this->input->post('Status_Label_4'),
                        'Label_5' 				=> $this->input->post('Label_5'),
                        'Status_Label_5' 		=> $this->input->post('Status_Label_5'),
                        'Label_6' 				=> $this->input->post('Label_6'),
                        'Status_Label_6' 		=> $this->input->post('Status_Label_6'),   
						'Label_7' 				=> $this->input->post('Label_7'),
                        'Status_Label_7' 		=> $this->input->post('Status_Label_7'),
                        'Label_8' 				=> $this->input->post('Label_8'),
                        'Status_Label_8' 		=> $this->input->post('Status_Label_8'),
                        'Label_9' 				=> $this->input->post('Label_9'),
                        'Status_Label_9' 		=> $this->input->post('Status_Label_9'),						
                        'Label_10' 				=> $this->input->post('Label_10'),
                        'Status_Label_10' 		=> $this->input->post('Status_Label_10'),
                        'Status_Label_11' 		=> $this->input->post('Status_Label_11'),
                        'Status_Label_12' 		=> $this->input->post('Status_Label_12'),
                        'Status_Label_13' 		=> $this->input->post('Status_Label_13'),
                        'Status_Label_14' 		=> $this->input->post('Status_Label_14'),
                        'Status_Label_15' 		=> $this->input->post('Status_Label_15'),
                        'Status' 				=> 1,
                        'Created' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
						'SEO'      				=> $this->custom->seo_Url($this->input->post('Title'))
                    );
					
					$table = 'builder';
					$key   = 'IdBuilder';
					$query = $this->query->update_query($table, $data, $key, $IdBuilder);
					if ($query) {
						echo "1";
					}else{
						echo "0";
					}

	}
	
	public function get_Json() {
        $row = $this->query->query("SELECT * FROM `builder` ORDER BY IdBuilder DESC");
        $data['get_Json'] = $row;
        $count  = 1;
		if(count($data['get_Json']) > 0){
			foreach ($data['get_Json'] as $sj) {
				$Action = "";			
				$Action .= str_replace('=','',base64_encode($sj->IdBuilder));
				$rows[0] = $count++;
				$rows[1] = $sj->Title;
				$rows[2] = $sj->Created;
				$rows[3] = $Action;
				$rows[4] = $sj->Status;
				$response['aaData'][] = $rows;
			}
			echo json_encode($response, true);
		}else{
				$rows[0] = "";
				$rows[1] = "";
				$rows[2] = "";
				$rows[3] = "";
				$rows[4] = "";
				$response['aaData'][] = $rows;
			echo json_encode($response, true);	
		}
    }
	
	public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
	
	public function delete($id) {
					$table = 'builder';
                    $key   = 'IdBuilder';
					$row = $this->query->query("SELECT * FROM `builder` WHERE `IdBuilder` = ".base64_decode($id));
					$data['Builder'] = $row;
					if(count($data['Builder']) > 0){
						$query = $this->query->delete($table, $key, str_replace('=','',$id));
						if ($query) {
							echo 1;
						}else{
							echo 0;	
						}
					}else{
						echo 0;
					}
    }

	public function status($id) {
		$explode   = explode('_', $id);
		$IdBuilder   = $explode[0];
		$Status    = $explode[1];
                if ($Status == 1) {
                    $ac = 0;
                } else {
                    $ac = 1;
                }
                $data = array(
                    'Status' => $ac
                );
                $table  = 'builder';
                $key 	= 'IdBuilder';
                $query  = $this->query->status($table, $data, $key, $IdBuilder, $ac);
                if ($query) {
                    echo 1;
                }else{
					echo 0;
				}
	}
}
