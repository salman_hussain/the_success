<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('cms/query');
        $this->load->library('Custom');
        $this->load->library('pagination');
    }

    public function index() {
	
        $data['site_Info'] = $this->custom->site_Info();
        $data['error'] = "";
		
		/* -----login----- */
        if (isset($_POST['login-btn'])) {
            $data['error'] = "";
            $Email 	  = $this->input->post('username');
            $Password = $this->input->post('password');
            $key 	  = '';
           
            $PasswordEncrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $Password, MCRYPT_MODE_CBC, md5(md5($key))));

            $this->load->model('fyadmin/query');

            $query = $this->query->login($Email, $PasswordEncrypted);            

            if ($query->num_rows() == 1) {
                foreach ($query->result_array() as $recs => $res) {
                    $this->session->set_userdata(array(
                        'AccountId' 	=> $res['IdAccount'],
                        'FullName' 		=> $res['FullName'],
                        'Account_Type' 	=> $res['IdAccountType'],
                        'Is_Login' 		=> true
                            )

                    );
                }
                redirect(base_url() . 'cms/dashboard');

            } else {
                $data['error'] = "Invalid Email Address/Password.";
                $this->load->view('cms/home', $data);
            }
        } else {
            $this->load->view('cms/home', $data);
        }
        /* ----- //login----- */
        if ($this->session->userdata('Is_Login') == true) {
            redirect(base_url() . 'cms/dashboard');
        }
    }
	
	    public function logout() {

        $this->session->unset_userdata('AccountId');
        $this->session->unset_userdata('FullName');
        $this->session->unset_userdata('Account_Type');
        $this->session->unset_userdata('Is_Login');
        $this->session->sess_destroy();
        redirect(base_url() . 'cms');

    }
}
