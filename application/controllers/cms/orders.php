<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Orders extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('cms/query');
        $this->load->library('custom');
    }

    public function index() {

			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
				
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Dashboard";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_orders', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_orders');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	 public function view_all_orders() {
			
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Dashboard";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_orders', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_orders');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	
	public function add_new_orders() {
			
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Add New orders";
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/add_new_orders', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/add_new_orders');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_add_new_orders() {
					$data = array(
                        'Type' 					=> $this->input->post('Type'),
                        'Status' 				=> 1,
                        'Created' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
                    );
					
					$table = 'orders';
					$query = $this->query->insert_query($table, $data);
					$CreatedID = $query['CreatedID'];
					if ($query['query'] == 1) {
						echo "1";
					}else{
						echo "0";
					}

	}

	public function edit_orders() {

			if ($this->session->userdata('Is_Login') == true) {
			
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Edit orders";
			$IdOrders			  = $this->get_view();
			$data['GetIdOrders']     = $this->get_view();
			$row = $this->query->query("SELECT `orders`.IdOrders, `orders`.OrderNo, `orders`.Total, `orders`.Status, `orders`.Created, `orders`.FirstName, `orders`.LastName, `orders`.Address, `orders`.Phone, `orders`.Company, `accounts`.FullName, `accounts`.EmailAddress, `accounts`.PhoneNumber FROM `orders` LEFT JOIN `accounts` ON `orders`.AccountId = `accounts`.IdAccount WHERE `orders`.IdOrders = ".base64_decode($IdOrders)." ORDER BY `orders`.IdOrders DESC");
			$data['orders'] = $row;
			$row = $this->query->query("SELECT * FROM `order_details` WHERE IdOrders = ".base64_decode($IdOrders)." ORDER BY IorderDetails ASC");
			$data['details'] = $row;
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/edit_orders', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/edit_orders');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_edit_orders() {
                    $IdAccountType 				= $this->input->post('IdAccountType');
					$data = array(
                        'Type' 					=> $this->input->post('Type'),
                        'Status' 				=> 1,
                        'Updated' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId')
					);
					
					$table = 'orders';
					$key   = 'IdAccountType';
					$query = $this->query->update_query($table, $data, $key, $IdAccountType);
					if ($query) {
						echo "1";
					}else{
						echo "0";
					}

	}
	
	public function get_Json() {
        $row = $this->query->query("SELECT `orders`.IdOrders, `orders`.OrderNo, `orders`.Total, `orders`.Status, `orders`.Created, `accounts`.FullName, `accounts`.EmailAddress, `accounts`.PhoneNumber FROM `orders` LEFT JOIN `accounts` ON `orders`.AccountId = `accounts`.IdAccount ORDER BY `orders`.IdOrders DESC");
        $data['get_Json'] = $row;
        $count  = 1;
		if(count($data['get_Json']) > 0){
			foreach ($data['get_Json'] as $sj) {
				if($sj->Status == "0"){
					$Status = "In progress";
				}else if($sj->Status == "1"){
					$Status = "Success";
				}else if($sj->Status == "2"){
					$Status = "Cancel";
				}
			    $Action  = "";			
				$Action .= str_replace('=','',base64_encode($sj->IdOrders));
				$rows[0] = $count++;
				$rows[1] = $sj->FullName;
				$rows[2] = $sj->OrderNo;
				$rows[3] = $sj->EmailAddress;
				$rows[4] = $sj->PhoneNumber;
				$rows[5] = "US $".$sj->Total;
				$rows[6] = $sj->Created;
				$rows[7] = $Status;
				$rows[8] = $Action;
				$response['aaData'][] = $rows;
			}
			echo json_encode($response, true);
		}else{
				$rows[0] = "";
				$rows[1] = "";
				$rows[2] = "";
				$rows[3] = "";
				$rows[4] = "";
				$rows[5] = "";
				$rows[6] = "";
				$rows[7] = "";
				$rows[8] = "";
				$response['aaData'][] = $rows;
			echo json_encode($response, true);	
		}
    }
	
	public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
	
	public function delete($id) {
					$table = 'orders';
                    $key   = 'IdOrders';
					$row = $this->query->query("SELECT * FROM `orders` WHERE `IdOrders` = ".base64_decode($id));
					$data['orders'] = $row;
					if(count($data['orders']) > 0){
						$query = $this->query->delete($table, $key, str_replace('=','',$id));
						$table = 'order_details';
						$key   = 'IdOrders';
						$query = $this->query->delete($table, $key, str_replace('=','',$id));
						if ($query) {
							echo 1;
						}else{
							echo 0;	
						}
					}else{
						echo 0;
					}
    }
	public function status($id) {
		$explode 	   = explode('_', $id);
		$IdAccountType = $explode[0];
		$Status    	   = $explode[1];
                if ($Status == 1) {
                    $ac = 0;
                } else {
                    $ac = 1;
                }
                $data = array(
                    'Status' => $ac
                );
                $table  = 'orders';
                $key 	= 'IdAccountType';
                $query  = $this->query->status($table, $data, $key, $IdAccountType, $ac);
                if ($query) {
                    echo 1;
                }else{
					echo 0;
				}
	}
	
}
