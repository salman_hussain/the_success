<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account_type extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('cms/query');
        $this->load->library('custom');
    }

    public function index() {

			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
				
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Dashboard";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_account_type', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_account_type');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	 public function view_all_account_type() {
			
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Dashboard";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_account_type', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_account_type');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	
	public function add_new_account_type() {
			
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Add New account_type";
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/add_new_account_type', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/add_new_account_type');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_add_new_account_type() {
					$data = array(
                        'Type' 					=> $this->input->post('Type'),
                        'Status' 				=> 1,
                        'Created' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
                    );
					
					$table = 'account_type';
					$query = $this->query->insert_query($table, $data);
					$CreatedID = $query['CreatedID'];
					if ($query['query'] == 1) {
						echo "1";
					}else{
						echo "0";
					}

	}

	public function edit_account_type() {

			if ($this->session->userdata('Is_Login') == true) {
			
			$data['menu']        = $this->custom->menuBar();
			
            $id 	= $this->session->userdata('AccountId');
            $type 	= $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Edit account_type";
			$IdAccountType			  = $this->get_view();
			$row = $this->query->query("SELECT * FROM `account_type` WHERE `IdAccountType` = ".base64_decode($IdAccountType));
			$data['account_type'] = $row;
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/edit_account_type', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/edit_account_type');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_edit_account_type() {
                    $IdAccountType 				= $this->input->post('IdAccountType');
					$data = array(
                        'Type' 					=> $this->input->post('Type'),
                        'Status' 				=> 1,
                        'Updated' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId')
					);
					
					$table = 'account_type';
					$key   = 'IdAccountType';
					$query = $this->query->update_query($table, $data, $key, $IdAccountType);
					if ($query) {
						echo "1";
					}else{
						echo "0";
					}

	}
	
	public function get_Json() {
        $row = $this->query->query("SELECT * FROM `account_type` ORDER BY IdAccountType DESC");
        $data['get_Json'] = $row;
        $count  = 1;
		if(count($data['get_Json']) > 0){
			foreach ($data['get_Json'] as $sj) {
			    $Action  = "";			
				$Action .= str_replace('=','',base64_encode($sj->IdAccountType));
				$rows[0] = $count++;
				$rows[1] = $sj->Type;
				$rows[2] = $sj->Created;
				$rows[3] = $Action;
				$rows[4] = $sj->Status;
				$response['aaData'][] = $rows;
			}
			echo json_encode($response, true);
		}else{
				$rows[0] = "";
				$rows[1] = "";
				$rows[2] = "";
				$rows[3] = "";
				$rows[4] = "";
				$response['aaData'][] = $rows;
			echo json_encode($response, true);	
		}
    }
	
	public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
	
	public function delete($id) {
					$table = 'account_type';
                    $key   = 'IdAccountType';
					$row = $this->query->query("SELECT * FROM `account_type` WHERE `IdAccountType` = ".base64_decode($id));
					$data['account_type'] = $row;
					if(count($data['account_type']) > 0){
						$query = $this->query->delete($table, $key, str_replace('=','',$id));
						if ($query) {
							echo 1;
						}else{
							echo 0;	
						}
					}else{
						echo 0;
					}
    }
	public function status($id) {
		$explode 	   = explode('_', $id);
		$IdAccountType = $explode[0];
		$Status    	   = $explode[1];
                if ($Status == 1) {
                    $ac = 0;
                } else {
                    $ac = 1;
                }
                $data = array(
                    'Status' => $ac
                );
                $table  = 'account_type';
                $key 	= 'IdAccountType';
                $query  = $this->query->status($table, $data, $key, $IdAccountType, $ac);
                if ($query) {
                    echo 1;
                }else{
					echo 0;
				}
	}
}
