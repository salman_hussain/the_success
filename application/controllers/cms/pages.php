<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('cms/query');
        $this->load->library('custom');
		$this->load->helper("image");
		$this->load->library('image_lib');

    }

    public function index() {

			if ($this->session->userdata('Is_Login') == true) {
			
			$data['menu']        = $this->custom->menuBar();	
           
 		    $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "View All";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all_builder', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all_builder');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	 public function view_all($ID) {
			
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();	
			
			$row = $this->query->query("SELECT * FROM `builder` WHERE `IdBuilder` = ".base64_decode($ID));
			$data['PageSettings'] = $row;
			
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            $data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "View All";
            
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/view_all', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/view_all');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	
	public function add_new($ID) {
		
			if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();

			$row = $this->query->query("SELECT * FROM `builder` WHERE `IdBuilder` = ".base64_decode($ID));
			$data['PageSettings'] = $row;
			
			$row = $this->query->query("SELECT * FROM `pages` WHERE `Status` = '1' AND `IdBuilder` = '2' ");
			$data['UnderPage'] = $row;
			
			$id 	= $this->session->userdata('AccountId');
			$type 	= $this->session->userdata('FullName');
			$data['site_Info']    = $this->custom->site_Info();
			$data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Add New";
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/add_new', $data);
			}else{
			$this->load->view('cms/header',$data);		 
			$this->load->view('cms/aside');	
			$this->load->view('cms/add_new');
			$this->load->view('cms/footer');		 
			}
		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_add_new() {
					$data = array(
                        'IdBuilder' 			=> $this->input->post('IdBuilder'),
                        'UnderPage' 			=> $this->input->post('UnderPage'),
                        'Title' 				=> $this->input->post('Title'),
                        'MetaKeyword' 			=> $this->input->post('MetaKeyword'),
                        'MetaDescription' 		=> $this->input->post('MetaDescription'),
                        'DatePicker' 			=> $this->input->post('DatePicker'),
                        'Description' 			=> $this->input->post('Description'),
                        'OtherText' 			=> $this->input->post('OtherText'),
                        'ExternalURL' 			=> $this->input->post('ExternalURL'),
                        'OrderBy' 				=> $this->input->post('OrderBy'),
                        'Header' 				=> $this->input->post('Header'),
                        'Footer' 				=> $this->input->post('Footer'),
                        'Status' 				=> 1,
                        'Created' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
						'SEO'      				=> $this->custom->seo_Url($this->input->post('Title'))
                    );
					
					$table = 'pages';
					$query = $this->query->insert_query($table, $data);
					$CreatedID = $query['CreatedID'];
					if ($query['query'] == 1) {

					/*------Multiple Image--------*/	
					$name_array = array();
					$count = count($_FILES['UploadImage']['size']);
					foreach($_FILES as $key=>$value)
					for($s=0; $s < $count; $s++) {
					$_FILES['UploadImage']['name']			= $value['name'][$s];
					$_FILES['UploadImage']['type']    		= $value['type'][$s];
					$_FILES['UploadImage']['tmp_name'] 		= $value['tmp_name'][$s];
					$_FILES['UploadImage']['error']       	= $value['error'][$s];
					$_FILES['UploadImage']['size']    		= $value['size'][$s];  
					$config['upload_path'] 					= 'assets/upload/';
					$config['allowed_types'] 				= 'gif|jpg|png';
					
					$this->load->library('upload', $config);
					
					if($this->upload->do_upload('UploadImage')){
					$data = $this->upload->data();
					$Image = $data['file_name'];
						$data = array(
							   'Image'			=> $Image,
							   'IdPages'		=> $CreatedID,
							   'SetOrder'		=> $s
						);
						$table   = 'images';
						$query = $this->query->insert_query($table, $data);
						}
					}				
					/*------Multiple Image--------*/	
					
					/*------Multiple File--------*/	
				if(isset($_FILES['UploadFile']['name'])){
					if(count($_FILES['UploadFile']['name']) > 0){
						//Loop through each file
						for($i=0; $i<count($_FILES['UploadFile']['name']); $i++) {
						  //Get the temp file path
							$tmpFilePath = $_FILES['UploadFile']['tmp_name'][$i];

							//Make sure we have a filepath
							if($tmpFilePath != ""){
							
								//save the filename
								$File 	  = uniqid().'-'.$_FILES['UploadFile']['name'][$i];

								//save the url and the file
								$filePath = "assets/upload/" . $File;

								//Upload the file into the temp dir
									if(move_uploaded_file($tmpFilePath, $filePath)) {

									$data = array(
								   'File'			=> $File,
								   'IdPages'		=> $CreatedID,
								   'SetOrder'		=> $i
									);
									
									$table   = 'files';
									$query = $this->query->insert_query($table, $data);
									}

								}
							  }
					}  
				}	/*------Multiple file--------*/	

						echo "1";
					}else{
						echo "0";
					}

	}
	
	public function edit_pages($ID) {
		
		    if ($this->session->userdata('Is_Login') == true) {
				
			$data['menu']        = $this->custom->menuBar();
			
			$explode    = explode('_', $ID);
			$IdBuilder  = $explode[0];
			$IdPages    = $explode[1];
					
			$row = $this->query->query("SELECT * FROM `builder` WHERE `Status` = '1'");
			$data['menu'] = $row;

			$row = $this->query->query("SELECT * FROM `builder` WHERE `IdBuilder` = ".base64_decode($IdBuilder));
			$data['PageSettings'] = $row;
			
			$row = $this->query->query("SELECT * FROM `pages` WHERE `Status` = '1' AND `IdBuilder` = '2' ");
			$data['UnderPage'] = $row;
			
			$row = $this->query->query("SELECT * FROM `pages` WHERE `IdPages` = '".base64_decode($IdPages)."'");
			$data['EditPages'] = $row;
			
			$row = $this->query->query("SELECT * FROM `images`");
			$data['Images'] = $row;
			
			$row = $this->query->query("SELECT * FROM `files`");
			$data['File'] = $row;
			
			
			$id 	= $this->session->userdata('AccountId');
			$type 	= $this->session->userdata('FullName');
			$data['site_Info']    = $this->custom->site_Info();
			$data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Edit";
				if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
					$this->load->view('cms/edit_pages', $data);
				}else{
					$this->load->view('cms/header',$data);		 
					$this->load->view('cms/aside');	
					$this->load->view('cms/edit_pages');
					$this->load->view('cms/footer');		 
				}

		
		} else {
            redirect('cms/');
        }
    }
	
	public function do_edit_pages() {
                    $IdPages 				    = $this->input->post('IdPages');
					$data = array(
                        'IdBuilder' 			=> $this->input->post('IdBuilder'),
                        'UnderPage' 			=> $this->input->post('UnderPage'),
                        'Title' 				=> $this->input->post('Title'),
                        'MetaKeyword' 			=> $this->input->post('MetaKeyword'),
                        'MetaDescription' 		=> $this->input->post('MetaDescription'),
                        'DatePicker' 			=> $this->input->post('DatePicker'),
                        'Description' 			=> $this->input->post('Description'),
                        'OtherText' 			=> $this->input->post('OtherText'),
                        'ExternalURL' 			=> $this->input->post('ExternalURL'),
                        'OrderBy' 				=> $this->input->post('OrderBy'),
                        'Header' 				=> $this->input->post('Header'),
                        'Footer' 				=> $this->input->post('Footer'),
                        'Status' 				=> 1,
                        'Created' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
						'SEO'      				=> $this->custom->seo_Url($this->input->post('Title'))
                    );
					
					$table = 'pages';
					$key   = 'IdPages';
					$query = $this->query->update_query($table, $data, $key, $IdPages);
					if ($query) {
											
						/*------Multiple Image--------*/	
					$name_array = array();
					$count = count($_FILES['UploadImage']['size']);
					foreach($_FILES as $key=>$value)
					for($s=0; $s < $count; $s++) {
					$_FILES['UploadImage']['name']			= $value['name'][$s];
					$_FILES['UploadImage']['type']    		= $value['type'][$s];
					$_FILES['UploadImage']['tmp_name'] 		= $value['tmp_name'][$s];
					$_FILES['UploadImage']['error']       	= $value['error'][$s];
					$_FILES['UploadImage']['size']    		= $value['size'][$s];  
					$config['upload_path'] 					= 'assets/upload/';
					$config['allowed_types'] 				= 'gif|jpg|png';
					
					$this->load->library('upload', $config);
					
					if($this->upload->do_upload('UploadImage')){
					$data = $this->upload->data();
					$Image = $data['file_name'];
						$data = array(
							   'Image'			=> $Image,
							   'IdPages'		=> $IdPages,
							   'SetOrder'		=> $s
						);
						$table   = 'images';
						$query = $this->query->insert_query($table, $data);
						}
					}				
					/*------Multiple Image--------*/	
					
					/*------Multiple File--------*/	
				if(isset($_FILES['UploadFile']['name'])){	
					if(count($_FILES['UploadFile']['name']) > 0){
						//Loop through each file
						for($i=0; $i<count($_FILES['UploadFile']['name']); $i++) {
						  //Get the temp file path
							$tmpFilePath = $_FILES['UploadFile']['tmp_name'][$i];

							//Make sure we have a filepath
							if($tmpFilePath != ""){
							
								//save the filename
								$File 	  = uniqid().'-'.$_FILES['UploadFile']['name'][$i];

								//save the url and the file
								$filePath = "assets/upload/" . $File;

								//Upload the file into the temp dir
									if(move_uploaded_file($tmpFilePath, $filePath)) {

									$data = array(
								   'File'			=> $File,
								   'IdPages'		=> $IdPages,
								   'SetOrder'		=> $i
									);
									
									$table   = 'files';
									$query = $this->query->insert_query($table, $data);
									}

								}
							  }
					}  
				}
						/*------Multiple file--------*/	
						
						echo "1";
					}else{
						echo "0";
					}

	}
	
	public function get_Json($ID) {
        $row = $this->query->query("SELECT * FROM `pages` WHERE `IdBuilder` = '".base64_decode($ID)."' ORDER BY IdPages DESC");
        $data['get_Json'] = $row;
		//print_r($data['get_Json']);
        $count  = 1;
		if(count($data['get_Json']) > 0){
			foreach ($data['get_Json'] as $sj) {
				$Action = "";			
				$Action .= str_replace('=','',base64_encode($sj->IdPages));
				$rows[0] = $count++;
				$rows[1] = $sj->Title;
				$rows[2] = $sj->Created;
				$rows[3] = $Action;
				$rows[4] = $sj->Status;
				$response['aaData'][] = $rows;
			}
			echo json_encode($response, true);
		}else{
				$rows[0] = "";
				$rows[1] = "";
				$rows[2] = "";
				$rows[3] = "";
				$response['aaData'][] = $rows;
			echo json_encode($response, true);	
		}
    }
	
	public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
	
	public function delete($id) {
					$table = 'pages';
                    $key   = 'IdPages';
					$row = $this->query->query("SELECT * FROM `pages` WHERE `IdPages` = ".base64_decode($id));
					$data['pages'] = $row;
					if(count($data['pages']) > 0){
						$query = $this->query->delete($table, $key, $id);
						if ($query) {
						$table = 'files';
						$key   = 'IdPages';
						$query = $this->query->delete($table, $key, $id);
						
						$table = 'images';
						$key   = 'IdPages';
						$query = $this->query->delete($table, $key, $id);
							echo 1;
						}else{
							echo 0;	
						}
					}else{
						echo 0;
					}
    }
	
	public function delete_image($id) {
		$table = 'images';
		$key   = 'IdImages';
		$query = $this->query->delete_image($table, $id, $key);
		if ($query){
		echo 1;
		}else{
		echo 0;
		}
	}
	public function delete_file($id) {
		$table = 'files';
		$key   = 'IdFiles';
		$query = $this->query->delete_image($table, $id, $key);
		if ($query){
		echo 1;
		}else{
		echo 0;
		}
	}
	
	public function status($id) {
		$explode   = explode('_', $id);
		$IdPages   = $explode[0];
		$Status    = $explode[1];
                if ($Status == 1) {
                    $ac = 0;
                } else {
                    $ac = 1;
                }
                $data = array(
                    'Status' => $ac
                );
                $table  = 'pages';
                $key 	= 'IdPages';
                $query  = $this->query->status($table, $data, $key, $IdPages, $ac);
                if ($query) {
                    echo 1;
                }else{
					echo 0;
				}
	}
}
