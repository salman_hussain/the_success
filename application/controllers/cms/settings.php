<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('cms/query');
        $this->load->library('custom');
    }

    public function index() {
			
        if ($this->session->userdata('Is_Login') == true) {
			
			$data['menu']        = $this->custom->menuBar();	
			
            $id 				  = $this->session->userdata('AccountId');
            $type 				  = $this->session->userdata('FullName');
            
			$data['site_Info']    = $this->custom->site_Info();
            $data['profile_Info'] = $this->custom->profile_Info($id);
			$data['P_title']      = "Settings";
            
			$row = $this->query->query("SELECT * FROM `settings` WHERE `IdSettings` = 1");
			$data['Settings'] = $row;
			
			if(isset($_GET['ajaxcall']) && $_GET['ajaxcall'] == 1){
			$this->load->view('cms/settings', $data);
			}else{
			$this->load->view('cms/header',$data);		 
		    $this->load->view('cms/aside');	
			$this->load->view('cms/settings');
			$this->load->view('cms/footer');		 
			}
			
        } else {
            redirect('cms/');
        }
    }
	
	public function do_edit_settings() {
                   
				   $IdSettings 			= $this->input->post('IdSettings');
						
					/*------Image--------*/	
					$name_array = array();
					$count = count($_FILES['Logo']['size']);
					foreach($_FILES as $key=>$value)
					for($s=0; $s < $count; $s++) {
					$_FILES['Logo']['name']			= $value['name'][$s];
					$_FILES['Logo']['type']    		= $value['type'][$s];
					$_FILES['Logo']['tmp_name'] 		= $value['tmp_name'][$s];
					$_FILES['Logo']['error']       	= $value['error'][$s];
					$_FILES['Logo']['size']    		= $value['size'][$s];  
					$config['upload_path'] 					= 'assets/upload/';
					$config['allowed_types'] 				= 'gif|jpg|png';
					
					$this->load->library('upload', $config);
					
					if($this->upload->do_upload('Logo')){
					$data = $this->upload->data();
					$Logo = $data['file_name'];
						$data = array(
							   'Logo'			=> $Logo
						);
						$table = 'settings';
						$key   = 'IdSettings';
						$query = $this->query->update_query($table, $data, $key, $IdSettings);
						}
					}				
					/*------Image--------*/
						
					$data = array(
                        'Title' 				=> $this->input->post('Title'),
                        'MetaKeyword' 			=> $this->input->post('MetaKeyword'),
                        'MetaDescription' 		=> $this->input->post('MetaDescription'),
                        'EmailTo' 				=> $this->input->post('EmailTo'),
                        'EmailFrom' 			=> $this->input->post('EmailFrom'),
                        'Mobile' 				=> $this->input->post('Mobile'),
                        'Phone' 				=> $this->input->post('Phone'),
                        'Address' 				=> $this->input->post('Address'),
                        'HeaderText' 			=> $this->input->post('HeaderText'),
                        'FooterText' 			=> $this->input->post('FooterText'),
                        'Copyright' 			=> $this->input->post('Copyright'),
                        'Text_1' 				=> $this->input->post('Text_1'),   
						'Text_2' 				=> $this->input->post('Text_2'),
                        'Text_3' 				=> $this->input->post('Text_3'),
                        'Facebook' 				=> $this->input->post('Facebook'),
                        'Twitter' 				=> $this->input->post('Twitter'),
                        'GooglePlus' 			=> $this->input->post('GooglePlus'),
                        'LinkedIn' 				=> $this->input->post('LinkedIn'),						
                        'Updated' 				=> date("Y-m-d H:i:s"),
                        'AccountId' 			=> $this->session->userdata('AccountId'),
                    );
					
					$table = 'settings';
					$key   = 'IdSettings';
					$query = $this->query->update_query($table, $data, $key, $IdSettings);
					if ($query) {
						echo "1";
					}else{
						echo "0";
					}

	}
	
	public function delete_image($IdSettings) {
		$data = array(
			'Logo' 				=> ''
		);
		$table = 'settings';
		$key   = 'IdSettings';
		$query = $this->query->update_query($table, $data, $key, $IdSettings);
		if ($query){
		echo 1;
		}else{
		echo 0;
		}
	}
}
