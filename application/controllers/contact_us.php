<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class contact_us extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('query');
        $this->load->library('Custom');
        $this->load->library('pagination');
    }

    public function index() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        /* pages */
        $row = $this->query->query("SELECT * FROM `pages`  LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE Status  = 1  AND SEO = '" . $this->get_view() . "'");
        $data['Pages'] = $row;
        /* pages */


        $this->load->view('contact_us', $data);
    }

    public function send_email() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();


        $this->load->view('contact_us', $data);
    }

    public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
    
    public function contact_msg() {
        
        $site_Info = $this->custom->site_Info();


        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $msg = $this->input->post('msg');

        $to1 = $email;
        $subject1 = "The Success Inquiry";
        $txt1 = "[AUTO-RESPONSE, please do not reply]\n\n\nDear Customer,\n\nIt is to confirm that we have received your inquiry. Our representative\nwill respond to your inquiry shortly.\n\nRegards,\n\TheSuccess";
        $from2 = $site_Info['EmailFrom'];
        $headers = "From: TheSuccess <$from2>" . "\r\n";

        mail($to1, $subject1, $txt1, $headers);


        $to2 = $site_Info['EmailTo'];
        $subject2 = "TheSuccess Inquiry";
        $txt2 = "Name: $name \nEmail: $email\nMessage: $msg";
        $from2 = $site_Info['EmailFrom'];
        $headers = "From: TheSuccess <$from2>" . "\r\n";

        mail($to2, $subject1, $txt1, $headers);


        echo "1";
    }

}
