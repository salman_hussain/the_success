<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shops extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('query');
        $this->load->library('Custom');
        $this->load->library('pagination');
    }

    public function index() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        /* pages */
        $row = $this->query->query("SELECT * FROM `pages`  LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE Status  = 1  AND SEO = '" . $this->get_view() . "'");
        $data['Pages'] = $row;
        /* pages */

        /* shops */
        $row = $this->query->query("SELECT * FROM `pages` LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE pages.Status  = 1 AND  pages.IdBuilder = 7 GROUP BY pages.IdPages ORDER BY pages.OrderBy ASC");
        $data['Shops'] = $row;
        /* shops */

        $this->load->view('shops', $data);
    }

    public function details() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        /* shops */
        $row = $this->query->query("SELECT * FROM `pages`  LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE pages.IdBuilder = 7 AND Status  = 1  AND SEO = '" . $this->get_view() . "'");
        $data['Shops'] = $row;
        /* shops */
		
		/* ShopsImages */
        $row = $this->query->query("SELECT * FROM `images` WHERE IdPages = '" .  $data['Shops'][0]->IdPages . "'");
        $data['ShopsImages'] = $row;
        /* ShopsImages */
        $this->load->view('shops_details', $data);
    }

    public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }

}
