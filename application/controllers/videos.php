<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Videos extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('query');
        $this->load->library('Custom');
        $this->load->library('pagination');
    }

    public function index() {
		$data['site_Info']    = $this->custom->site_Info();
        $data['Menu']         = $this->custom->menu();
		
		/* pages */
	  $row = $this->query->query("SELECT * FROM `pages`  LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE Status  = 1  AND SEO = '".$this->get_view()."'");
	  $data['Pages'] = $row;
		/* pages */
		
		/* videos */
	  $row = $this->query->query("SELECT * FROM `pages` LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE pages.Status  = 1 AND pages.IdBuilder = 3 ORDER BY pages.OrderBy ASC");
	  $data['Videos'] = $row;
		/* videos */
	
	    $this->load->view('videos', $data);
    }
	
	public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
	
	function get_youtube_video_image($youtube_code)
{
	// get the video code if this is an embed code	(old embed)
	preg_match('/youtube\.com\/v\/([\w\-]+)/', $youtube_code, $match);
 
	// if old embed returned an empty ID, try capuring the ID from the new iframe embed
	if($match[1] == '')
		preg_match('/youtube\.com\/embed\/([\w\-]+)/', $youtube_code, $match);
 
	// if it is not an embed code, get the video code from the youtube URL	
	if($match[1] == '')
		preg_match('/v\=(.+)&/',$youtube_code ,$match);
 
	// get the corresponding thumbnail images	
	$full_size_thumbnail_image = "http://img.youtube.com/vi/".$match[1]."/0.jpg";
	$small_thumbnail_image1    = "http://img.youtube.com/vi/".$match[1]."/1.jpg";
	$small_thumbnail_image2    = "http://img.youtube.com/vi/".$match[1]."/2.jpg";
	$small_thumbnail_image3    = "http://img.youtube.com/vi/".$match[1]."/3.jpg";
 
	// return whichever thumbnail image you would like to retrieve
	return $full_size_thumbnail_image;		
}
}
