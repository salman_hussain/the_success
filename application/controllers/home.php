<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->helper('text');
        $this->load->library('image_lib');
        $this->load->model('query');
        $this->load->library('Custom');
        $this->load->library('pagination');
        $this->load->library('cart');
        $this->load->library('paypal_lib');
    }

    public function index() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();
		
		/* pages */
	  $row = $this->query->query("SELECT * FROM `pages`  LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE Status  = 1  AND SEO = 'home' ");
	  $data['Pages'] = $row;
		/* pages */
		
		
        /* Slider */
        $row = $this->query->query("SELECT * FROM `pages` LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE pages.Status  = 1 AND  pages.IdBuilder = 1 ORDER BY pages.OrderBy ASC");
        $data['Slider'] = $row;
        /* Slider */

        /* shops */
        $row = $this->query->query("SELECT * FROM `pages` LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE pages.Status  = 1 AND  pages.IdBuilder = 7 GROUP BY pages.IdPages ORDER BY pages.OrderBy ASC");
        $data['Shops'] = $row;
        /* shops */

        /* Blogs List */
        $row = $this->query->query("SELECT * FROM `pages` LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE pages.Status  = 1 AND  pages.IdBuilder = 4 ORDER BY pages.IdPages DESC LIMIT 4");
        $data['BlogsList'] = $row;
        /* Blogs List */

        $this->load->view('home', $data);
    }

    public function header_cart_view() {

        $cart_check1 = $this->cart->total_items();
        echo $cart_check1;
    }

    public function add_cart() {

        $id = $this->input->post('id');
        $qty = $this->input->post('qty');
        $price = $this->input->post('price');
        $user_id = $this->input->post('user_id');
        $count = 0;

        foreach ($this->cart->contents() as $items) {

            if ($items['id'] == $id) {
                $count = 1;
                $Qty = $items['qty'] + $qty;
                $row_id = $items['rowid'];
            }
        }

        if ($count == 0) {
            $insert_data = array(
                'id' => $id,
                'price' => $price,
                'name' => 'xyz',
                'qty' => $qty,
                'options' => array('user_id' => $user_id)
            );

            $add = $this->cart->insert($insert_data);
        } else {
            $data = array(
                'rowid' => $row_id,
                'qty' => $Qty
            );

            $add = $this->cart->update($data);
            //$add = true;
        }


        if ($add) {
            $this->header_cart_view();
        } else {
            echo "-1";
        }
    }

    public function empty_cart() {
        $this->cart->destroy();
        redirect(base_url());
    }

    public function remove_cart_item() {
        $pId = $this->input->post('pId');
        //echo $carId;

        $data = array(
            'rowid' => $pId,
            'qty' => 0
        );

        $add = $this->cart->update($data);

        if ($add) {
            echo "1";
        } else {
            echo "-1";
        }
    }

    public function view_cart() {

        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        $this->load->view('view_cart', $data);
    }
	
	public function load_cart() {

        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        $this->load->view('load_cart', $data);
    }

    public function checkout() {

        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();
        $this->load->view('checkout', $data);
    }

    public function submit() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        $OrderNo = uniqid();
        //Set variables for paypal form
        $paypalURL = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //test PayPal api url
        $paypalID = 'murtazamurtaza18@gmail.com'; //business email
        $returnURL = base_url() . 'home/success?i=' . $OrderNo; //payment success url
        $cancelURL = base_url() . 'home/cancel?i=' . $OrderNo; //payment cancel url
        $notifyURL = base_url() . 'home/ipn?i=' . $OrderNo; //ipn url
        //get particular product data

        $userID = $this->session->userdata('user_id'); //current user id
        $logo = base_url() . '/assets/upload/logo.png';

        $this->paypal_lib->add_field('business', $paypalID);
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('custom', $userID);

        $this->session->set_userdata('OrderNo', $OrderNo);

        $count = 0;
        $total = 0;
        $cart_check = $this->cart->contents();
        foreach ($cart_check as $item) {
            $count++;
            $this->paypal_lib->add_field('item_name_' . $count, $this->menu->product_name($item['id']));
            $this->paypal_lib->add_field('amount_' . $count, $item['price']);
            $this->paypal_lib->add_field('quantity_' . $count, $item['qty']);
            $total += $item['qty'] * $item['price'];
        }

        $this->paypal_lib->image($logo);


        /* orders table */
        $data = array(
            'OrderNo' => $OrderNo,
            'FirstName' => $this->input->post('FirstName'),
            'LastName' => $this->input->post('LastName'),
            'Company' => $this->input->post('Company'),
            'Phone' => $this->input->post('Phone'),
            'Address' => $this->input->post('Address'),
            'City' => $this->input->post('City'),
            'Country' => $this->input->post('Country'),
            'Total' => $total,
            'AccountId' => $userID,
            'Created' => date("Y-m-d H:i:s"),
            'Status' => 0, // In progress
            'ViewOrder' => 0 // new order
        );
        $table = 'orders';
        $query = $this->query->insert_query($table, $data);
        $IdOrders = $query['CreatedID'];
        /* order details table */
        foreach ($cart_check as $item) {
            $data = array(
                'IdOrders' => $IdOrders,
                'OrderNo' => $OrderNo,
                'ItemID' => $item['id'],
                'ItemName' => $this->menu->product_name($item['id']),
                'Price' => $item['price'],
                'Qty' => $item['qty']
            );

            $table = 'order_details';
            $query = $this->query->insert_query($table, $data);
        }

        $this->paypal_lib->paypal_auto_form();
    }

    function success() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        //get the transaction data
        $paypalInfo = $this->input->get();

        if ($this->input->get('i') && $this->input->get('i') != "") {
            $OrderNo = $this->input->get('i');
            $Success = json_encode($_REQUEST);
            $array = array(
                'Success' => $Success,
                'Status' => 1 // Success order
            );
            $table = 'orders';
            $query = $this->query->update_query($table, $array, 'OrderNo', $OrderNo);
        } else {
            $OrderNo = $this->session->userdata('OrderNo');
            $Success = json_encode($_REQUEST);
            $array = array(
                'Success' => $Success,
                'Status' => 1 // Success order
            );
            $table = 'orders';
            $query = $this->query->update_query($table, $array, 'OrderNo', $OrderNo);
        }

        $data['msg'] = "Thank you for your order. Your order id is <strong>'" . $OrderNo . "'</strong>.<br> Your transaction has been completed successfully and a confirmation email has been sent to your account.<br> <strong>Keep shopping</strong><br><br>";
        $this->cart->destroy();
        $this->load->view('success', $data);
    }

    function cancel() {
        $data['site_Info'] = $this->custom->site_Info();
        $data['Menu'] = $this->custom->menu();

        if ($this->input->get('i') && $this->input->get('i') != "") {
            $OrderNo = $this->input->get('i');
            $array = array(
                'Status' => 2 // Cancel order
            );
            $table = 'orders';
            $query = $this->query->update_query($table, $array, 'OrderNo', $OrderNo);
        } else {
            $OrderNo = $this->session->userdata('OrderNo');
            $array = array(
                'Status' => 2 // Cancel order
            );
            $table = 'orders';
            $query = $this->query->update_query($table, $array, 'OrderNo', $OrderNo);
        }
        $data['msg'] = "Your order has been successfully canceled.";
        $this->cart->destroy();
        $this->load->view('cancel', $data);
    }

    function ipn() {
        //paypal return transaction details array
        $paypalInfo = $this->input->post();

        $data['user_id'] = $paypalInfo['custom'];
        $data['product_id'] = $paypalInfo["item_number"];
        $data['txn_id'] = $paypalInfo["txn_id"];
        $data['payment_gross'] = $paypalInfo["payment_gross"];
        $data['currency_code'] = $paypalInfo["mc_currency"];
        $data['payer_email'] = $paypalInfo["payer_email"];
        $data['payment_status'] = $paypalInfo["payment_status"];

        $paypalURL = $this->paypal_lib->paypal_url;
        $result = $this->paypal_lib->curlPost($paypalURL, $paypalInfo);
        if ($this->input->get('i') && $this->input->get('i') != "") {
            $OrderNo = $this->input->get('i');
            $IPN = json_encode($_REQUEST);
            $array = array(
			    'Updated' => date("Y-m-d H:i:s"),
                'IPN' => $IPN,
                'Status' => 1 // Success order
            );
            $table = 'orders';
            $query = $this->query->update_query($table, $array, 'OrderNo', $OrderNo);

            $to = 'no-reply@mail.com';    //  your email
            if ($this->paypal_lib->validate_ipn()) {
                $body = 'An instant payment notification was successfully received from ';
                $body .= $this->paypal_lib->ipn_data['payer_email'] . ' on ' . date('m/d/Y') . ' at ' . date('g:i A') . "\n\n";
                $body .= " Details:\n";

                foreach ($this->paypal_lib->ipn_data as $key => $value)
                    $body .= "\n$key: $value";

                // load email lib and email results
                $this->load->library('email');
                $this->email->to($to);
                $this->email->from($this->paypal_lib->ipn_data['payer_email'], $this->paypal_lib->ipn_data['payer_name']);
                $this->email->subject('CI paypal_lib IPN (Received Payment)');
                $this->email->message($body);
                $this->email->send();
            }
        } else {
            $OrderNo = $this->session->userdata('OrderNo');
            $IPN = json_encode($_REQUEST);
            $array = array(
			    'Updated' => date("Y-m-d H:i:s"),
                'IPN' => $IPN,
                'Status' => 1 // Success order
            );
            $table = 'orders';
            $query = $this->query->update_query($table, $array, 'OrderNo', $OrderNo);

                $to = 'murtazamurtaza18@gmail.com';    //  your email
            if ($this->paypal_lib->validate_ipn()) {
                $body = 'An instant payment notification was successfully received from ';
                $body .= $this->paypal_lib->ipn_data['payer_email'] . ' on ' . date('m/d/Y') . ' at ' . date('g:i A') . "\n\n";
                $body .= " Details:\n";

                foreach ($this->paypal_lib->ipn_data as $key => $value)
                    $body .= "\n$key: $value";

                // load email lib and email results
                $this->load->library('email');
                $this->email->to($to);
                $this->email->from($this->paypal_lib->ipn_data['payer_email'], $this->paypal_lib->ipn_data['payer_name']);
                $this->email->subject('CI paypal_lib IPN (Received Payment)');
                $this->email->message($body);
                $this->email->send();
            }
        }
        //check whether the payment is verified
        if (eregi("VERIFIED", $result)) {
            //insert the transaction data into the database
            if ($this->input->get('i') && $this->input->get('i') != "") {
                $OrderNo = $this->input->get('i');
                $IPN = json_encode($_REQUEST);
                $array = array(
                    'IPN' => $IPN,
                    'Status' => 1 // Success order
                );
                $table = 'orders';
                $query = $this->query->update_query($table, $array, 'OrderNo', $OrderNo);

                $to = 'murtazamurtaza18@gmail.com';    //  your email
                if ($this->paypal_lib->validate_ipn()) {
                    $body = 'An instant payment notification was successfully received from ';
                    $body .= $this->paypal_lib->ipn_data['payer_email'] . ' on ' . date('m/d/Y') . ' at ' . date('g:i A') . "\n\n";
                    $body .= " Details:\n";

                    foreach ($this->paypal_lib->ipn_data as $key => $value)
                        $body .= "\n$key: $value";

                    // load email lib and email results
                    $this->load->library('email');
                    $this->email->to($to);
                    $this->email->from($this->paypal_lib->ipn_data['payer_email'], $this->paypal_lib->ipn_data['payer_name']);
                    $this->email->subject('CI paypal_lib IPN (Received Payment)');
                    $this->email->message($body);
                    $this->email->send();
                }
            } else {
                $OrderNo = $this->session->userdata('OrderNo');
                $IPN = json_encode($_REQUEST);
                $array = array(
				    'Updated' => date("Y-m-d H:i:s"),
                    'IPN' => $IPN,
                    'Status' => 1 // Success order
                );
                $table = 'orders';
                $query = $this->query->update_query($table, $array, 'OrderNo', $OrderNo);

                $to = 'murtazamurtaza18@gmail.com';    //  your email
                if ($this->paypal_lib->validate_ipn()) {
                    $body = 'An instant payment notification was successfully received from ';
                    $body .= $this->paypal_lib->ipn_data['payer_email'] . ' on ' . date('m/d/Y') . ' at ' . date('g:i A') . "\n\n";
                    $body .= " Details:\n";

                    foreach ($this->paypal_lib->ipn_data as $key => $value)
                        $body .= "\n$key: $value";

                    // load email lib and email results
                    $this->load->library('email');
                    $this->email->to($to);
                    $this->email->from($this->paypal_lib->ipn_data['payer_email'], $this->paypal_lib->ipn_data['payer_name']);
                    $this->email->subject('CI paypal_lib IPN (Received Payment)');
                    $this->email->message($body);
                    $this->email->send();
                }
            }
        }
    }
    
    public function subtract_cart_item()
    {
        $pId = $this->input->post('pId');
        
        //echo $carId;
        
        foreach ($this->cart->contents() as $items) {

            if ($items['rowid'] == $pId) {
                $Qty = $items['qty'];
            }
        }
        
        $data = array(
            'rowid' => $pId,
            'qty' => $Qty-1
        );

        $add = $this->cart->update($data);

        if ($add) {
            echo "1";
        } else {
            echo "0";
        }
    }
    
    public function add_cart_item()
    {
        $pId = $this->input->post('pId');
        
        //echo $carId;
        
        foreach ($this->cart->contents() as $items) {

            if ($items['rowid'] == $pId) {
                $Qty = $items['qty'];
            }
        }
        
        $data = array(
            'rowid' => $pId,
            'qty' => $Qty+1
        );

        $add = $this->cart->update($data);

        if ($add) {
            echo "1";
        } else {
            echo "0";
        }
    }
	public function get_view() {
        $total_segments = $this->uri->total_segments();
        $segments = $this->uri->segment($total_segments);
        return $segments;
    }
}
