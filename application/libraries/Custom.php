<?php
class Custom {
	
	private $CI;
	
	 function __construct() {
       $this->CI =& get_instance();
       $this->CI->load->database();
	 }
	
  		public function seo_Url($string){
			
			$url = str_replace("'", '', $string);
			$url = str_replace('%20', ' ', $url);
			$url = preg_replace('~[^\\pL0-9_]+~u', '-', $url); 
			// substitutes anything but letters, numbers and '_' with separator
			$url = trim($url, "-");
			$url = iconv("utf-8", "us-ascii//TRANSLIT", $url); 
			// you may opt for your own custom character map for encoding.
			$url = strtolower($url);
			$url = preg_replace('~[^-a-z0-9_]+~', '', $url); 
			// keep only letters, numbers, '_' and separator
			return $url;
		}
		
	   public function site_Info(){
		  
		  $query = $this->CI->db->query('SELECT * FROM `settings` WHERE `IdSettings` = "1"');
		  $query = $query->result();
			
		  foreach ($query as $row){
				
					$data['Title'] 			 = $row->Title;
					$data['MetaKeywords'] 	 = $row->MetaKeyword;
					$data['MetaDescription'] = $row->MetaDescription;
					$data['HeaderText'] 	 = $row->HeaderText;
					$data['FooterText'] 	 = $row->FooterText;
					$data['Logo'] 	 		 = $row->Logo;
					$data['EmailTo'] 	 	 = $row->EmailTo;
					$data['EmailFrom'] 	 	 = $row->EmailFrom;
					$data['Phone'] 	 		 = $row->Phone;
					$data['Mobile'] 	 	 = $row->Mobile;
					$data['Copyright'] 	 	 = $row->Copyright;
					
					$data['Facebook'] 	 	 = $row->Facebook;
					$data['Twitter'] 	 	 = $row->Twitter;
					$data['GooglePlus'] 	 = $row->GooglePlus;
					$data['LinkedIn'] 	 	 = $row->LinkedIn;
					
					$data['Address'] 	 	 = $row->Address;
					
				
			}
		 return $data;
		
		}
		
		
		 public function profile_Info($ID){
		  
		  $query = $this->CI->db->query("SELECT * FROM `accounts`  WHERE IdAccount =".$ID);
		  $query = $query->result();
			
		  foreach ($query as $row){
			$data['Pro_IdAccount'] 		= $row->IdAccount;
			$data['Pro_FullName'] 		= $row->FullName;
			$data['Pro_ProfileImage'] 	= $row->ProfileImage;
		  }
		 return $data;
		
		}
	
		public function menuBar(){
				$row = $this->CI->query->query("SELECT * FROM `builder` WHERE `Status` = '1'");
				return $row;
		}
		
		public function menu(){
				$row = $this->CI->query->query("SELECT * FROM `pages` WHERE `Status` = '1'");
				return $row;
		}
}
?>