<?php

class Menu {

    private $ci;            // para CodeIgniter Super Global Referencias o variables globales
    private $id_menu = '';
    private $class_menu = '';
    private $class_parent = '';
    private $class_last = '';

    // --------------------------------------------------------------------
    /**
     * PHP5        Constructor
     *
     */
    function __construct()
    {
        $this->ci = &get_instance();    // get a reference to CodeIgniter.
    }

    // --------------------------------------------------------------------
    /**
     * build_menu($table, $type)
     *
     * Description:
     *
     * builds the Dynaminc dropdown menu
     * $table allows for passing in a MySQL table name for different menu tables.
     * $type is for the type of menu to display ie; topmenu, mainmenu, sidebar menu
     * or a footer menu.
     *
     * @param    string    the MySQL database table name.
     * @param    string    the type of menu to display.
     * @return    string    $html_out using CodeIgniter achor tags.
     */
    

    /**
     * get_childs($menu, $parent_id) - SEE Above Method.
     *
     * Description:
     *
     * Builds all child submenus using a recurse method call.
     *
     * @param    mixed    $id
     * @param    string    $id usuario
     * @return    mixed    $html_out if has subcats else FALSE
     */
    
    
    function product_name($id)
    {
        $query = $this->ci->db->query("SELECT * FROM `pages` LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE pages.IdPages = $id ");
        $result = $query->result();
        return $result[0]->Title;
    }
    
    function product_image($id)
    {
        $query = $this->ci->db->query("SELECT * FROM `pages` LEFT JOIN `images` ON pages.IdPages = images.IdPages WHERE pages.IdPages = $id ");
        $result = $query->result();
        return $result[0]->Image;
    }
    
    function get_child_menu($id) {
        
        $query = $this->ci->db->query("SELECT * FROM pages_post WHERE status = 1 AND underPage = $id order by orderBy");
        $result = $query->result();
        $html_out = '';
        if(count($result) != 0){
            $html_out .= '<ul>';
            for($i = 0; $i < count($result) ; $i++) {

                $html_out .= '<li>'
                        . '<a href="'.base_url().'home/pages/'.$result[$i]->SEO.'" >'.$result[$i]->title.'</a>';

                $html_out .= '</li>';

            }
            $html_out .= '</ul>';
        }
        
        return $html_out;
        
    }
    
    function get_menu() {
        
        $query = $this->ci->db->query("SELECT * FROM pages_post WHERE status = 1 AND ShowOnHeader = 1 order by orderBy");
        
        $html_out = '';
        
        $result = $query->result();
        
        
        for($i = 0; $i < count($result) ; $i++) {
            $dropdown = $this->get_child_menu($result[$i]->pageId);
            
            $html_out .= '<li>'
                    . '<a href="'.base_url().'home/pages/'.$result[$i]->SEO.'" >'.$result[$i]->title.'</a>';
           
            
            $html_out .= $dropdown;
            
            $html_out .= '</li>';
            
        }
        
        return $html_out;
    }

    function dealer_name($id)
    {
        $query = $this->ci->db->query("SELECT * FROM accounts WHERE accountId = $id ");
        $result = $query->result();
        return $result[0]->name;
    }
    
    function total_users()
    {
        $query = $this->ci->db->query("SELECT count(*) as count FROM accounts where account_type = 3 and status = 1");
        $result = $query->result();
        return $result[0]->count;
    }
    
    function total_dealers()
    {
        $query = $this->ci->db->query("SELECT count(*) as count FROM accounts where account_type = 4 and status = 1");
        $result = $query->result();
        return $result[0]->count;
    }

    function total_users_day()
    {
        $query = $this->ci->db->query("select count(DISTINCT(user_ip)) as count from current_users where created = CURDATE()");
        $result = $query->result();
        return $result[0]->count;
    }
    
    function total_users_month()
    {
        $query = $this->ci->db->query("select count(DISTINCT(user_ip)) as count from current_users where DATE_FORMAT(created,'%m') = DATE_FORMAT(CURDATE(),'%m')");
        $result = $query->result();
        return $result[0]->count;
    }
    
    function total_users_week()
    {
        $query = $this->ci->db->query("select count(DISTINCT(user_ip)) as count from current_users where created BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE()");
        $result = $query->result();
        return $result[0]->count;
    }
	
	function product_dealer_name($id)
    {
        $query = $this->ci->db->query("SELECT product.username FROM `product` WHERE product.proID = $id ");
        $result = $query->result();
        return $result[0]->username;
    }
    

}

// ------------------------------------------------------------------------
// End of Dynamic_menu Library Class.
// ------------------------------------------------------------------------
/* End of file Dynamic_menu.php */
/* Location: ../application/libraries/Dynamic_menu.php */