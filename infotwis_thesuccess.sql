-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 18, 2016 at 10:48 AM
-- Server version: 5.6.28-log
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `infotwis_thesuccess`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `IdAccount` int(11) NOT NULL AUTO_INCREMENT,
  `FullName` varchar(45) DEFAULT NULL,
  `EmailAddress` varchar(45) DEFAULT NULL,
  `Password` text,
  `ProfileImage` varchar(45) DEFAULT NULL,
  `PhoneNumber` varchar(45) DEFAULT NULL,
  `city` varchar(20) NOT NULL,
  `country` varchar(20) NOT NULL,
  `Website` text,
  `Twitter` varchar(45) DEFAULT NULL,
  `Facebook` varchar(45) DEFAULT NULL,
  `LinkedIn` varchar(45) DEFAULT NULL,
  `Googleplus` varchar(45) DEFAULT NULL,
  `YouTube` varchar(45) DEFAULT NULL,
  `Instagram` varchar(45) DEFAULT NULL,
  `Pinterest` varchar(45) DEFAULT NULL,
  `IdAccountType` int(11) NOT NULL,
  `SEO` text,
  `Status` text,
  `Created` datetime DEFAULT NULL,
  `Updated` datetime DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdAccount`),
  KEY `fk_account_account_type_idx` (`IdAccountType`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`IdAccount`, `FullName`, `EmailAddress`, `Password`, `ProfileImage`, `PhoneNumber`, `city`, `country`, `Website`, `Twitter`, `Facebook`, `LinkedIn`, `Googleplus`, `YouTube`, `Instagram`, `Pinterest`, `IdAccountType`, `SEO`, `Status`, `Created`, `Updated`, `AccountId`) VALUES
(1, 'admin', 'admin@mail.com', '85tOoZypnY6HpqvTjNZm+g15iT0F8kTikowC8uoMyac=', 'thumb3.jpg', '', '', '', '', '', '0', '', '', '', '', '', 1, 'admin', '1', '2016-04-25 00:00:00', '2016-04-27 05:41:53', 1),
(2, 'Ghulam Murtaza', 'murtazamurtaza18@gmail.com', '85tOoZypnY6HpqvTjNZm+g15iT0F8kTikowC8uoMyac=', 'thumb2.jpg', '', '', '', '', '', '0', '', '', '', '', '', 5, 'Ghulam-Murtaza', '1', '2016-04-25 00:00:00', '2016-04-27 05:41:01', 2),
(4, 'john', 'john@mail.com', '85tOoZypnY6HpqvTjNZm+g15iT0F8kTikowC8uoMyac=', 'clientlogo2.jpg', '1212121212', '', '', '#', '#', '0', '#', '#', '#', '#', '#', 6, NULL, '1', '2016-04-27 12:16:01', '2016-04-27 12:24:32', 1),
(5, 'zain', 'zainshafimemon@gmail.com', 'QApGqDOckbzwZ+G0ypQs5uLhSwmRxH4KrDV3LJhJFR8=', NULL, 'adfsf', 'Karachi', 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-05-22 00:00:00', NULL, NULL),
(9, 'salman', 'salman.hussainali@gmail.com', 'zhTMhMtHi+E947dfS8/S5b260rk75yDeE7XneT2LYNU=', NULL, '03319203940', 'karachi', 'pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-05-24 00:00:00', NULL, NULL),
(10, 'zain bandukda', 'zainumemon@gmail.com', 'VBt3Mnm7mxXQd1h2/KS/fSbO16UuHDQHLoH+P/wie/g=', NULL, '789456', 'Karachi', 'Pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-05-30 00:00:00', NULL, NULL),
(11, 'arbish', 'Stroardlean88@rhyta.com', 'IEE05xdWmqdSYIf6ROgcwkYz/ld88V2Nq/xL6X4I+FY=', NULL, '90078601', 'karachi', 'pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-06-01 00:00:00', NULL, NULL),
(12, 'server', 'asasasas@rhyta.com', 'WIyf21Bn4A0zpsoSEJVWK7p8hx0iPJbsQWPqKxnPLSI=', NULL, '123456', 'asd', 'asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-06-01 00:00:00', NULL, NULL),
(13, 'newuser', 'assdasasas@rhyta.com', 'WIyf21Bn4A0zpsoSEJVWK7p8hx0iPJbsQWPqKxnPLSI=', NULL, '1', 's', 'a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-06-01 00:00:00', NULL, NULL),
(14, 'admin', 'Behat1972@dayrep.com', '85tOoZypnY6HpqvTjNZm+g15iT0F8kTikowC8uoMyac=', NULL, '342929680', 'karachi', 'pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-06-02 00:00:00', NULL, NULL),
(15, 'asd', 'Behat1972@dayrep.com', 'VyvhS640u1Y5ufKAVmDI+Ff6+PZzxm0cK2d2KS9acjw=', NULL, '03002175125', 'karachi', 'pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-06-02 00:00:00', NULL, NULL),
(16, 'umar', 'umar.maqsood06@gmail.com', 'Ve5cYpjRrlpT48ONr9AsbofFgiR4Fry74inD0RfG5qM=', NULL, '232323', 'karachi', 'pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-06-02 00:00:00', NULL, NULL),
(19, 'kelly', 'kelly@superrito.com', 'VyvhS640u1Y5ufKAVmDI+Ff6+PZzxm0cK2d2KS9acjw=', NULL, '12345', 'karachi', 'pakistan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL, '1', '2016-06-13 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `account_type`
--

CREATE TABLE IF NOT EXISTS `account_type` (
  `IdAccountType` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(45) DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Updated` datetime DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdAccountType`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `account_type`
--

INSERT INTO `account_type` (`IdAccountType`, `Type`, `Status`, `Created`, `Updated`, `AccountId`) VALUES
(1, 'Admin', '1', '2016-04-06 22:29:39', '2016-04-06 22:29:42', NULL),
(2, 'Subadmin', '1', '2016-04-06 22:29:39', '2016-04-06 22:29:42', NULL),
(5, 'Developera', '1', '2016-04-06 22:29:39', '2016-04-24 21:35:51', 1),
(6, 'Users', '1', '2016-05-22 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `builder`
--

CREATE TABLE IF NOT EXISTS `builder` (
  `IdBuilder` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `Label_1` varchar(255) DEFAULT NULL,
  `Status_Label_1` int(11) DEFAULT NULL,
  `Label_2` varchar(255) DEFAULT NULL,
  `Status_Label_2` int(11) DEFAULT NULL,
  `Label_3` varchar(255) DEFAULT NULL,
  `Status_Label_3` int(11) DEFAULT NULL,
  `Label_4` varchar(255) DEFAULT NULL,
  `Status_Label_4` int(11) DEFAULT NULL,
  `Label_5` varchar(255) DEFAULT NULL,
  `Status_Label_5` int(11) DEFAULT NULL,
  `Label_6` varchar(255) DEFAULT NULL,
  `Status_Label_6` int(11) DEFAULT NULL,
  `Label_7` varchar(255) DEFAULT NULL,
  `Status_Label_7` int(11) DEFAULT NULL,
  `Label_8` varchar(255) DEFAULT NULL,
  `Status_Label_8` int(11) DEFAULT NULL,
  `Label_9` varchar(255) DEFAULT NULL,
  `Status_Label_9` int(11) DEFAULT NULL,
  `Label_10` varchar(255) DEFAULT NULL,
  `Status_Label_10` int(11) DEFAULT NULL,
  `Status_Label_11` int(11) DEFAULT NULL,
  `Status_Label_12` int(11) DEFAULT NULL,
  `Status_Label_13` int(11) DEFAULT NULL,
  `Status_Label_14` int(11) DEFAULT NULL,
  `Status_Label_15` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Updated` datetime DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  `SEO` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IdBuilder`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `builder`
--

INSERT INTO `builder` (`IdBuilder`, `Title`, `Label_1`, `Status_Label_1`, `Label_2`, `Status_Label_2`, `Label_3`, `Status_Label_3`, `Label_4`, `Status_Label_4`, `Label_5`, `Status_Label_5`, `Label_6`, `Status_Label_6`, `Label_7`, `Status_Label_7`, `Label_8`, `Status_Label_8`, `Label_9`, `Status_Label_9`, `Label_10`, `Status_Label_10`, `Status_Label_11`, `Status_Label_12`, `Status_Label_13`, `Status_Label_14`, `Status_Label_15`, `Status`, `Created`, `Updated`, `AccountId`, `SEO`) VALUES
(1, 'Slider', 'Title', 1, '', 0, '', 0, '', 0, '', 0, 'Upload Image', 1, '', 0, 'Description', 1, 'External URL', 1, 'OrderBY', 1, 0, 0, 0, 0, 0, 1, '2016-05-07 09:02:58', NULL, 2, 'slider'),
(2, 'Pages', 'Title', 1, 'Meta Keyword', 1, 'Meta Description', 1, '', 0, '', 0, 'Upload Image', 0, '', 0, 'Description', 1, '', 0, '', 0, 1, 0, 0, 0, 0, 1, '2016-05-30 08:51:46', NULL, 2, 'pages'),
(3, 'Videos', 'Title', 1, '', 0, '', 0, '', 0, '', 0, '', 0, '', 0, 'Video URl', 1, '', 0, '', 0, 0, 0, 0, 0, 0, 1, '2016-05-08 10:16:02', NULL, 2, 'videos'),
(4, 'Blogs', 'Title', 1, '', 0, '', 0, '', 0, 'Short Description', 1, 'Upload Images (Size 800 X 350)', 1, '', 0, 'Description', 1, '', 0, '', 0, 1, 1, 0, 0, 0, 1, '2016-06-14 07:00:23', NULL, 2, 'blogs'),
(5, 'Events', 'Title', 1, '', 0, '', 0, 'Date', 1, '', 0, 'Upload Images (Size 800 X 350)', 1, '', 0, 'Description', 1, '', 0, '', 0, 1, 1, 0, 0, 0, 1, '2016-06-09 08:02:04', NULL, 2, 'events'),
(6, 'Glitz Life', 'Title', 1, '', 0, '', 0, '', 0, '', 0, 'Upload Images (Size 800 X 350)', 1, '', 0, 'Description', 1, '', 0, '', 0, 1, 1, 0, 0, 0, 1, '2016-06-09 08:02:11', NULL, 2, 'glitz-life'),
(7, 'Shops', 'Title', 1, 'Meta Keyword', 1, 'Meta Description', 1, '', 0, 'Price', 1, 'Upload Images', 1, '', 0, 'Description', 1, '', 0, '', 0, 1, 1, 0, 0, 0, 1, '2016-05-07 08:55:01', NULL, 2, 'shops');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `IdFiles` int(11) NOT NULL AUTO_INCREMENT,
  `File` text,
  `IdPages` int(11) DEFAULT NULL,
  `SetOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdFiles`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `IdImages` int(11) NOT NULL AUTO_INCREMENT,
  `Image` text,
  `IdPages` int(11) DEFAULT NULL,
  `SetOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdImages`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`IdImages`, `Image`, `IdPages`, `SetOrder`) VALUES
(4, 'savor_life_mag_NEW_cover.png', 5, 0),
(5, 'savor_life_mag_on_silver2.png', 5, 1),
(6, 'Savor_Life_Bag_with_3_goodies.jpg', 6, 0),
(7, 'savor_life_bag_with_flowers.jpg', 6, 1),
(10, 'slideshow_3.jpg', 8, 0),
(11, '1.jpg', 4, 0),
(12, '2.jpg', 16, 0),
(13, '3.jpg', 17, 0),
(17, 'g1.jpg', 20, 0),
(20, '487799_473635909370580_813110884_n1.jpg', 21, 0),
(23, 'Lighthouse.jpg', 19, 0),
(24, 'Desert.jpg', 18, 0),
(25, '487799_473635909370580_813110884_n2.jpg', 25, 0),
(26, 'Koala.jpg', 26, 0),
(27, '9.jpg', 1, 0),
(28, '10.jpg', 7, 0),
(29, 'Screen_Shot_2016-06-07_at_2.35_.52_pm_.png', 23, 0),
(30, 'girl-running-mountain-4049139.jpg', 29, 0),
(33, '5-Ways-Socially-Mature-Brands-Deliver-Great-Customer-Experience1.jpg', 30, 0),
(34, 'screenshot_071.jpg', 31, 0),
(36, 'Jellyfish.jpg', 32, 1),
(37, 'Koala1.jpg', 32, 2),
(38, 'Lighthouse1.jpg', 32, 3),
(39, 'Penguins.jpg', 32, 4),
(40, 'Tulips.jpg', 32, 5);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `IdOrders` int(11) NOT NULL AUTO_INCREMENT,
  `OrderNo` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Company` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Address` text,
  `City` varchar(255) DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `Total` varchar(255) DEFAULT NULL,
  `Success` text,
  `IPN` text,
  `Status` varchar(255) DEFAULT NULL,
  `ViewOrder` varchar(255) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Updated` datetime DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdOrders`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`IdOrders`, `OrderNo`, `FirstName`, `LastName`, `Company`, `Phone`, `Address`, `City`, `Country`, `Total`, `Success`, `IPN`, `Status`, `ViewOrder`, `Created`, `Updated`, `AccountId`) VALUES
(1, '5749594e4f285', 'w', 'w', 'w', 'w', 'w', 'w', 'w', '74.95', '{"home\\/success":"","i":"5749594e4f285","ci_session":"a:10:{s:10:\\"session_id\\";s:32:\\"1d044d188c891c351540ec639858d39d\\";s:10:\\"ip_address\\";s:14:\\"111.92.139.247\\";s:10:\\"user_agent\\";s:72:\\"Mozilla\\/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko\\/20100101 Firefox\\/41.0\\";s:13:\\"last_activity\\";i:1464424917;s:9:\\"user_data\\";s:0:\\"\\";s:7:\\"user_id\\";s:1:\\"4\\";s:9:\\"user_name\\";s:4:\\"john\\";s:9:\\"user_Type\\";s:1:\\"6\\";s:10:\\"user_Login\\";b:1;s:7:\\"OrderNo\\";s:13:\\"5749594e4f285\\";}b84aa91d9b92d5e283141a62a71e03178373bee9"}', '{"home\\/ipn":"","i":"5749594e4f285","mc_gross":"74.95","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","tax":"0.00","item_number2":"","payer_id":"WLDSPVXPSX9YS","address_street":"1 Main St","payment_date":"01:41:26 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"2","mc_handling1":"0.00","mc_handling2":"0.00","address_city":"San Jose","verify_sign":"A.w52znvfNdR8HJSw8YllvGBM2I5AHrip580jjCqggqgs8.DwVcYMglB","payer_email":"umar.maqsood06@gmail.com","mc_shipping1":"0.00","mc_shipping2":"0.00","tax1":"0.00","tax2":"0.00","txn_id":"70290234B3409590D","payment_type":"instant","last_name":"FY","address_state":"CA","item_name1":"SAVOR LIFE MAGAZINE: Volume 1","receiver_email":"murtazamurtaza18@gmail.com","item_name2":"Dream, Do, Savor Gift Bag","quantity1":"1","quantity2":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"19.95","mc_currency":"USD","mc_gross_2":"55.00","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"74.95","ipn_track_id":"27a1cce1b755d"}', '1', '0', '2016-05-28 01:39:42', NULL, 4),
(2, '57495a44db6aa', 't', 't', 't', 't', 'ttt', 't', 't', '74.95', '{"home\\/success":"","i":"57495a44db6aa","ci_session":"a:10:{s:10:\\"session_id\\";s:32:\\"1d044d188c891c351540ec639858d39d\\";s:10:\\"ip_address\\";s:14:\\"111.92.139.247\\";s:10:\\"user_agent\\";s:72:\\"Mozilla\\/5.0 (Windows NT 6.1; WOW64; rv:41.0) Gecko\\/20100101 Firefox\\/41.0\\";s:13:\\"last_activity\\";i:1464424917;s:9:\\"user_data\\";s:0:\\"\\";s:7:\\"user_id\\";s:1:\\"4\\";s:9:\\"user_name\\";s:4:\\"john\\";s:9:\\"user_Type\\";s:1:\\"6\\";s:10:\\"user_Login\\";b:1;s:7:\\"OrderNo\\";s:13:\\"57495a44db6aa\\";}8478d554c81f2ca6e415d9580b11282dc2f27ce5"}', '{"home\\/ipn":"","i":"57495a44db6aa","mc_gross":"74.95","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","tax":"0.00","item_number2":"","payer_id":"WLDSPVXPSX9YS","address_street":"1 Main St","payment_date":"01:44:10 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"2","mc_handling1":"0.00","mc_handling2":"0.00","address_city":"San Jose","verify_sign":"AyPYlQ8Oa6oGsLhFw3LOipLJYfWnAnpfgd1eEwesWIE.5uNqTSzusDov","payer_email":"umar.maqsood06@gmail.com","mc_shipping1":"0.00","mc_shipping2":"0.00","tax1":"0.00","tax2":"0.00","txn_id":"9Y741217CS1700311","payment_type":"instant","last_name":"FY","address_state":"CA","item_name1":"Dream, Do, Savor Gift Bag","receiver_email":"murtazamurtaza18@gmail.com","item_name2":"SAVOR LIFE MAGAZINE: Volume 1","quantity1":"1","quantity2":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","mc_gross_2":"19.95","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"74.95","ipn_track_id":"53b510844295c"}', '1', '0', '2016-05-28 01:43:48', NULL, 4),
(3, '57495a812b862', 'asa', 'as', 'as', 'asa', 'asa', 'as', 'a', '55', '{"home\\/success":"","i":"57495a812b862","mc_gross":"55.00","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"01:49:39 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"1","mc_handling1":"0.00","address_city":"San Jose","payer_email":"umar.maqsood06@gmail.com","verify_sign":"AbUFAFbCbZb1fkAmj-W6lYEbcukIAWU284tpD3qFNP3kobHc2I6JqpOS","mc_shipping1":"0.00","tax1":"0.00","txn_id":"31H387270J7800354","payment_type":"instant","last_name":"FY","item_name1":"Dream, Do, Savor Gift Bag","address_state":"CA","receiver_email":"murtazamurtaza18@gmail.com","quantity1":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"55.00","merchant_return_link":"click here","auth":"AlR5mrePRF-tTkhMrM4DsnKoAxxq6RjvXDj1L2l79.HfdTTSvBkj6iwlROtRI1Wkfu-ZKG7AtmTRwQNoitFSokQ","ci_session":"a:11:{s:10:\\"session_id\\";s:32:\\"2ea340cd60f8612e79332cdabbccaa2b\\";s:10:\\"ip_address\\";s:14:\\"111.88.237.194\\";s:10:\\"user_agent\\";s:73:\\"Mozilla\\/5.0 (Windows NT 10.0; WOW64; rv:46.0) Gecko\\/20100101 Firefox\\/46.0\\";s:13:\\"last_activity\\";i:1464424995;s:9:\\"user_data\\";s:0:\\"\\";s:13:\\"cart_contents\\";a:3:{s:32:\\"1679091c5a880faf6fb5e6087eb1b2dc\\";a:7:{s:5:\\"rowid\\";s:32:\\"1679091c5a880faf6fb5e6087eb1b2dc\\";s:2:\\"id\\";s:1:\\"6\\";s:5:\\"price\\";s:5:\\"55.00\\";s:4:\\"name\\";s:3:\\"xyz\\";s:3:\\"qty\\";s:1:\\"1\\";s:7:\\"options\\";a:1:{s:7:\\"user_id\\";b:0;}s:8:\\"subtotal\\";d:55;}s:11:\\"total_items\\";i:1;s:10:\\"cart_total\\";d:55;}s:7:\\"user_id\\";s:1:\\"4\\";s:9:\\"user_name\\";s:4:\\"john\\";s:9:\\"user_Type\\";s:1:\\"6\\";s:10:\\"user_Login\\";b:1;s:7:\\"OrderNo\\";s:13:\\"57495a812b862\\";}8838dec1f34968da8e776a6d0b30bf1121908d2a"}', '{"home\\/ipn":"","i":"57495a812b862","mc_gross":"55.00","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"01:49:39 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"1","mc_handling1":"0.00","address_city":"San Jose","verify_sign":"AtTUbrYua2iMJ1DG25fRxu4tmoI5Am4TDhSML8Hpf0N9pmCNrwYPsYmU","payer_email":"umar.maqsood06@gmail.com","mc_shipping1":"0.00","tax1":"0.00","txn_id":"31H387270J7800354","payment_type":"instant","last_name":"FY","address_state":"CA","item_name1":"Dream, Do, Savor Gift Bag","receiver_email":"murtazamurtaza18@gmail.com","quantity1":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"55.00","ipn_track_id":"1bc658c97a1bd"}', '1', '0', '2016-05-28 01:44:49', NULL, 4),
(4, '57495b469bf44', 'k', 'k', 'k', 'k', 'k', 'k', 'k', '55', '{"home\\/success":"","i":"57495b469bf44","mc_gross":"55.00","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"01:48:59 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"1","mc_handling1":"0.00","address_city":"San Jose","payer_email":"umar.maqsood06@gmail.com","verify_sign":"AFcWxV21C7fd0v3bYYYRCpSSRl31A9z6g4vGNF68lRSipwFS1LRBkkLr","mc_shipping1":"0.00","tax1":"0.00","txn_id":"0Y557549TG028223U","payment_type":"instant","last_name":"FY","item_name1":"Dream, Do, Savor Gift Bag","address_state":"CA","receiver_email":"murtazamurtaza18@gmail.com","quantity1":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"55.00","merchant_return_link":"click here","auth":"ATcn4Lk7QKh8OhUglp2yJRs8wFc1vQfzRpGyKGYBq7vonhoUf4ij50IiblT3xvdmAFKhOCkKIIMDA.frTA063GQ","__gads":"ID=60454694f1a0764a:T=1438105915:S=ALNI_MYnsysrHNTW5F6lVOppJ_b4Qk2PhQ","_ga":"GA1.2.14484537.1461356637","ci_session":"a:11:{s:10:\\"session_id\\";s:32:\\"3970ea5ce2b6171d77d39d2074d2421a\\";s:10:\\"ip_address\\";s:14:\\"111.92.139.247\\";s:10:\\"user_agent\\";s:109:\\"Mozilla\\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/50.0.2661.102 Safari\\/537.36\\";s:13:\\"last_activity\\";i:1464425123;s:9:\\"user_data\\";s:0:\\"\\";s:13:\\"cart_contents\\";a:3:{s:32:\\"1679091c5a880faf6fb5e6087eb1b2dc\\";a:7:{s:5:\\"rowid\\";s:32:\\"1679091c5a880faf6fb5e6087eb1b2dc\\";s:2:\\"id\\";s:1:\\"6\\";s:5:\\"price\\";s:5:\\"55.00\\";s:4:\\"name\\";s:3:\\"xyz\\";s:3:\\"qty\\";s:1:\\"1\\";s:7:\\"options\\";a:1:{s:7:\\"user_id\\";b:0;}s:8:\\"subtotal\\";d:55;}s:11:\\"total_items\\";i:1;s:10:\\"cart_total\\";d:55;}s:7:\\"user_id\\";s:1:\\"4\\";s:9:\\"user_name\\";s:4:\\"john\\";s:9:\\"user_Type\\";s:1:\\"6\\";s:10:\\"user_Login\\";b:1;s:7:\\"OrderNo\\";s:13:\\"57495b469bf44\\";}50c88ddb85b6bd5966aae12d7c41c5a3e6deb85f"}', '{"home\\/ipn":"","i":"57495b469bf44","mc_gross":"55.00","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"01:48:59 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"1","mc_handling1":"0.00","address_city":"San Jose","verify_sign":"A-D5eqOkDkHGCI9HY7K8XPx4DkAFAvZSI5lFxZDo6FmQ0.e9PrFN55lO","payer_email":"umar.maqsood06@gmail.com","mc_shipping1":"0.00","tax1":"0.00","txn_id":"0Y557549TG028223U","payment_type":"instant","last_name":"FY","address_state":"CA","item_name1":"Dream, Do, Savor Gift Bag","receiver_email":"murtazamurtaza18@gmail.com","quantity1":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"55.00","ipn_track_id":"ab5be3a0cda8f"}', '1', '0', '2016-05-28 01:48:06', NULL, 4),
(5, '57495bf85a4eb', 'hg', 'fg', 'fg', 'gf', 'fg', 'g', 'fg', '74.95', NULL, NULL, '0', '0', '2016-05-28 01:51:04', NULL, 4),
(6, '57495ca34bb30', '0', '0', '0', '0', '0', '0', '0', '74.95', NULL, NULL, '0', '0', '2016-05-28 01:53:55', NULL, 4),
(7, '57495cebe96d9', 'hg', 'fg', 'fg', 'gf', 'fg', 'g', 'fg', '74.95', '{"home\\/success":"","i":"57495cebe96d9","mc_gross":"74.95","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","item_number2":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"01:55:32 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"2","mc_handling1":"0.00","mc_handling2":"0.00","address_city":"San Jose","payer_email":"umar.maqsood06@gmail.com","verify_sign":"A6GbfHo0f54HJaPEhIkd43uKNQ-JAuie6X0T5ZO78RF9KRKmPszSMpDA","mc_shipping1":"0.00","mc_shipping2":"0.00","tax1":"0.00","tax2":"0.00","txn_id":"16438685DC352140G","payment_type":"instant","last_name":"FY","item_name1":"Dream, Do, Savor Gift Bag","address_state":"CA","receiver_email":"murtazamurtaza18@gmail.com","item_name2":"SAVOR LIFE MAGAZINE: Volume 1","quantity1":"1","quantity2":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","mc_gross_2":"19.95","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"74.95","merchant_return_link":"click here","auth":"Aw-SlF-loYSDkvJJDKbTIOVm3yaVa3R9AFmYw7MRYSAhDpvwoyneYLhhsgP5PrjGiYtBRVOuHAuzjAMIUHkHtxg","__gads":"ID=60454694f1a0764a:T=1438105915:S=ALNI_MYnsysrHNTW5F6lVOppJ_b4Qk2PhQ","_ga":"GA1.2.14484537.1461356637","ci_session":"a:11:{s:10:\\"session_id\\";s:32:\\"d10a2dcdfec5332b0ff8ead8b7d21e20\\";s:10:\\"ip_address\\";s:14:\\"111.92.139.247\\";s:10:\\"user_agent\\";s:109:\\"Mozilla\\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/50.0.2661.102 Safari\\/537.36\\";s:13:\\"last_activity\\";i:1464425443;s:9:\\"user_data\\";s:0:\\"\\";s:7:\\"user_id\\";s:1:\\"4\\";s:9:\\"user_name\\";s:4:\\"john\\";s:9:\\"user_Type\\";s:1:\\"6\\";s:10:\\"user_Login\\";b:1;s:7:\\"OrderNo\\";s:13:\\"57495cebe96d9\\";s:13:\\"cart_contents\\";a:4:{s:32:\\"1679091c5a880faf6fb5e6087eb1b2dc\\";a:7:{s:5:\\"rowid\\";s:32:\\"1679091c5a880faf6fb5e6087eb1b2dc\\";s:2:\\"id\\";s:1:\\"6\\";s:5:\\"price\\";s:5:\\"55.00\\";s:4:\\"name\\";s:3:\\"xyz\\";s:3:\\"qty\\";s:1:\\"1\\";s:7:\\"options\\";a:1:{s:7:\\"user_id\\";b:0;}s:8:\\"subtotal\\";d:55;}s:32:\\"e4da3b7fbbce2345d7772b0674a318d5\\";a:7:{s:5:\\"rowid\\";s:32:\\"e4da3b7fbbce2345d7772b0674a318d5\\";s:2:\\"id\\";s:1:\\"5\\";s:5:\\"price\\";s:5:\\"19.95\\";s:4:\\"name\\";s:3:\\"xyz\\";s:3:\\"qty\\";s:1:\\"1\\";s:7:\\"options\\";a:1:{s:7:\\"user_id\\";b:0;}s:8:\\"subtotal\\";d:19.949999999999999289457264239899814128875732421875;}s:11:\\"total_items\\";i:2;s:10:\\"cart_total\\";d:74.9500000000000028421709430404007434844970703125;}}b83dba24f28460ae27b3ffe1f4c6c95cf6fbd651"}', '{"home\\/ipn":"","i":"57495cebe96d9","mc_gross":"74.95","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","tax":"0.00","item_number2":"","payer_id":"WLDSPVXPSX9YS","address_street":"1 Main St","payment_date":"01:55:32 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"2","mc_handling1":"0.00","mc_handling2":"0.00","address_city":"San Jose","verify_sign":"Ae4fHI1yoAGg.VY6dmhnWmixPLMfAWtyknaFGJWRBJfcTRKy02PnLiPY","payer_email":"umar.maqsood06@gmail.com","mc_shipping1":"0.00","mc_shipping2":"0.00","tax1":"0.00","tax2":"0.00","txn_id":"16438685DC352140G","payment_type":"instant","last_name":"FY","address_state":"CA","item_name1":"Dream, Do, Savor Gift Bag","receiver_email":"murtazamurtaza18@gmail.com","item_name2":"SAVOR LIFE MAGAZINE: Volume 1","quantity1":"1","quantity2":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","mc_gross_2":"19.95","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"74.95","ipn_track_id":"538b6901a05b2"}', '1', '0', '2016-05-28 01:55:07', NULL, 4),
(8, '57495d34adcc5', 'f', 'ff', 'f', 'f', 'ff', 'f', 'f', '55', '{"home\\/success":"","i":"57495d34adcc5","mc_gross":"55.00","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"01:57:18 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"1","mc_handling1":"0.00","address_city":"San Jose","payer_email":"umar.maqsood06@gmail.com","verify_sign":"Ai5Td855iftein9bGGUh391pRoTeAQLO82sszSvG6mp8xSE6hGNxFt.c","mc_shipping1":"0.00","tax1":"0.00","txn_id":"9YE66813525177939","payment_type":"instant","last_name":"FY","item_name1":"Dream, Do, Savor Gift Bag","address_state":"CA","receiver_email":"murtazamurtaza18@gmail.com","quantity1":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"55.00","merchant_return_link":"click here","auth":"A6ZTtXUAG-LvHmSM0y7Y0vpMI0Y.oVYpbsLPi1NmfBF5fq32uJuZrqdFoJK9cMZaQwZ7kJPgXGL.P55fNrgKOoQ","__gads":"ID=60454694f1a0764a:T=1438105915:S=ALNI_MYnsysrHNTW5F6lVOppJ_b4Qk2PhQ","_ga":"GA1.2.14484537.1461356637","ci_session":"a:11:{s:10:\\"session_id\\";s:32:\\"6184fb682cd3c9a785993ce2d5e0904e\\";s:10:\\"ip_address\\";s:14:\\"111.92.139.247\\";s:10:\\"user_agent\\";s:109:\\"Mozilla\\/5.0 (Windows NT 6.1; WOW64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/50.0.2661.102 Safari\\/537.36\\";s:13:\\"last_activity\\";i:1464425750;s:9:\\"user_data\\";s:0:\\"\\";s:7:\\"user_id\\";s:1:\\"4\\";s:9:\\"user_name\\";s:4:\\"john\\";s:9:\\"user_Type\\";s:1:\\"6\\";s:10:\\"user_Login\\";b:1;s:7:\\"OrderNo\\";s:13:\\"57495d34adcc5\\";s:13:\\"cart_contents\\";a:3:{s:32:\\"1679091c5a880faf6fb5e6087eb1b2dc\\";a:7:{s:5:\\"rowid\\";s:32:\\"1679091c5a880faf6fb5e6087eb1b2dc\\";s:2:\\"id\\";s:1:\\"6\\";s:5:\\"price\\";s:5:\\"55.00\\";s:4:\\"name\\";s:3:\\"xyz\\";s:3:\\"qty\\";s:1:\\"1\\";s:7:\\"options\\";a:1:{s:7:\\"user_id\\";b:0;}s:8:\\"subtotal\\";d:55;}s:11:\\"total_items\\";i:1;s:10:\\"cart_total\\";d:55;}}7a11915cc88cc3052ba4bba092c65d649df64568"}', '{"home\\/ipn":"","i":"57495d34adcc5","mc_gross":"55.00","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"01:57:18 May 28, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"4","payer_status":"verified","address_country":"United States","num_cart_items":"1","mc_handling1":"0.00","address_city":"San Jose","verify_sign":"AHRdi57TESH7SBc8-jTiaBLvLxXSAzdBsjGQS0BSaiaqCzbxJ6QP-Mmj","payer_email":"umar.maqsood06@gmail.com","mc_shipping1":"0.00","tax1":"0.00","txn_id":"9YE66813525177939","payment_type":"instant","last_name":"FY","address_state":"CA","item_name1":"Dream, Do, Savor Gift Bag","receiver_email":"murtazamurtaza18@gmail.com","quantity1":"1","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"55.00","mc_currency":"USD","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"55.00","ipn_track_id":"10bb8f9f1c9f8"}', '1', '0', '2016-05-28 01:56:20', NULL, 4),
(9, '574c5831801f1', 'sad', 'sadf', 'safd', '8798456', 'afsd', 'asdf', 'asf', '19.95', NULL, NULL, '0', '0', '2016-05-30 08:11:45', NULL, 10),
(10, '574c622cc2120', 'john testing', 'john testing', '', '111111', '11111', 'Pickering', 'Pakistan', '154.75', NULL, NULL, '2', '0', '2016-05-30 08:54:20', NULL, 4),
(11, '574ed8eae6f30', 'arbish', 'palla', 'avialdo', '090078601', 'defence phase 4', 'karachi', 'pakistan', '159.6', NULL, '{"home\\/ipn":"","i":"574ed8eae6f30","mc_gross":"159.60","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"05:50:08 Jun 01, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"13","payer_status":"verified","address_country":"United States","num_cart_items":"1","mc_handling1":"0.00","address_city":"San Jose","verify_sign":"A7TScWF18EM-I3KA.2WYmtjLPnX-A8q8QHgDcHyCeDARZZJjYEHFAqsi","payer_email":"umar.maqsood06@gmail.com","mc_shipping1":"0.00","tax1":"0.00","txn_id":"8Y987695M6463161M","payment_type":"instant","last_name":"FY","address_state":"CA","item_name1":"SAVOR LIFE MAGAZINE: Volume 1","receiver_email":"murtazamurtaza18@gmail.com","quantity1":"8","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"159.60","mc_currency":"USD","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"159.60","ipn_track_id":"629d73541b13"}', '1', '0', '2016-06-01 05:45:30', '2016-06-01 05:50:25', 13),
(13, '575e5dad8eefb', 'kelly', 'john', '', '091213123', 'asdasdasdasdasd', 'karachi', 'pakistan', '275', NULL, '{"home\\/ipn":"","i":"575e5dad8eefb","mc_gross":"275.00","protection_eligibility":"Ineligible","address_status":"confirmed","item_number1":"","payer_id":"WLDSPVXPSX9YS","tax":"0.00","address_street":"1 Main St","payment_date":"00:19:22 Jun 13, 2016 PDT","payment_status":"Pending","charset":"windows-1252","address_zip":"95131","mc_shipping":"0.00","mc_handling":"0.00","first_name":"FY","address_country_code":"US","address_name":"FY FY","notify_version":"3.8","custom":"19","payer_status":"verified","address_country":"United States","num_cart_items":"1","mc_handling1":"0.00","address_city":"San Jose","verify_sign":"AOh0tu.5JUQyG2Aao4MpntBA2sFjAlbxqIvqSXOaPrG-y0bRRudVtqKh","payer_email":"umar.maqsood06@gmail.com","mc_shipping1":"0.00","tax1":"0.00","txn_id":"38W03663HG037315S","payment_type":"instant","last_name":"FY","address_state":"CA","item_name1":"Dream, Do, Savor Gift Bag","receiver_email":"murtazamurtaza18@gmail.com","quantity1":"5","pending_reason":"unilateral","txn_type":"cart","mc_gross_1":"275.00","mc_currency":"USD","residence_country":"US","test_ipn":"1","transaction_subject":"","payment_gross":"275.00","ipn_track_id":"8ac96ba434b9"}', '1', '0', '2016-06-13 00:15:57', '2016-06-13 00:19:30', 19);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE IF NOT EXISTS `order_details` (
  `IorderDetails` int(11) NOT NULL AUTO_INCREMENT,
  `IdOrders` int(11) DEFAULT NULL,
  `OrderNo` varchar(255) DEFAULT NULL,
  `ItemID` int(11) DEFAULT NULL,
  `ItemName` varchar(255) DEFAULT NULL,
  `Price` varchar(255) DEFAULT NULL,
  `Qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`IorderDetails`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`IorderDetails`, `IdOrders`, `OrderNo`, `ItemID`, `ItemName`, `Price`, `Qty`) VALUES
(1, 1, '5749594e4f285', 5, 'SAVOR LIFE MAGAZINE: Volume 1', '19.95', 1),
(2, 1, '5749594e4f285', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(3, 2, '57495a44db6aa', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(4, 2, '57495a44db6aa', 5, 'SAVOR LIFE MAGAZINE: Volume 1', '19.95', 1),
(5, 3, '57495a812b862', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(6, 4, '57495b469bf44', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(7, 5, '57495bf85a4eb', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(8, 5, '57495bf85a4eb', 5, 'SAVOR LIFE MAGAZINE: Volume 1', '19.95', 1),
(9, 6, '57495ca34bb30', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(10, 6, '57495ca34bb30', 5, 'SAVOR LIFE MAGAZINE: Volume 1', '19.95', 1),
(11, 7, '57495cebe96d9', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(12, 7, '57495cebe96d9', 5, 'SAVOR LIFE MAGAZINE: Volume 1', '19.95', 1),
(13, 8, '57495d34adcc5', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(14, 9, '574c5831801f1', 5, 'SAVOR LIFE MAGAZINE: Volume 1', '19.95', 1),
(15, 10, '574c622cc2120', 5, 'SAVOR LIFE MAGAZINE: Volume 1', '19.95', 5),
(16, 10, '574c622cc2120', 6, 'Dream, Do, Savor Gift Bag', '55.00', 1),
(17, 11, '574ed8eae6f30', 5, 'SAVOR LIFE MAGAZINE: Volume 1', '19.95', 8),
(18, 13, '575e5dad8eefb', 6, 'Dream, Do, Savor Gift Bag', '55.00', 5);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `IdPages` int(11) NOT NULL AUTO_INCREMENT,
  `IdBuilder` int(11) DEFAULT NULL,
  `UnderPage` int(11) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MetaKeyword` varchar(255) DEFAULT NULL,
  `MetaDescription` varchar(255) DEFAULT NULL,
  `DatePicker` varchar(255) DEFAULT NULL,
  `OtherText` varchar(255) DEFAULT NULL,
  `Description` text,
  `ExternalURL` text,
  `OrderBy` int(11) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  `Created` datetime DEFAULT NULL,
  `Updated` datetime DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  `SEO` text,
  `Header` int(11) NOT NULL,
  `Footer` int(11) NOT NULL,
  PRIMARY KEY (`IdPages`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`IdPages`, `IdBuilder`, `UnderPage`, `Title`, `MetaKeyword`, `MetaDescription`, `DatePicker`, `OtherText`, `Description`, `ExternalURL`, `OrderBy`, `Status`, `Created`, `Updated`, `AccountId`, `SEO`, `Header`, `Footer`) VALUES
(1, 1, 0, 'Relax during workdays', '0', '0', '0', '0', 'At dawn on the 13th the Carnatic entered the port of Yokohama. This is an important port of call in the Pacific, where all the mail-steamers, and those carrying travellers between North', '#', 1, '1', '2016-06-06 04:15:56', NULL, 2, 'relax-during-workdays', 0, 0),
(2, 2, 0, 'Home', 'Home', 'Home', '0', '0', '<p>Home</p>\r\n', '0', 0, '1', '2016-05-07 09:04:05', NULL, 2, 'home', 1, 1),
(3, 3, 0, 'PrizeBond', '0', '0', '0', '0', 'https://www.youtube.com/watch?v=kttOM-GurEU', '0', 0, '0', '2016-05-08 10:23:08', NULL, 2, 'prizebond', 0, 0),
(4, 4, 0, 'RELAX DURING WORKDAYS', '0', '0', '0', 'Short Description here', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '0', 0, '1', '2016-06-14 16:08:20', NULL, 2, 'relax-during-workdays', 0, 0),
(5, 7, 0, 'SAVOR LIFE MAGAZINE: Volume 1', 'SAVOR LIFE MAGAZINE: Volume 1', 'SAVOR LIFE MAGAZINE: Volume 1', '0', '19.95', '<p>Gift with purchase: a travel-size Om Aroma organic beauty product!</p>\r\n\r\n<p>&quot;If Fast Company + Oprah Magazine had a baby, it would be Savor Life Magazine.&quot; &nbsp;</p>\r\n\r\n<p>Finally, a magazine that is designed for the woman entrepreneur&#39;s lifestyle. Part coffee-table book, part magazine, part workbook, SAVOR LIFE is a stunning publication that addresses all aspects of a busy woman&#39;s life.</p>\r\n\r\n<p>The first of its kind, SAVOR LIFE offers education, inspiration, and wellness content. Our guided worksheets create an interactive experience for the reader through thought-starter exercises.</p>\r\n\r\n<p>DREAM: An idea, a spark, a vision--the biggest visionaries defy &quot;business as usual.&quot; Carving out time to think without the white noise is essential to staying on top of the game. Get inspired to go on creativity vacations, breakthrough limiting beliefs, and dream backwards.</p>\r\n\r\n<p>DO: By taking inspired action and collaborating through win/win partnerships, accomplishing dreams becomes a whole lot more fun. Be challenged to focus on what&#39;s important with the right mindset so that you can bring your visions to life... easier and faster.</p>\r\n\r\n<p>SAVOR: What&#39;s the point of working hard if you are not savoring life? Enjoy the successes you&#39;ve created, eat well, look great, feel good, carve out time for supreme self care. This is non-negotiable &quot;Me Time&quot; so you can give to your family, business, friends and team from a cup of abundance.</p>\r\n\r\n<p>Volume 1 features include:</p>\r\n\r\n<ul>\r\n	<li>Vosges founder, Katrina Markoff, shares how she built her $30 million dollar chocolate enterprise and why you should not &quot;expand until you&#39;re bursting at the seams&quot;.</li>\r\n	<li>Lessons learned from Alexandra Wilson, founder of Gilt + Glamsquad; Heather Thomson, founder of Yummie; Gretchen Rubin, best-selling author of Better than Before</li>\r\n	<li>Easy green recipes for the busy woman</li>\r\n	<li>Beauty lessons from Om Aroma founder, Angela Jia Kim</li>\r\n	<li>Inspiration for Creativity Vacations</li>\r\n	<li>Look out for the next magazine launch in October at the SAVOR LIFE Conference (formerly Rock the World)!</li>\r\n</ul>\r\n\r\n<p>&quot;SAVOR LIFE has blown my mind away. I love how luxurious it feels; the content and photography are superb. It feels more like a coffee table book. This is so much more than a magazine!&quot; - Annie Toan, Arcana Restaurant (Boulder, CO)</p>\r\n', '0', 0, '1', '2016-05-07 08:54:48', NULL, 2, 'savor-life-magazine-volume-1', 0, 0),
(6, 7, 0, 'Dream, Do, Savor Gift Bag', 'Dream, Do, Savor Gift Bag', 'Dream, Do, Savor Gift Bag', '0', '55.00', '<p>The Dream, Do, Savor Gift Package is the perfect gift for inspiration! Gift this to Mom on Mother&#39;s Day, friends, clients, employees, and sisters... and they will be thanking you for a meaningful gift that helps them to savor life.</p>\r\n\r\n<p>&quot;The &#39;Dream, Do, Savor Gift Package&#39; is designed to inspire the busy mom to savor the successes of life, turn her dreams into reality, and have fun in the process.&quot; - MarthaStewart.com</p>\r\n\r\n<p>The package includes:</p>\r\n\r\n<ul>\r\n	<li>(1) Daily Action Planner</li>\r\n	<li>(1) SAVOR Life magazine (with guided worksheets)</li>\r\n	<li>(1) Dream, Do, Savor premium notebook</li>\r\n	<li>Savor Life eco-friendly canvas tote bag</li>\r\n</ul>\r\n\r\n<p>Purchased as a gift set, you save more than 20%!</p>\r\n\r\n<p>Our best-selling Daily Action Planner helps busy women organize their &quot;gorgeous chaos&quot;. It is made of two components that work together: Weekly Pilot Plans and Daily Action Plans.</p>\r\n\r\n<p>Included:</p>\r\n\r\n<ul>\r\n	<li>Instructions</li>\r\n	<li>5 weeks of Weekly Pilot Plan</li>\r\n	<li>25 days of Daily Action Plans</li>\r\n	<li>Extra pages for notes</li>\r\n</ul>\r\n\r\n<p>The Daily Action Planner is designed for a month-at-a-time so that you can quickly reference each month as needed. It&#39;s perfect for the busy entrepreneur, on-the-go mom, managers, and other professionals who need a system to &quot;remember it all&quot; for prioritizing the day.</p>\r\n\r\n<p>SAVOR LIFE is a premium women&#39;s magazine book (with guided worksheets) from beauty to business. The first of its kind, SAVOR LIFE offers education, inspiration, community, wellness and beauty content. Our guided worksheets set the stage for professional and personal growth through meaningful creative exercises.</p>\r\n\r\n<p>The Dream, Do, Savor notebook is a premium journal made with recycled paper and silk-screened by hand.</p>\r\n\r\n<p>&quot;The Daily Action Planner is hands-down the tool that has made the biggest difference in my business. Before using it, I was a scattered mess that was easily distracted. It has anchored me in a powerful way, and I love the ships and frogs system, which keeps me moving forward daily.&quot; - Rosemary Camposano, Founder of HALO Blow Dry Bar (San Francisco, CA)</p>\r\n\r\n<p>&quot;Love my Daily Action Planner - power, potential, and a &quot;can-do&quot; attitude!&quot; - Gretchen Zelek, DoD Fitness</p>\r\n\r\n<p>&quot;The daily ships I&rsquo;ve been sending out from the Daily Action Planner have been coming back big time: from press like USA Today and celebrities like Brad Pitt and Michael Jordan being personally given Good Karmal.&quot; - Patty West, Good Karmal</p>\r\n', '0', 0, '1', '2016-05-07 08:57:14', NULL, 2, 'dream-do-savor-gift-bag', 0, 0),
(7, 1, 0, 'Relax during workdays', '0', '0', '0', '0', 'At dawn on the 13th the Carnatic entered the port of Yokohama. This is an important port of call in the Pacific, where all the mail-steamers, and those carrying travellers between North', '#', 2, '1', '2016-06-06 04:17:12', NULL, 2, 'relax-during-workdays', 0, 0),
(8, 1, 0, 'port of Yokohama.', '0', '0', '0', '0', 'At dawn on the 13th the Carnatic entered the port of Yokohama.  This is an important port of <br>call in the Pacific, where all the mail-steamers, and those carrying travellers between North', 'fb.com', 3, '1', '2016-05-30 07:53:34', NULL, 1, 'port-of-yokohama', 0, 0),
(9, 2, 0, 'About Us', 'About Us', 'About Us', '0', '0', '<p><img alt="" src="http://ourwork.infotwister.com/thesuccess/assets/cms/plugins/ckfinder/userfiles/images/Screen Shot 2016-06-07 at 2_20_21 pm.png" style="height:722px; width:1443px" /></p>\n\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n', '0', 0, '1', '2016-06-07 02:23:44', NULL, 1, 'about-us', 1, 0),
(10, 2, 0, 'Videos', 'Videos', 'Videos', '0', '0', '<p>Videos</p>\r\n', '0', 0, '1', '2016-05-07 09:05:48', NULL, 2, 'videos', 1, 1),
(11, 2, 0, 'Shops', 'Shops', 'Shops', '0', '0', '<p>Shops</p>\r\n', '0', 0, '1', '2016-05-07 09:05:59', NULL, 2, 'shops', 1, 1),
(12, 2, 0, 'Blog', 'Blog', 'Blog', '0', '0', '', '0', 0, '1', '2016-05-08 07:48:54', NULL, 2, 'blog', 1, 1),
(13, 2, 0, 'Events', 'Events', 'Events', '0', '0', '<p>Events</p>\r\n', '0', 0, '1', '2016-05-07 09:06:20', NULL, 2, 'events', 1, 1),
(14, 2, 0, 'Glitz Life', 'Glitz Life', 'Glitz Life', '0', '0', '<p>Glitz Life</p>\r\n', '0', 0, '1', '2016-05-07 09:06:31', NULL, 2, 'glitz-life', 1, 1),
(15, 2, 0, 'Contact Us', 'Contact Us', 'Contact Us', '0', '0', '<p>Contact</p>\n\n<p>244 Lafeyette Ave<br />\nNew York, NY 10003 Phone: 630-219-1761<br />\nFax: 630-219-1761&nbsp;<br />E-mail: contact@theprosperitysociety.com</p>\n', '0', 0, '1', '2016-05-09 19:02:49', NULL, 1, 'contact-us', 1, 1),
(16, 4, 0, 'Relax during workdays', '0', '0', '0', 'Short Description here', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '0', 0, '1', '2016-06-14 16:08:13', NULL, 2, 'relax-during-workdays', 0, 0),
(17, 4, 0, 'Relax during workdays', '0', '0', '0', 'Short Description here', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '0', 0, '1', '2016-06-14 16:08:05', NULL, 2, 'relax-during-workdays', 0, 0),
(18, 5, 0, 'DO NYC : CASH FLOW SECRETS W/ ANGELA JIA KIM + NOHA WAIBSNAIDER', '0', '0', '05/24/2016', '0', '<p>In 2004, Noha Waibsnaider founded Peeled Snacks when she noticed there was something missing from the snack aisle. Store shelves were full of processed potato chips and corn syrup-filled candy. Noha set out to create tasty and nourishing snacks made with pure, wholesome ingredients.<br />\nPeeled Snacks are available nationwide in the US &amp; Canada via coffee shops, airports, gyms, hotels, and in Starbucks, Hudson News, Whole Foods, and Amazon.</p>\n\n<p><img alt="" src="http://localhost/thesuccess/assets/cms/plugins/ckfinder/userfiles/images/1.jpg" style="border-style:solid; border-width:1px; float:left; height:200px; margin:2px; width:300px" /><img alt="" src="http://localhost/thesuccess/assets/cms/plugins/ckfinder/userfiles/images/2.jpg" style="border-style:solid; border-width:2px; float:left; height:200px; margin:2px; width:300px" /></p>\n\n<p><br />\n<br />\n<br />\n<br />\n<br />\n<br />\n<br />\n&nbsp;</p>\n\n<p><br />\n<br />\n<img alt="" src="http://localhost/thesuccess/assets/cms/plugins/ckfinder/userfiles/images/3.jpg" style="border-style:solid; border-width:2px; float:left; height:200px; margin:2px; width:300px" /><img alt="" src="http://localhost/thesuccess/assets/cms/plugins/ckfinder/userfiles/images/4.jpg" style="border-style:solid; border-width:2px; float:left; height:200px; margin:2px; width:300px" /></p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p><br />\n<br />\nNot your typical interviewer, Savor founder Angela Jia Kim has gained a reputation for holding probing no&ndash;holds&ndash;barred interviews with business celebrity CEOs and authors to get to the underbelly of success.<br />\n<br />\n<strong>During this interview, you will learn how to:</strong><br />\n- Stay flexible with your business plan.<br />\n- Find the right funding for your company.<br />\n- Manage your cash flow.<br />\n- Negotiate your way into major distribution channels.<br />\n<br />\n<strong>Schedule:</strong><br />\n6:00 - 6:30&nbsp;&nbsp; &nbsp;&nbsp; Happy Hour &quot;Curated Connection&quot; Intros<br />\n6:30 - 7:15&nbsp;&nbsp; &nbsp;&nbsp; LIVE Taping of Savor Speaker Series<br />\n7:15 - 8:00&nbsp;&nbsp; &nbsp;&nbsp; Audience Q + A<br />\n8:00 - 9:00&nbsp;&nbsp; &nbsp;&nbsp; Breakout: &quot;Give, Give, Get&quot; Mastermind Sessions<br />\n<br />\n<strong>Mastermind Sessions:</strong><br />\nThe &quot;DO&quot; Speaker Series are followed by Savor&#39;s signature &quot;Give, Give, Get&quot; Mastermind break-out sessions. Join fellow women creators + makers and get their brilliant brains on your business to breakthrough to the next level.</p>\n', '0', 0, '1', '2016-06-01 03:15:49', NULL, 1, 'do-nyc-cash-flow-secrets-w-angela-jia-kim-noha-waibsnaider', 0, 0),
(19, 5, 0, 'SAVOR SPEAKER VIDEO SERIES: THE ENTREPRENEUR''S RECIPE FOR SUCCESS FT. JESSICA HERRIN', '0', '0', '05/20/2016', '0', '<p>&quot;Why not have a little music and a little fun? If you&#39;re passionate and having fun, you do better. Having each other in a support system, you also do better.&quot; - Jessica Herrin.<br />\nIn this Savor Speaker VIDEO event, Jessica Herrin supplies kernels that will fuel you to take action to pursue your personal path. After joining two successful tech startups out of college, she went to the Stanford Graduate School of Business, where, at the ripe age of 24, she co-founded the now world&#39;s leading wedding site, WeddingChannel.com. Hear how she has discovered how to achieve a rockin&#39; business + a rockin&#39; life!<br />\n<br />\nAs the brains behind the Stella &amp; Dot Family Brands, Jessica Herrin proves you can style your life with smarts, courage, and tenacity. She&rsquo;s been recognized for her business savvy in O, The Oprah Magazine, The Wall Street Journal and The New York Times. Jessica is most proud of the recognition she gets from the women of Stella &amp; Dot, who are mirroring her success in reinventing the home business opportunity for the modern woman.<br />\n&nbsp;<br />\n<strong>During this presentation you will learn to:</strong><br />\n- Keep a high bar in your life.<br />\n- Feel like a successful person, in addition to being a successful entrepreneur.<br />\n- Levitate over any obstacle in your business.</p>\n', '0', 0, '1', '2016-06-07 02:59:18', NULL, 1, 'savor-speaker-video-series-the-entrepreneurs-recipe-for-success-ft-jessica-herrin', 0, 0),
(20, 6, 0, 'BEAUTY BITES: Mother’s Day Almond Shortbread (Vegan + Gluten-Free)', '0', '0', '0', '0', '<p>Show Mom your love and appreciation by baking her a batch of scrumptious #beautyfood shortbread! Just like its classic counterpart, this treat is light, buttery, and absolutely irresistible&hellip; but this one beautifies!</p>\r\n\r\n<p>The secret? Skin-saving coconut oil. It mimics the taste and texture of butter in bakery goodies while giving serious hydration from the inside out. The medium chain fatty acids also boost metabolism, allowing your body to use the fats as energy, rather than storing them.</p>\r\n\r\n<p>These bites are rich with subtle sweetness, a hint of warm vanilla and toasty almond.</p>\r\n\r\n<p>And they make such a pretty gift!</p>\r\n\r\n<p><strong>Ingredients:</strong></p>\r\n\r\n<ul>\r\n	<li>3/4 C tapioca flour | easily digested + adds light texture</li>\r\n	<li>1 C all-purpose gluten-free flour | protein + fiber</li>\r\n	<li>1 C raw, virgin coconut oil | anti-aging + skin plumping</li>\r\n	<li>&frac34; c raw coconut sugar | natural sweetener</li>\r\n	<li>1 Tbsp bourbon vanilla or 1 vanilla bean | anti-oxidants to help prevent wrinkles</li>\r\n	<li>1 tsp pure almond extract | retains skin moisture</li>\r\n	<li>&frac12; &ndash; &frac34; C raw sliced almonds | vitamin E, prevents cell damage</li>\r\n</ul>\r\n\r\n<p><strong>Directions:</strong></p>\r\n\r\n<p>Preheat oven to 350F.</p>\r\n\r\n<p>Warm the coconut oil on a very low flame until completely melted. While it&rsquo;s heating, whisk the flours and sugar together in a large mixing bowl. If you are using vanilla bean, split the pod and scrape the beans into the bowl. Pour the melted oil over the dry mixture evenly, then add extracts. Mix it as well as you can, then get in there with your hands until everything is evenly coated. The mixture will be a bit crumbly.</p>\r\n\r\n<p>Spoon the dough into an 11&rdquo; x 7&rdquo; baking pan. Press the dough down evenly with your fingers. Sprinkle the sliced almonds on top, and carefully press them into the dough. Score into 12 cookies, then bake for 18-20 minutes. Ovens vary, so keep an eye on them to ensure they don&rsquo;t cook too quickly or burn. You want them just golden on the edges.&nbsp; Allow them to cool completely, then store them in an airtight tin.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '0', 0, '1', '2016-05-08 09:53:31', NULL, 2, 'beauty-bites-mother-s-day-almond-shortbread-vegan-gluten-free', 0, 0),
(21, 1, 0, 'lost of fun', '0', '0', '0', '0', 'last summer vocation was awsome', '', 1, '0', '2016-05-30 08:50:14', NULL, 2, 'lost-of-fun', 0, 0),
(22, 2, 0, 'blogs', 'enhaskjdnaksjd kajs dkja sdkj askjd kajs dkja sdkj', 'asjdnoaind ad oa diondkh aklidmoa dk;a d''ia d;kja k;jd a;khd dua dlja slidk alskd ola dlka sdlk asld alks dalsk dalsk dalskd laksd alskd ', '0', '0', '<ul>\n	<li>\n	<p>prizes according to periodically draw procedures, the purchasers of these Registered Prize Bonds will be able to get profit on a quarterly basis.</p>\n	</li>\n	<li><img alt="" src="http://prizebond.avialdo.com/assets/imgs/tax.jpg" style="height:352px; width:1176px" />\n	<p>19Aug</p>\n\n	<p>Prize bonds attracting tax-avoiding money</p>\n\n	<p>By:&nbsp;SHAHID IQBAL ,&nbsp;Year:&nbsp;2015</p>\n\n	<p>KARACHI: The issue of withholding tax on banking transactions has taken a new turn as protesting traders have started investing in bonds. Higher demand for prize bonds has pushed up their prices and certain denominations are now in short supply. &ldquo;The prize bonds of Rs25,000 and Rs40,000 are not available due to sudden rise in demand,&rdquo; said a bond trader in the open market. He said they were charging a premium of Rs400 to Rs1,000 per bond. The amount also varies with the date of announcement of prizes. A market watcher said, &ldquo;Instead of putting their money elsewhere, investors prefer to invest in prize bonds as they are like liquid assets and are payable as cash.&rdquo;</p>\n	</li>\n	<li><img alt="" src="http://prizebond.avialdo.com/assets/imgs/blog.jpg" style="height:352px; width:1176px" />\n	<p>14Nov</p>\n\n	<p>Privatise this, privatise that &ndash; just make me rich!</p>\n\n	<p>By:&nbsp;Saad Niaz ,&nbsp;Year:&nbsp;2014</p>\n\n	<p>It has been over a year since the formation of the Privatisation Commission and there has been no productive groundwork. ILLUSTRATION: JAMAL KHURSHID/ FILE I look around my house searching for items I can sell and make a quick buck. I come across some furniture that needs fresh coats of polish before I can even present it for sale. This requires too much effort! Why don&rsquo;t I just sell that antique painting that has been hanging in the drawing room for years? This is effortless and is guaranteed to bring me a truck load of cash. As it is, I just have five hours to make money!</p>\n	</li>\n</ul>\n', '0', 0, '0', '2016-05-30 08:43:58', NULL, 1, 'blogs', 1, 1),
(23, 4, 0, 'prize bond', '0', '0', '0', 'Short Description here', '<p><img alt="" src="http://prizebond.avialdo.com/assets/imgs/bonds.jpg" style="height:352px; width:1176px" /></p>\r\n\r\n<p>10Jan</p>\r\n\r\n<p>National Savings to launch &lsquo;Registered Prize Bonds&rsquo; in Pakistan</p>\r\n\r\n<p>By:&nbsp;The News Teller ,&nbsp;Year:&nbsp;2016</p>\r\n\r\n<p>ISLAMABAD: The National Savings has officially announced to launch Registered Prize Bonds in Pakistan on the decree of the Ministry of Finance. The registered bonds are being launched to generate more funds for the federal government. The Ministry of Finance has approved launching of Registered Prize Bonds by the National Savings. In addition to providing cash prizes by normal draw, the holders will also be able to earn the profit on a quarterly basis. Telling about the Registered Prize Bonds, the official of National Savings has stated that these bonds will be registered on the name of the purchasers and no other holder will be allowed to claim the holdings instead of the registered purchasers. This will further enhance the security of bond in case of theft or lost. The Federal Government has taken this step to generate the more funds. In addition to offering the normal cash prizes according to periodically draw procedures, the purchasers of these Registered Prize Bonds will be able to get profit on a quarterly basis.</p>\r\n', '0', 0, '1', '2016-06-14 16:07:53', NULL, 2, 'prize-bond', 0, 0),
(24, 3, 0, 'linked union', '0', '0', '0', '0', 'https://www.youtube.com/watch?v=kttOM-GurEU', '0', 0, '1', '2016-06-01 06:01:18', NULL, 2, 'linked-union', 0, 0),
(25, 6, 0, 'new era', '0', '0', '0', '0', '<p>asdasdas</p>\n', '0', 0, '1', '2016-06-01 03:17:51', NULL, 1, 'new-era', 0, 0),
(26, 4, 0, 'Relax during works day', '0', '0', '0', 'Short Description here', '<p>i f you really want to be relax then leave your GF trus me it works ;)</p>\r\n\r\n<p>i f you really want to be relax then leave your GF trus me it works ;)</p>\r\n\r\n<p>i f you really want to be relax then leave your GF trus me it works ;)</p>\r\n\r\n<p>i f you really want to be relax then leave your GF trus me it works ;)</p>\r\n\r\n<p>i f you really want to be relax then leave your GF trus me it works ;)</p>\r\n', '0', 0, '1', '2016-06-14 16:07:44', NULL, 2, 'relax-during-works-day', 0, 0),
(27, 4, 0, 'new blog', '0', '0', '0', 'Short Description here', '<p>this is my new blog&nbsp;this is my new blogthis is my new blog&nbsp;this is my new blog&nbsp;this is my new blogthis is my new blogthis is my new blog&nbsp;this is my new blog&nbsp;this is my new blog&nbsp;this is my new blog&nbsp;this is my new blog</p>\r\n', '0', 0, '0', '2016-06-14 16:07:26', NULL, 2, 'new-blog', 0, 0),
(28, 3, 0, 'test', '0', '0', '0', '0', 'https://www.youtube.com/watch?v=yblgC0yNBRI', '0', 0, '1', '2016-06-02 10:04:31', NULL, 2, 'test', 0, 0),
(29, 4, 0, 'Test', '0', '0', '0', 'Short Description here', '<p>&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;est Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;est Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test</p>\r\n\r\n<p>est Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>est Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test&nbsp;Test Test Test</p>\r\n', '0', 0, '1', '2016-06-14 16:07:15', NULL, 2, 'test', 0, 0),
(30, 4, 0, '5 Ways Socially Mature Brands Deliver Great Customer Experience', '0', '0', '0', 'hi Murtuza, its time to check the short description', '<p>&nbsp;</p>\n\n<p>Social customer service is the wave of the future. In just a few years, customers have gone from begrudgingly waiting on phone lines, hoping to finally hear the sound of a human voice, to simply posting questions and expecting to be answered within minutes.&nbsp;Technology has&nbsp;<a href="http://www.convinceandconvert.com/customer-experience/23-statistics-that-show-why-customer-service-mostly-sucks/" target="_blank">raised the bar</a>&nbsp;in terms of customer expectations. With everything moving faster, you have to be on the ball with regards to your communication strategy, branding, and customer interactions. In addition,&nbsp;<strong>you are no longer competing exclusively with brands in your industry. Instead, you&rsquo;re&nbsp;competing with every other company your customers have ever dealt with.</strong></p>\n\n<p>As a result, brands have had to take their social media interactions to the next level. Now that social media has become the platform of choice for customer-brand communications, it&rsquo;s time for every brand to up their techniques in order to create the optimum experience for each and every customer.</p>\n\n<h3>1. Timing Is Everything</h3>\n\n<p>When your customer takes the time to drop a comment on social media&mdash;whether it&rsquo;s a complaint, praise, a question, or something else entirely&mdash;they&rsquo;re looking for instant gratification and resolution. They want an answer, an explanation, or some type of acknowledgement from you almost instantaneously.</p>\n\n<p>After all, in their mind, you are there to meet their needs. So if there is a lapse in time, it seems as if you have other priorities. In fact,&nbsp;<strong><a href="http://blog.hubspot.com/marketing/twitter-response-time-data#sm.0000hyeuygcfnfo3x2p2hsp5q8x3z" target="_blank">72 percent of customers</a>&nbsp;who complain on Twitter expect a response within an hour</strong>, and 53 percent&nbsp;believe waiting time should be even less. (highlight to tweet)</p>\n\n<p><img alt="social media response time data" src="http://www.convinceandconvert.com/wp-content/uploads/2016/06/social-media-response-time-data.png" style="border-style:none; height:auto; margin:0px auto 2.4rem; width:700px" />Even for complex solutions, your initial response time must be speedy. Because Facebook messenger now shows response time, your customers will automatically know exactly how long it took you to get back to them. The faster it is, the more impressed they will be. Make sure to address the issue quickly&nbsp;and convey&nbsp;that you are working to find the best solution for their situation.</p>\n\n<h3>2. Make Customer Service the Top Priority Across Channels</h3>\n\n<p>You are on Facebook, Twitter, Instagram, and Snapchat,&nbsp;but are all these channels optimized for customer service, or are they used purely as a vehicle for marketing content? Today&rsquo;s customers are looking to<a href="http://www.forbes.com/sites/theyec/2012/12/24/5-ways-social-media-takes-customer-relationships-to-the-next-level-2/" target="_blank">&nbsp;build relationships with brands through social&nbsp;</a>accounts. Therefore, it can&rsquo;t be all about you&mdash;it has to be about them as well. Dedicate some time to develop a social media strategy which addresses and fuels your customers&rsquo; needs in all relevant social channels. This way, you become a true #socialfirst brand, making your customers feel valued and important.</p>\n\n<p>Not all social media tools are created equal, especially when it comes to optimizing it for customer service. Customers today want to reach you on channels that are convenient for them. That means&nbsp;<strong>your customer service&nbsp;should be available over any social&nbsp;platform where they are already present</strong>. It&rsquo;s therefore important that whatever platform you use has the capabilities for mobile communication and Facebook Messenger. This way, customers are able to reach you in a manner that feels natural and easy for them.</p>\n\n<p>According to YPulse,&nbsp;<a href="https://www.ypulse.com/post/view/the-future-of-real-time-24-7-customer-service" target="_blank">75 percent of purchases</a>&nbsp;will be on mobile by 2020. This means your customer service platform must&nbsp;be easy to access through mobile in order to stay ahead of the game. In addition, more and more companies are getting in on the Facebook Messenger action. This gives companies the capability to discuss personal issues in a manner that is both convenient and private,&nbsp;allowing both the brand and customer to win.</p>\n\n<p><img alt="mobile ecommerce data" src="http://www.convinceandconvert.com/wp-content/uploads/2016/06/mobile-ecommerce-data.png" style="border-style:none; height:auto; margin:0px auto 2.4rem; width:596px" /></p>\n\n<h3>3. Be Human</h3>\n\n<p>At the end of the day, people are looking for human interaction. That&rsquo;s why they prefer to work with<a href="http://digital-matchbox.com/heres-how-your-favorite-brands-tweet-their-human-side/" target="_blank">&nbsp;companies that show personality</a>&nbsp;over brands that seem robotic and automated. This is especially true on social media. Since social channels are made for casual interactions, brands that are able to communicate in a natural tone generally win the seal of approval. However, it&rsquo;s important to match your customer&rsquo;s tone. If your customer is addressing you in a more formalized manner, it&rsquo;s always better to answer formally in order to boost credibility.</p>\n\n<p>It&rsquo;s important to extend this strategy both on and off social media. Just as your communication on social should generally be more relatable (i.e. writing how people actually speak), your website and other marketing materials should reflect this human tone in order to keep it personal.</p>\n\n<p>You&#39;ll Also Like</p>\n\n<p><a href="http://www.convinceandconvert.com/social-media-strategy/track-hashtag-campaigns/">How to Track Hashtag Campaigns with Social Media Analytics</a></p>\n\n<p><a href="http://www.convinceandconvert.com/social-media-case-studies/power-customer-experiences-with-content/">How to Power Happy Customer Experiences With Content</a></p>\n\n<p><a href="http://www.convinceandconvert.com/social-media-research/42-percent-of-consumers-complaining-in-social-media-expect-60-minute-response-time/">42 Percent of Consumers Complaining in Social Media Expect 60 Minute Response Time</a></p>\n\n<h3>4. Resolve In-Channel</h3>\n\n<p>Removing your customers from their preferred channel to solve an issue is both inconvenient and inefficient. People don&rsquo;t want to add unnecessary extra steps to solve their problems. Instead, they want you to meet them the whole way, putting the full responsibility on your end.</p>\n\n<p><strong>If you are discussing an issue on Facebook, keep it on Facebook.</strong>&nbsp;In fact, using Facebook messenger has helped brands like Hyatt connect with their customers in a manner that is extremely convenient for everyone. Since Messenger is a personal channel meant for one-on-one communication, it can be optimized for personal resolution as opposed to public venting. Customers appreciate this sentiment, and as a result, continue to remain loyal to the hotel chain.</p>\n\n<h3>5. Get Your Entire Team Involved</h3>\n\n<p>Social is no longer a vehicle exclusively for your marketing department. In fact, your social interactions will be more well-rounded and effective if you get your entire team on board with a distinct social media plan.&nbsp;<strong>This way, you present yourself as a unified voice</strong>, giving the entire company the opportunity to familiarize themselves with your customers&rsquo; needs.</p>\n\n<p>This integration is essential. Through it, you make sure your products and services actually meet your customer&rsquo;s standards while also keeping the overall customer experience top-notch.</p>\n\n<p>Keep in mind that your&nbsp;<a href="http://www.convinceandconvert.com/social-media-strategy/ceo-twitter-account/" target="_blank">CEO is not excluded from this effort</a>. In fact, your CEO&nbsp;active on personal social accounts can make a huge difference in showing the human side your company and solidifying your branding.</p>\n\n<p>Maturing your social media strategy is a great way to improve customer experience. By following the tips above, you go the extra mile&nbsp;to make sure that your company listens and respects your customer&rsquo;s needs, establishing you as a credible and genuine company that people want to work with again and again.</p>\n\n<p><em>Get more content like this, plus the very BEST marketing education, totally free. Get our&nbsp;<a href="http://convinceandconvert.com/newsletter" target="_blank">Definitive</a>&nbsp;email newsletter.</em></p>\n', '0', 0, '1', '2016-06-15 03:01:40', NULL, 1, '5-ways-socially-mature-brands-deliver-great-customer-experience', 0, 0),
(31, 4, 0, 'lost of fun', '0', '0', '0', 'Short Description here', '<p>as</p>\r\n', '0', 0, '1', '2016-06-14 16:06:55', NULL, 2, 'lost-of-fun', 0, 0),
(32, 7, 0, 'buy nature', '', '', '0', '250000', '<p>This is very good prduct</p>\n', '0', 0, '1', '2016-06-14 01:21:49', NULL, 1, 'buy-nature', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `IdSettings` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `MetaKeyword` time DEFAULT NULL,
  `MetaDescription` time DEFAULT NULL,
  `EmailTo` varchar(255) DEFAULT NULL,
  `EmailFrom` varchar(255) DEFAULT NULL,
  `Mobile` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  `Address` text,
  `Logo` text,
  `HeaderText` text,
  `FooterText` text,
  `Copyright` text,
  `Text_1` text,
  `Text_2` text,
  `Text_3` text,
  `Facebook` text,
  `Twitter` text,
  `GooglePlus` text,
  `LinkedIn` text,
  `Updated` datetime DEFAULT NULL,
  `AccountId` int(11) DEFAULT NULL,
  PRIMARY KEY (`IdSettings`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`IdSettings`, `Title`, `MetaKeyword`, `MetaDescription`, `EmailTo`, `EmailFrom`, `Mobile`, `Phone`, `Address`, `Logo`, `HeaderText`, `FooterText`, `Copyright`, `Text_1`, `Text_2`, `Text_3`, `Facebook`, `Twitter`, `GooglePlus`, `LinkedIn`, `Updated`, `AccountId`) VALUES
(1, 'The Success', '00:00:00', '00:00:00', 'info@mail.com', 'info@mail.com', '123456789', '123456789', '', 'Hydrangeas.jpg', '', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took…', 'Copyright © 2016 CMS', 'this is text 1', 'this is text 2', 'this is text 3', 'www.facebook.com', '#', '#', '#', '2016-06-13 04:59:11', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `fk_account_account_type` FOREIGN KEY (`IdAccountType`) REFERENCES `account_type` (`IdAccountType`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
